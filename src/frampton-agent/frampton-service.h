/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define FRAMPTON_TYPE_SERVICE (frampton_service_get_type ())
G_DECLARE_FINAL_TYPE (FramptonService, frampton_service, FRAMPTON, SERVICE,
                      GApplication)

FramptonService *frampton_service_new (void);

G_END_DECLS
