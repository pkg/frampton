/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "frampton-service.h"

#include "frampton-agent.h"

struct _FramptonService
{
  GApplication parent;
};

G_DEFINE_TYPE (FramptonService, frampton_service, G_TYPE_APPLICATION)

static void
frampton_service_activate (GApplication *app)
{
  DEBUG ("Service activated");
}

static gboolean
frampton_service_dbus_register (GApplication *app,
                                GDBusConnection *connection,
                                const gchar *object_path,
                                GError **error)
{
  GApplicationClass *parent_class =
    G_APPLICATION_CLASS (frampton_service_parent_class);

  if (!parent_class->dbus_register (app, connection, object_path, error))
    return FALSE;

  g_application_hold (app);
  DEBUG ("Registering on D-Bus at %s", object_path);

  return TRUE;
}

static void
frampton_service_dbus_unregister (GApplication *app,
                                  GDBusConnection *connection,
                                  const gchar *object_path)
{
  GApplicationClass *parent_class =
    G_APPLICATION_CLASS (frampton_service_parent_class);

  DEBUG ("Unregistered from D-Bus");
  g_application_release (app);

  parent_class->dbus_unregister (app, connection, object_path);
}

static void
frampton_service_class_init (FramptonServiceClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->activate = frampton_service_activate;
  app_class->dbus_register = frampton_service_dbus_register;
  app_class->dbus_unregister = frampton_service_dbus_unregister;
}

static void
frampton_service_init (FramptonService *self)
{
}

FramptonService *
frampton_service_new (void)
{
  return FRAMPTON_SERVICE (g_object_new (FRAMPTON_TYPE_SERVICE,
                                         "application-id", FRAMPTON_AGENT_ID,
                                         "flags", G_APPLICATION_IS_SERVICE,
                                         NULL));
}
