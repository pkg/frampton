/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "frampton.h"
extern gchar *FRAMPTON_AUDIO_PLAYER_APP_NAME;
//#include "roller.h"

//static gboolean cover_to_detail = TRUE;
//#define ORIGIN_ITEM_SIZE       162.0
//#define TARGET_ITEM_SIZE       277.0

//static void cover_to_detail_transition(RollerContainer *roller, guint row, FramptonAudioPlayer *pAudioPlayer);

static gint get_song_roller_related_thumb_row(FramptonAudioPlayer *pAudioPlayer, gint inRow)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	ThornburyModelIter *iter_master;
	gint relatedRow = 0;

	if(NULL == priv->pSongRollerModel)
		return relatedRow;

	iter_master = thornbury_model_get_iter_at_row (priv->pSongRollerModel, inRow);
        if(NULL == iter_master)
                return relatedRow;

	thornbury_model_iter_get(iter_master, SONGS_ROLLER_RELATED_THUMB_INDEX, &relatedRow, -1);

	if(iter_master)
		g_object_unref(iter_master);

	return relatedRow;
}

static gint get_song_roller_focused_row(FramptonAudioPlayer *pAudioPlayer)
{
        GHashTable *pHash =  NULL;
        gint focused_row = -1;

        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

        pHash = thornbury_get_property(priv->pThornburyViewManager,  SONGS_ROLLER, "focused-row", NULL);

        if(NULL != pHash)
        {
                focused_row = g_value_get_int(g_hash_table_lookup(pHash, "focused-row"));

        }

        if(NULL != pHash)
        {
                g_hash_table_destroy(pHash);
                pHash = NULL;
        }

        return focused_row;
}

static gint get_thumb_roller_focused_row( FramptonAudioPlayer *pAudioPlayer)
{
	GHashTable *pHash =  NULL;
	gint focused_row = -1;

	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

        pHash = thornbury_get_property(priv->pThornburyViewManager,  THUMB_ROLLER, "focused-row", NULL);

        if(NULL != pHash)
        {
              	focused_row = g_value_get_int(g_hash_table_lookup(pHash, "focused-row"));
 
        }

	if(NULL != pHash)
        {
                g_hash_table_destroy(pHash);
                pHash = NULL;
        }

	return focused_row;
}

/*********************************************************************************************
 * Function:    find_sort_item
 * Description: find sort item
 * Parameters:  key, value, userData
 * Return:      Bool value to indicate if
 ********************************************************************************************/
static gboolean find_sort_item(gpointer key, gpointer value, gpointer userData)
{
    if(NULL != key && NULL != value)
    {
        if(g_str_equal((gchar*)key, (gchar*)userData))
        {
            return TRUE;
        }
    }
    return FALSE;
}

/*********************************************************************************************
 * Function: update_sort_roller_item
 * Description: update sort roller based on list roller alphabet
 * Parameters:  pAudioPlayer*, pRollerName*
 * Return:   gint
 ********************************************************************************************/
static void
update_sort_roller_item (FramptonAudioPlayer *pAudioPlayer,
    const gchar *pRollerName,
    ThornburyModel *pModel,
    gint inColumn)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	GHashTable *pHash =  NULL;
        gint focused_row = -1;
	gchar *pData = NULL;
	gchar *pSortData = NULL;
	ThornburyModelIter *iter = NULL;
	gchar *pChar = NULL;
	gchar *pStrUp = NULL;

	if(priv->bSortItemfocus)
        {
                priv->bSortItemfocus = FALSE;
                return;
        }

	/* get list roller focused row */
	pHash = thornbury_get_property(priv->pThornburyViewManager, pRollerName, "focused-row", NULL);
	if(NULL == pHash)
        {
                return;
        }

        focused_row = g_value_get_int(g_hash_table_lookup(pHash, "focused-row"));

	if(NULL != pHash)
	{
		g_hash_table_destroy(pHash);
		pHash = NULL;
	}

	/* get focused row data from model */
	iter = thornbury_model_get_iter_at_row (pModel, focused_row);
        if(NULL == iter)
                return;

        thornbury_model_iter_get(iter, inColumn, &pData, -1);

	g_object_unref (iter);

	/* extract first character */
	if(NULL != pData)
	{
		pChar = g_strndup(pData, 1);

                pStrUp = g_ascii_strup(pChar, 1);
		
		if(NULL != pChar)
			g_free(pChar);
	}

	/* iter through sort roller model to find the list roller character */
	iter = thornbury_model_get_first_iter(priv->pSortRollerModel);
	while (!thornbury_model_iter_is_last (iter))
	{
		gint inRow = -1;
		thornbury_model_iter_get(iter, SORT_ROLLER_COLUMN_LABEL, &pSortData, -1);
		if(g_strcmp0(pSortData, pStrUp) == 0)
		{
			focused_row = thornbury_model_iter_get_row(iter);
			pHash = thornbury_get_property(priv->pThornburyViewManager, THUMB_SORT_ROLLER, "focused-row", NULL);

	                if(NULL != pHash)
        	        {
                	        inRow = g_value_get_int(g_hash_table_lookup(pHash, "focused-row"));
                        	DEBUG ("row in focus  = %d", inRow);
                	}

			if(inRow != focused_row)
	                        thornbury_set_property(priv->pThornburyViewManager, THUMB_SORT_ROLLER, "focused-row", focused_row, NULL);

			if(NULL != pHash)
			{
				g_hash_table_destroy(pHash);
				pHash = NULL;
			}

			priv->bListSortfocus = TRUE;
			if(pSortData != NULL)
                            	g_free(pSortData);
             	        break;
		}
		g_free(pSortData);
		iter = thornbury_model_iter_next (iter);
	}
	if(NULL != pStrUp)
		g_free(pStrUp);

	g_object_unref (iter);
}

/*********************************************************************************************
 * Function: update_list_roller_sort_item
 * Description: update list roller based on sort roller alphabet
 * Parameters:  pAudioPlayer*, inRow
 * Return:   gint
 ********************************************************************************************/
static void update_list_roller_sort_item( FramptonAudioPlayer *pAudioPlayer, gint inRow)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        ThornburyModelIter *iter = NULL;
        gchar *value = NULL;
        gchar *sortKey = NULL;
        gint inFocusedRow;
        gint focused_row = -1;
        GHashTable *pHash =  NULL;

	iter = thornbury_model_get_iter_at_row (priv->pSortRollerModel, inRow);

        if(NULL == iter)
                return;

	/* For the same child,if this call back is called then do nothing and return */
        if(priv->bListSortfocus)
        {
                priv->bListSortfocus = FALSE;
                return;
        }

	thornbury_model_iter_get(iter, SORT_ROLLER_COLUMN_LABEL, &value, -1);

	if(iter)
		g_object_unref(iter);

	if(value == NULL)
		return;

        DEBUG (" sort row focused value = %s", value);

	sortKey = g_hash_table_find(priv->pSortListHash, (GHRFunc)find_sort_item, value); 

	if(sortKey)
	{
		const gchar *pRoller = NULL;

		/* convert to int */
		inFocusedRow = atoi((gchar*)sortKey);

		/* update the sort item in list rollerbased on current mode */
		if(FRAMPTON_AUDIO_PLAYER_MODE_SONGS == priv->enCurrentMode)
			pRoller = SONGS_ROLLER;
		else
			pRoller = ARTIST_ALBUM_ROLLER;
	
		pHash = thornbury_get_property(priv->pThornburyViewManager, pRoller, "focused-row", NULL);

		if(NULL != pHash)
		{
			focused_row = g_value_get_int(g_hash_table_lookup(pHash, "focused-row"));
			DEBUG ("row in focus in %s = %d", pRoller, focused_row);
		}

		if(focused_row != inFocusedRow)
			thornbury_set_property(priv->pThornburyViewManager, pRoller, "focused-row",  inFocusedRow, NULL);

		priv->bSortItemfocus = TRUE;

		if(NULL != pHash)
		{
			g_hash_table_destroy(pHash);
			pHash = NULL;
		}
	}
}

/*********************************************************************************************
 * Function: update_sort_list
 * Description: sort list for sync between list/sort roller 
 * Parameters:  pText*, pAudioPlayer(
 * Return:   
 ********************************************************************************************/
void update_sort_list(gchar *pText, gint inRow, FramptonAudioPlayer *pAudioPlayer)
{

	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gchar *pChar = NULL;
	gchar *pStrUp = NULL;
	gchar *text = NULL;
	text = g_strdup_printf("%d", inRow);
	if(NULL != pText)
	{
		pChar = g_strndup(pText, 1);	

		pStrUp = g_ascii_strup(pChar, 1);

		if(NULL == g_hash_table_lookup(priv->pSortListHash, pStrUp))
			g_hash_table_insert(priv->pSortListHash, pStrUp, text);

		if(NULL != pChar)
		{
			g_free(pChar);
			pChar = NULL;
		}
	}
}

/*********************************************************************************************
 * Function: create_and_position_line   
 * Description: line creation
 * Parameters:  lineColor, x, y, width, height, parent*
 * Return:   line actor*
 ********************************************************************************************/
ClutterActor* create_and_position_line(ClutterColor lineColor, gfloat x, gfloat y, gfloat width, gfloat height, ClutterActor *parent)
{
        ClutterActor *line = NULL;
        line = clutter_actor_new();
        clutter_actor_set_background_color(line, &lineColor);
        clutter_actor_set_size(line, width, height);

        if(NULL != parent)
                clutter_actor_add_child(parent, line);

        clutter_actor_set_position(line, x, y);
        return line;
}

/*********************************************************************************************
 * Function: create_and_position_text   
 * Description: text creation
 * Parameters:  fontName, fontColor, x, y, parent*
 * Return:   text actor*
 ********************************************************************************************/
ClutterActor* create_and_position_text(const gchar* fontName, ClutterColor fontColor, gfloat x, gfloat y, ClutterActor *parent)
{
         /*Create the text actor, set it's font and position it within it's parent container */
        ClutterActor *text = clutter_text_new();
        clutter_text_set_font_name(CLUTTER_TEXT(text), fontName);
        clutter_text_set_color(CLUTTER_TEXT(text), &fontColor);
        if(NULL != parent)
                clutter_actor_add_child(parent, text);
        clutter_actor_set_position(text, x, y);
        return text;

}

/*********************************************************************************************
 * Function: view_switch_begin_cb
 * Description: callback for back pressed
 * Parameters:  (ThornburyViewManager *, previous view name, pUserData
 * Return:   void
 ********************************************************************************************/
void view_switch_begin_cb(ThornburyViewManager *pThornburyViewManager, gchar *pName, gpointer *pUserData)
{
	FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
	FramptonAudioPlayerPrivate *priv;

	if(! FRAMPTON_IS_AUDIO_PLAYER(pUserData))
                return;

	priv = pAudioPlayer->priv;
	if( g_strcmp0(pName, "ThumbView" ) == 0 && priv->bRemoveState == FALSE)
	{
		add_remove_thumbs_divisible_by_4(pAudioPlayer, TRUE);
		g_timeout_add(500, (GSourceFunc)set_thumb_to_focus_on_view_change, priv->pCurrentTrack);
	}
}

/*********************************************************************************************
 * Function: get_current_playing_item_number
 * Description: get the current playing item number in the given model
 * Parameters:  pModel*, pAudioPlayer*, column
 * Return:   gint
 ********************************************************************************************/
gint get_current_playing_item_number(ThornburyModel *pModel, FramptonAudioPlayer *pAudioPlayer, gint column)
{
        gint rowToFocus = 0;
        gboolean bFound = FALSE;
        gchar *value = NULL;
        ThornburyModelIter *iter = NULL;
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

        if(priv->pCurrentTrack == NULL)
                return -1;

        if(pModel)
        {
                iter = thornbury_model_get_first_iter(pModel);

                while (!thornbury_model_iter_is_last (iter))
                {
                        thornbury_model_iter_get(iter, column, &value, -1);
                        rowToFocus = thornbury_model_iter_get_row(iter);
                        if(g_strcmp0(value, priv->pCurrentTrack) == 0)
                        {

                                bFound = TRUE;
                                break;
                        }
                        iter = thornbury_model_iter_next (iter);
                }
        }

	if(iter)
		g_object_unref(iter);
        if(bFound)
                return rowToFocus;
        else
                return -1;
}

/*********************************************************************************************
 * Function: view_switched   
 * Description: callback for view switch
 * Parameters:  (ThornburyViewManager *, pUserData
 * Return:   void
 ********************************************************************************************/
void view_switched(ThornburyViewManager *pThornburyViewManager, gpointer pName, gpointer *pUserData)
{
        gchar *pViewName = (gchar *)pName;
	FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

       DEBUG ("switched to view : %s", pViewName);

	if(g_strcmp0(pName, "DetailView" ) == 0 && priv->bRemoveState == TRUE)
	{
		add_remove_thumbs_divisible_by_4(pAudioPlayer, FALSE);
		//g_timeout_add(500, (GSourceFunc)set_thumb_to_focus_on_view_change, priv->pCurrentTrack);
	}
	if(priv->bNewUri)
	{	
		frampton_audio_player_initiate_play(pAudioPlayer, priv->pCurrentTrack);
		priv->bNewUri = FALSE;
	}

	if(NULL != priv->pCurrentView)
	{
		g_free(priv->pCurrentView);
		priv->pCurrentView = NULL;
	}
	priv->pCurrentView = g_strdup(pViewName);
	priv->bAnimation = FALSE;

	if(priv->disableLoadCheck == FALSE )
                priv->bLoad = TRUE;

}

/*********************************************************************************************
 * Function: context_drawer_button_released_cb   
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void list_context_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData)
{
        DEBUG ("drawer button pressed = %s",pName);
}

/*********************************************************************************************
 * Function: views_drawer_button_released_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void views_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData)
{
	FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gint focusedRow;
	gint rowToFocus = 0;

	if(! FRAMPTON_IS_AUDIO_PLAYER(pUserData))
		return;

	priv = pAudioPlayer->priv;
	//GHashTable *propHash = NULL;
	//gint row = 0;

	if(g_strcmp0(priv->pCurrentView, "ListView" ) == 0 && g_strcmp0(pName, "LIST-VIEW") == 0)
		return;
	if(g_strcmp0(priv->pCurrentView, "ThumbView" ) == 0 && g_strcmp0(pName, "THUMB") == 0)
		return; 
	if(g_strcmp0(priv->pCurrentView, "DetailView" ) == 0 && g_strcmp0(pName, "DETAIL-VIEW") == 0)
		return; 

	//focusedRow = get_thumb_roller_focused_row(pAudioPlayer);

	/* switch to list view with playing item in focus */
	if (!g_strcmp0(pName, "LIST-VIEW"))
	{
		if(priv->pCurrentTrack == NULL)
                {
			thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "ListView", TRUE, -1);
		}
		else
		{
			set_list_to_focus_on_view_change(pAudioPlayer, FALSE);
			thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "ListView", TRUE, -1);
		}
	}
	/* if switched view is Detail view */
	else if (!g_strcmp0(pName, "DETAIL-VIEW"))
        {
                if(g_strcmp0(priv->pCurrentView, "ThumbView" ) == 0)
                {
                        if(priv->pCurrentTrack == NULL)
                        {
                                focusedRow = get_thumb_roller_focused_row(pAudioPlayer);
				/* work around to play the focused row . Need to be fixed in roller */
                                thumb_roller_item_activated_cb(NULL, focusedRow + 4, pAudioPlayer);
                        }
                        else
                        {
							focusedRow = get_current_playing_item_number(priv->pSongRollerModel, pAudioPlayer, 0);
							rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, focusedRow);
			
							if(priv->disableLoadCheck == FALSE && g_strcmp0(priv->pCurrentView, "DetailView" ) != 0 )
			                	priv->bLoad = FALSE;

                                thornbury_switch_view (priv->pThornburyViewManager, (gchar *) "DetailView", TRUE, rowToFocus);
                        }
                }
		else if(g_strcmp0(priv->pCurrentView, "ListView" ) == 0)
		{
			if(priv->pCurrentTrack == NULL)
                        {
				focusedRow = get_song_roller_focused_row(pAudioPlayer);
				rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, focusedRow);
				albumartist_roller_item_activated_cb(NULL, rowToFocus, pAudioPlayer);
			}
			else
			{
				focusedRow = get_current_playing_item_number(priv->pSongRollerModel, pAudioPlayer, 0);
				rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, focusedRow);
				thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
                                thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "DetailView", TRUE, rowToFocus);
			}		
		}
	}
	/* if switched to thumb view */
	else 
	{
		/* if current view is detail view */
		if(g_strcmp0(priv->pCurrentView, "DetailView" ) == 0)
                {
                        if(priv->pCurrentTrack == NULL)
                        {
                                focusedRow = get_thumb_roller_focused_row(pAudioPlayer);
                                thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "ThumbView", TRUE, focusedRow);
                        }
                        else
                        {
                                rowToFocus = get_current_playing_item_number(priv->pThumbRollerModel, pAudioPlayer, 1);
                                thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "ThumbView", TRUE, rowToFocus);
                        }
                }
		/* if current view is list view */
		else if(g_strcmp0(priv->pCurrentView, "ListView" ) == 0)
		{
			if(priv->pCurrentTrack == NULL)
			{

				focusedRow = get_song_roller_focused_row(pAudioPlayer);
				rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, focusedRow);

				thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
				thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "ThumbView", TRUE, rowToFocus);
			}
			else
			{
				focusedRow = get_current_playing_item_number(priv->pSongRollerModel, pAudioPlayer, 0);
				rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, focusedRow);
				thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
				thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "ThumbView", TRUE, rowToFocus);
			}
		}
	}

}

/*********************************************************************************************
 * Function: meta_roller_item_activated_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
meta_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData)
{
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        ThornburyModelIter *iter;
        gchar *pUri = NULL;

         /* get the selected url */
        iter = thornbury_model_get_iter_at_row (pAudioPlayer->priv->pMetaRollerModel, inRow);
        if(NULL == iter)
                return;

	thornbury_model_iter_get(iter, SONGS_ROLLER_COLUMN_ID, &pUri, -1);

	if(iter)
		g_object_unref(iter);

	if(pUri == NULL || g_strcmp0(pUri, "") == 0)
                return;
        if(NULL == priv->pCurrentTrack || g_strcmp0(pUri, priv->pCurrentTrack) != 0)
        {
		gint rowToFocus;

                /* send play request */
		priv->bNewUri = TRUE;
		update_current_track_details(pAudioPlayer, pUri);
                frampton_audio_player_initiate_play(pAudioPlayer, pUri);
		rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, inRow);
		update_media_overlay(pAudioPlayer, rowToFocus, pUri);

        }

}

/*********************************************************************************************
 * Function: song_roller_item_activated_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
song_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData)
{
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        ThornburyModelIter *iter;
        gchar *pUri = NULL;
        gint rowToFocus = 0;

	 /* get the selected url */
        iter = thornbury_model_get_iter_at_row (pAudioPlayer->priv->pSongRollerModel, inRow);
        if(NULL == iter)
                return;

        thornbury_model_iter_get(iter, SONGS_ROLLER_COLUMN_ID, &pUri, -1);

	if(iter)
                g_object_unref(iter);

	if(pUri == NULL)
                return;

        if(NULL == priv->pCurrentTrack || g_strcmp0(pUri, priv->pCurrentTrack) != 0)
        {
                /* send play request */
                //frampton_audio_player_initiate_play(pAudioPlayer, pUri);
				priv->bNewUri = TRUE;
				update_current_track_details(pAudioPlayer, pUri);
				rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, inRow);
                update_media_overlay(pAudioPlayer, rowToFocus, pUri);
		thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
        }
	else
	{
		gint focusedRow = get_current_playing_item_number(priv->pSongRollerModel, pAudioPlayer, 0);
                rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, focusedRow);
                thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
	}

	thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "DetailView", TRUE, -1);
}

/*********************************************************************************************
 * Function: albumartist_roller_item_activated_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
albumartist_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData)
{
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        ThornburyModelIter *iter;
        gchar *pUri = NULL;
        gint rowToFocus = 0;
        gint focusedRow = 0;

         /* get the selected url */
        iter = thornbury_model_get_iter_at_row (pAudioPlayer->priv->pAlbumArtistRollerModel, inRow);
        if(NULL == iter)
                return;

        thornbury_model_iter_get(iter, ARTISTALBUM_ROLLER_COL_FIRST_URL, &pUri, -1);

	if(iter)
                g_object_unref(iter);

        if(pUri == NULL)
                return;

        if(NULL == priv->pCurrentTrack || g_strcmp0(pUri, priv->pCurrentTrack) != 0)
        {
                /* send play request */
                //frampton_audio_player_initiate_play(pAudioPlayer, pUri);
				priv->bNewUri = TRUE;
				update_current_track_details(pAudioPlayer, pUri);
				focusedRow = get_song_roller_focused_row(pAudioPlayer);
				rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, focusedRow);
				thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
				update_media_overlay(pAudioPlayer, rowToFocus, pUri);
        }
	else
	{
		focusedRow = get_current_playing_item_number(priv->pSongRollerModel, pAudioPlayer, 0);
		rowToFocus = get_song_roller_related_thumb_row(pAudioPlayer, focusedRow);
		thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
	}

	thornbury_switch_view_no_animation (priv->pThornburyViewManager, (gchar *) "DetailView", TRUE, -1);
}

/*********************************************************************************************
 * Function: sort_roller_item_activated_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
sort_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData)
{
	update_list_roller_sort_item(FRAMPTON_AUDIO_PLAYER(pUserData), inRow);
}

# if 0
static void animation_started_cb (ClutterAnimation *animation,MildenhallRoller *roller)
{
        if (!cover_to_detail)
        {
                clutter_actor_set_width (CLUTTER_ACTOR (roller), -1.0);
                g_object_set(roller,"width", 656.0, NULL);
        }
}

static void animation_completed_cb (ClutterAnimation *animation,MildenhallRoller *roller)
{
        ClutterActor *roller_ins;
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
	
        g_object_get(G_OBJECT(roller),"Roller",&roller_ins,NULL);
        if (cover_to_detail)
        {
                g_object_set (roller, "child-height", TARGET_ITEM_SIZE, NULL);
                g_object_set (roller, "child-width", TARGET_ITEM_SIZE, NULL);
                g_object_set (roller, "natural-width", TARGET_ITEM_SIZE, NULL);
                g_object_set(roller,"width", 277.0, NULL);
		thornbury_switch_view(pAudioPlayer->priv->pThornburyViewManager, "DetailView", TRUE);
		pAudioPlayer->priv->bAnimation = FALSE;
        	//cover_to_detail = !cover_to_detail;

        }
	else
        {
                g_object_set (roller, "child-height", ORIGIN_ITEM_SIZE, NULL);
                g_object_set (roller, "child-width", ORIGIN_ITEM_SIZE, NULL);
		cover_to_detail = TRUE;
        }
        
	v_ROLLERCONT_set_animation_mode(ROLLER_CONTAINER(roller), FALSE);

}

static void cover_to_detail_transition(RollerContainer *roller, guint row, FramptonAudioPlayer *pAudioPlayer)
{
	ClutterTimeline *timeline;
	v_ROLLERCONT_fixed_roller_set_min_visible_actors(ROLLER_CONTAINER(roller), 16);
        v_ROLLERCONT_set_animation_mode(ROLLER_CONTAINER(roller), TRUE);

        if (cover_to_detail)
        {
	        timeline = clutter_timeline_new (1500);
        	g_signal_connect (G_OBJECT (timeline), "started", G_CALLBACK (animation_started_cb), roller);
        	g_signal_connect (G_OBJECT (timeline), "completed", G_CALLBACK (animation_completed_cb), roller);

                v_ROLLERCONT_resize (ROLLER_CONTAINER(roller),
                                TARGET_ITEM_SIZE, /* roller width */
                                TARGET_ITEM_SIZE, /* item width */
                                TARGET_ITEM_SIZE, /* item height */
                                row,
                                timeline);
        }
	else
	{
	        timeline = clutter_timeline_new (1);
        	g_signal_connect (G_OBJECT (timeline), "started", G_CALLBACK (animation_started_cb), roller);
        	g_signal_connect (G_OBJECT (timeline), "completed", G_CALLBACK (animation_completed_cb), roller);

		 v_ROLLERCONT_resize (ROLLER_CONTAINER(roller),
                                ORIGIN_ITEM_SIZE * 4, /* roller width */
                                ORIGIN_ITEM_SIZE,     /* item width */
                                ORIGIN_ITEM_SIZE,     /* item height */
                                row,
                                timeline);

	}
}
# endif

/*********************************************************************************************
 * Function: thumb_roller_item_activated_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
thumb_roller_item_activated_cb (MildenhallRollerContainer *roller, guint row, gpointer pUserData)
{
	FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(pUserData);
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gchar *pUri = NULL;
	gboolean bFocus = TRUE;
	ThornburyModelIter *iter_master;
    DEBUG ("row = %d", row);
	//priv->pThumbRollerPtr = CLUTTER_ACTOR(roller);
	
	/* get the selected url */
	iter_master = thornbury_model_get_iter_at_row (pAudioPlayer->priv->pThumbRollerModel, row);
	if(NULL == iter_master)
		return;

        thornbury_model_iter_get(iter_master, THUMB_ROLLER_URL, &pUri, -1);
	if(pUri == NULL)
		return;

	if(priv->disableLoadCheck == FALSE && g_strcmp0(priv->pCurrentView, "DetailView" ) != 0 )
		priv->bLoad = FALSE;
	
	thornbury_model_iter_get(iter_master, THUMB_ROLLER_OVERLAY_SHOW, &bFocus, -1);


	if(! bFocus && (NULL == priv->pCurrentTrack || g_strcmp0(pUri, priv->pCurrentTrack) != 0) )
	{
		if(g_strcmp0(priv->pCurrentView, "DetailView" ) == 0 )
		{
			/* send play request */
			frampton_audio_player_initiate_play(pAudioPlayer, pUri);
		}
		else
		{
			priv->bNewUri = TRUE;
		}
		update_current_track_details(pAudioPlayer, pUri);
		update_media_overlay(pAudioPlayer, row, pUri);
		//thornbury_set_property(priv->pThornburyViewManager, DETAIL_INFO_ROLLER, "show", FALSE, NULL);
        }
	else
	{
		if(g_strcmp0(priv->pCurrentView, "DetailView" ) == 0 )
		{
			thornbury_model_iter_get(iter_master, THUMB_ROLLER_OVERLAY_STATE, &bFocus, -1);
	        	if(FALSE == bFocus)
        		{
                		update_resume_to_audio_agent(pAudioPlayer);
        		}
		}
	}

	if(g_strcmp0(priv->pCurrentView, "DetailView" ) != 0 )
	{
		bFocus = TRUE;
		thornbury_model_iter_get(iter_master, THUMB_ROLLER_OVERLAY_STATE, &bFocus, -1);
		if(FALSE == bFocus)
		{
			update_media_overlay_play_state(TRUE);
			update_highlight_arrow_state(TRUE);
			update_meta_highlight_arrow_state(TRUE);
			update_resume_to_audio_agent(pAudioPlayer);
		}

		/* transition to detail */
		priv->bAnimation = TRUE;
		
		thornbury_switch_view (priv->pThornburyViewManager, (gchar *) "DetailView", TRUE, row);
		//cover_to_detail_transition(roller, row, pAudioPlayer);
	}

	if(iter_master	)
                g_object_unref(iter_master);


}

/*********************************************************************************************
 * Function: sort_roller_locking_finished_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
sort_roller_locking_finished_cb (MildenhallRollerContainer *pRoller, gpointer pUserData)
{
	FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
	FramptonAudioPlayerPrivate *priv;
	GHashTable *pHash =  NULL;
	gint inRow = -1;

        if(! FRAMPTON_IS_AUDIO_PLAYER(pUserData))
                return;

	priv = pAudioPlayer->priv;

        pHash = thornbury_get_property(priv->pThornburyViewManager, THUMB_SORT_ROLLER, "focused-row", NULL);

	inRow = g_value_get_int(g_hash_table_lookup(pHash, "focused-row"));
        DEBUG ("sort row focused  = %d", inRow);

	if(NULL != pHash)
        {
                g_hash_table_destroy(pHash);
                pHash = NULL;
        }

	
	update_list_roller_sort_item(pAudioPlayer, inRow);
}

/*********************************************************************************************
 * Function: song_roller_locking_finished_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
song_roller_locking_finished_cb (MildenhallRollerContainer *pRoller, gpointer pUserData)
{
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
        FramptonAudioPlayerPrivate *priv;
        gint focused_row;
        gint focused_row_master;
        gchar *value;
        gchar *value_focus;
        gchar *value_master;
        guint slaveRow;
        ThornburyModel *model;
        ThornburyModel *slave_model;
        GHashTable *masterPropHash = NULL;
        GHashTable *slavePropHash = NULL;
        ThornburyModelIter *iter_master;

        if(! FRAMPTON_IS_AUDIO_PLAYER(pUserData))
                return;

        priv = pAudioPlayer->priv;

	if(FRAMPTON_AUDIO_PLAYER_MODE_SONGS == priv->enCurrentMode)
		update_sort_roller_item(pAudioPlayer, SONGS_ROLLER, priv->pSongRollerModel, SONGS_ROLLER_COLUMN_LABEL);

	if(priv->QuickMenufocus)
        {
                priv->QuickMenufocus = FALSE;
                return;
        }

	masterPropHash = thornbury_get_property(priv->pThornburyViewManager, ARTIST_ALBUM_ROLLER, "focused-row", "model", NULL);
	slavePropHash = thornbury_get_property(priv->pThornburyViewManager, SONGS_ROLLER, "focused-row", "model", NULL);
	
	if(NULL == masterPropHash || NULL == slavePropHash)
        {
                return;
        }

        focused_row_master = g_value_get_int(g_hash_table_lookup(masterPropHash, "focused-row"));
        DEBUG ("master row focused  = %d", focused_row_master);
        model = (ThornburyModel *)g_value_get_object(g_hash_table_lookup(masterPropHash, "model"));

        focused_row = g_value_get_int(g_hash_table_lookup(slavePropHash, "focused-row"));
       DEBUG ("slave row focused  = %d", focused_row);
        slave_model = (ThornburyModel *)g_value_get_object(g_hash_table_lookup(slavePropHash, "model"));

	if(priv->enCurrentMode == FRAMPTON_AUDIO_PLAYER_MODE_SONGS)
	{
		thornbury_set_property(priv->pThornburyViewManager, ARTIST_ALBUM_ROLLER, "focused-row",  focused_row, NULL);
                priv->MainMenufocus = TRUE;
		return;
	}
	iter_master = thornbury_model_get_iter_at_row (model, focused_row_master);
        thornbury_model_iter_get(iter_master, 0, &value_master, -1);

	if(iter_master)
		g_object_unref(iter_master);

	if(model)
        {
                ThornburyModelIter *iter = thornbury_model_get_iter_at_row (slave_model,focused_row);
                if(iter)
                {
                        thornbury_model_iter_get(iter,0,&value,-1);
                        if(value)
                        {
                                gchar *quick_menu = g_strdup(g_hash_table_lookup(priv->pSongsHash, value));
                                if(quick_menu)
                                {
                                        gchar *quickstr = NULL;
                                        ThornburyModelIter *inner_iter = thornbury_model_get_first_iter(model);
                                        while (!thornbury_model_iter_is_last (inner_iter))
                                        {

                                                thornbury_model_iter_get (inner_iter, 0, &quickstr, -1);
                                                if(g_strcmp0(quickstr,quick_menu) == 0)
                                                {
                                                        slaveRow = thornbury_model_iter_get_row (inner_iter);
                                                        thornbury_model_iter_get (inner_iter, 0, &value_focus, -1);

							if(g_strcmp0(value_master,quick_menu) == 0)
							{
								return;
							}
							//g_object_set (G_OBJECT (master_roller), "focused-row", slaveRow, NULL);
							thornbury_set_property(priv->pThornburyViewManager, ARTIST_ALBUM_ROLLER, "focused-row",  slaveRow, NULL);
                                                        priv->MainMenufocus = TRUE;
                                                        g_free(quickstr);
                                                        break;
                                                }
                                                g_free(quickstr);
                                                iter = thornbury_model_iter_next (inner_iter);
                                        }
                                        g_object_unref (iter);
                                        g_free(quick_menu);

                                }
                                g_free(value);
                        }
                }
                g_object_unref (iter);
        }
	if(NULL != masterPropHash)
	{
		g_hash_table_destroy(masterPropHash);
		masterPropHash = NULL;
	}
	if(NULL != slavePropHash)
	{
		g_hash_table_destroy(slavePropHash);
		slavePropHash = NULL;
	}

}

/*********************************************************************************************
 * Function: albumartist_roller_locking_finished_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
albumartist_roller_locking_finished_cb (MildenhallRollerContainer *pRoller, gpointer pUserData)
{
        gint focused_row;
        gint focused_row_slave;
        gchar *value;
        gchar *value_slave;
        guint slaveRow;
        ThornburyModel *model;
        ThornburyModel *slave_model;
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pUserData);
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        GHashTable *masterPropHash = NULL;
        GHashTable *slavePropHash = NULL;
        ThornburyModelIter *iter_slave;
	
	if(FRAMPTON_AUDIO_PLAYER_MODE_SONGS != priv->enCurrentMode)
		update_sort_roller_item(pAudioPlayer, ARTIST_ALBUM_ROLLER, priv->pAlbumArtistRollerModel, ARTISTALBUM_ROLLER_COL_MID_TEXT);

	/* For the same child,if this call back is called then do nothing and return */
	if(priv->MainMenufocus)
	{
		priv->MainMenufocus = FALSE;
		return;
	}
	

	masterPropHash = thornbury_get_property(priv->pThornburyViewManager,  ARTIST_ALBUM_ROLLER, "focused-row", "model", NULL);
	slavePropHash = thornbury_get_property(priv->pThornburyViewManager, SONGS_ROLLER, "focused-row", "model", NULL);
	
	if(NULL == masterPropHash || NULL == slavePropHash)
	{
		return;
	}

	focused_row = g_value_get_int(g_hash_table_lookup(masterPropHash, "focused-row"));
	DEBUG ("master row %d", focused_row);
	model = (ThornburyModel *)g_value_get_object(g_hash_table_lookup(masterPropHash, "model"));

	focused_row_slave = g_value_get_int(g_hash_table_lookup(slavePropHash, "focused-row"));
        DEBUG ("slave row %d", focused_row_slave);
        slave_model = (ThornburyModel *)g_value_get_object(g_hash_table_lookup(slavePropHash, "model"));

	if(priv->enCurrentMode == FRAMPTON_AUDIO_PLAYER_MODE_SONGS)
	{
		thornbury_set_property(priv->pThornburyViewManager, SONGS_ROLLER, "focused-row",  focused_row, NULL);
                priv->QuickMenufocus = TRUE;
		return;
	}

        iter_slave = thornbury_model_get_iter_at_row (slave_model, focused_row_slave);
        thornbury_model_iter_get(iter_slave,0,&value_slave,-1);
        if(model)
        {
                /* Get the corresponding iterative for the Master roller model */
                ThornburyModelIter *iter = thornbury_model_get_iter_at_row (model,focused_row);
                if(iter)
                {
                        /*Get the name of the child which is key value in hashtable*/
                        thornbury_model_iter_get(iter,0,&value,-1);
                        if(value)
                        {
                                /* For Key get the corresponding Pair in the hashtable */
                                gchar *quick_menu = g_strdup(g_hash_table_lookup(priv->pArtistAlbumHash, value));
                                if(quick_menu)
                                {
                                        gchar *quickstr = NULL;
                                        /* Get the exact row from the Quick menu Model for the pair Matched and
                                           set it to focus in quick menu roller*/
                                        ThornburyModelIter *inner_iter = thornbury_model_get_first_iter (slave_model);
                                        while (!thornbury_model_iter_is_last (inner_iter))
                                        {
						thornbury_model_iter_get (inner_iter, 0, &quickstr, -1);
                                                if(g_strcmp0(quickstr,quick_menu) == 0)
                                                {
                                                        slaveRow = thornbury_model_iter_get_row (inner_iter);
							thornbury_set_property(priv->pThornburyViewManager, SONGS_ROLLER, "focused-row",  slaveRow, NULL);
                                                        //g_object_set (G_OBJECT (slave_roller), "focused-row", slaveRow, NULL);
                                                        /* set the flag when QuickMenu is on focus */
                                                        priv->QuickMenufocus = TRUE;
                                                        g_free(quickstr);
                                                        break;
                                                }
                                                g_free(quickstr);
                                                inner_iter = thornbury_model_iter_next (inner_iter);
                                        }
                                        g_free(quick_menu);

                                }
                                g_free(value);
                        }
                }
                g_object_unref (iter);
        }
	if(NULL != masterPropHash)
        {
                g_hash_table_destroy(masterPropHash);
                masterPropHash = NULL;
        }
        if(NULL != slavePropHash)
        {
                g_hash_table_destroy(slavePropHash);
                slavePropHash = NULL;
        }

}

/*********************************************************************************************
 * Function: thumb_roller_locking_finished_cb
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void
thumb_roller_locking_finished_cb (MildenhallRollerContainer *pRoller, gpointer pUserData)
{   

}

/*********************************************************************************************
 * Function:info_roller_item_selected_cb 
 * Description: 
 * Parameters:  
 * Return:   void
 ********************************************************************************************/
void info_roller_item_selected_cb(MildenhallInfoRoller *roller, guint row, gpointer data)
{

}

/*********************************************************************************************
 * Function: progress_bar_play_requested_cb
 * Description: 
 * Parameters:  
 * Return:   gboolean
 ********************************************************************************************/
gboolean progress_bar_play_requested_cb(MildenhallProgressBar *progressBar, gpointer userData)
{
	if(FRAMPTON_AUDIO_PLAYER(userData) )
	{
		FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(userData);
		//FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
		update_resume_to_audio_agent(pAudioPlayer);
	}
	return TRUE;
}

/*********************************************************************************************
 * Function: progress_bar_pause_requested_cb
 * Description: 
 * Parameters:  
 * Return:   gboolean
 ********************************************************************************************/
gboolean progress_bar_pause_requested_cb(MildenhallProgressBar *progressBar, gpointer userData)
{
	if(FRAMPTON_AUDIO_PLAYER(userData) )
	{
		FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(userData);
		//FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
		update_pause_to_audio_agent(pAudioPlayer);
	}
	return TRUE;
}

/*********************************************************************************************
 * Function: progress_bar_seek_started_cb
 * Description: 
 * Parameters:  
 * Return:   gboolean
 ********************************************************************************************/
gboolean progress_bar_seek_started_cb(MildenhallProgressBar *progressBar, gfloat position, gpointer pUserData)
{
	if(FRAMPTON_AUDIO_PLAYER(pUserData) )
	{
		FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(pUserData);
		FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

		priv->bSeekStarted = TRUE;

	}
	return TRUE;
}

/*********************************************************************************************
 * Function: progress_bar_seek_updated_cb
 * Description: 
 * Parameters:  
 * Return:   gboolean
 ********************************************************************************************/
gboolean progress_bar_seek_updated_cb(MildenhallProgressBar *progressBar, gfloat position, gpointer pUserData)
{
	if(FRAMPTON_AUDIO_PLAYER(pUserData) )
	{
		FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(pUserData);
		FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

		/* send seek to audio agent */
		update_seek_to_audio_agent(position, pAudioPlayer);
		/* reset the flag to update progress bar */
		priv->bSeekStarted = FALSE;

	}
	return TRUE;
}

/*********************************************************************************************
 * Function: bottom_bar_pressed
 * Description: 
 * Parameters:  
 * Return:   gboolean
 ********************************************************************************************/
gboolean
bottom_bar_pressed (MildenhallBottomBar *bottomBar, gchar *pButtonName, gpointer pUserData)
{
        return FALSE;
}

/*********************************************************************************************
 * Function: bottom_bar_released
 * Description: 
 * Parameters:  
 * Return:   gboolean
 ********************************************************************************************/
gboolean
bottom_bar_released (MildenhallBottomBar *bottomBar, gchar *pButtonName, gpointer pUserData)
{
        if(FRAMPTON_AUDIO_PLAYER(pUserData) )
        {
                FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(pUserData);
                FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
		if(g_strcmp0(pButtonName, SHUFFLE) == 0)
		{
			priv->bShuffle = !priv->bShuffle;
			frampton_agent_call_set_shuffle_state(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, priv->bShuffle, NULL, shuffle_message_info_cb, NULL);
		}
		else
		{
			priv->bRepeat = !priv->bRepeat;
			frampton_agent_call_set_repeat_state(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, priv->bRepeat, NULL, repeat_message_info_cb, NULL);
		}
        }
        return FALSE;
}


/****************************************************
 * Function : get_requested_mode
 * Description: Get the requested mode of play
 * Parameters: The arguments with which the process
 *                         was launched
 * Return value: AudioPlayerMode
 ********************************************************/
FramptonAudioPlayerMode get_requested_mode(GList *execArgs)
{
        FramptonAudioPlayerMode audioMode = FRAMPTON_AUDIO_PLAYER_MODE_NONE;
        if(NULL != execArgs)
        {
                gchar *requestedMode = g_list_nth_data(execArgs, 1);

                if(NULL != requestedMode)
                {
                        //if(g_strcmp0(requestedMode, FRAMPTON_AUDIO_PLAYER_ARTISTS) == 0)
			if(g_strrstr(requestedMode, FRAMPTON_AUDIO_PLAYER_ARTISTS) != NULL)
                                audioMode = FRAMPTON_AUDIO_PLAYER_MODE_ARTIST;
                        else if (g_strrstr(requestedMode, FRAMPTON_AUDIO_PLAYER_ALBUM) != NULL)
                                audioMode = FRAMPTON_AUDIO_PLAYER_MODE_ALBUM;
                        else if (g_strrstr(requestedMode, FRAMPTON_AUDIO_PLAYER_SONGS) != NULL)
                                audioMode = FRAMPTON_AUDIO_PLAYER_MODE_SONGS;
                }
        }

        return audioMode;
}

/*********************************************************************************************
 * Function: clear_list
 * Description: free the list
 * Parameters:  FramptonAudioPlayer *
 * Return:   void
 ********************************************************************************************/
static void clear_list(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	if(NULL != priv->pCoverThumbList)
	{
		GList *tempList = g_list_first(priv->pCoverThumbList);
		while(tempList && tempList->data)
		{
			priv->pCoverThumbList = g_list_remove_link (priv->pCoverThumbList, tempList);
			g_free(tempList->data);
			tempList->data = NULL;
			g_list_free_1 (tempList);
			tempList = g_list_first(priv->pCoverThumbList);
		}
		priv->pCoverThumbList = NULL;
	}	
	if(NULL != priv->pThumbsList)
        {
                GList *tempList = g_list_first(priv->pThumbsList);
                while(tempList && tempList->data)
                {
			if(tempList->data)
			{
                        	g_free(tempList->data);
                        	tempList->data = NULL;
			}
                        tempList = g_list_next(tempList);
                }
		g_list_free(priv->pThumbsList);	
		priv->pThumbsList = NULL;
        }       
}

/*******************************************************************
 *Function: clear_hash_entries
 *Description: Clear all hash table entries
 *Paramters: Key, value, userdata
 *Return Value: Bool (To remove or not)
 *******************************************************************/
static gboolean clear_hash_entries(gpointer key, gpointer value, gpointer userData)
{
        if(NULL != key)
        {
                g_free(key);
                key = NULL;
        }

        if(NULL != value)
        {
                g_free(value);
                value = NULL;
        }
        return TRUE;
}

/*******************************************************************
 *Function: clear_hash_tables
 *Description: clear the hash tables
 *Paramters: AudioPlayer element
 *Return Value: void
 *******************************************************************/
static void clear_hash_tables(FramptonAudioPlayer *audioPlayer)
{
        FramptonAudioPlayerPrivate *priv = audioPlayer->priv;

        /* Clear the list view hash (it will again be populated based on the
         * new configuration) */
	if(NULL != priv->pArtistAlbumHash)
	{
		g_hash_table_foreach_remove(priv->pArtistAlbumHash, (GHRFunc)clear_hash_entries, audioPlayer);
		g_hash_table_destroy(priv->pArtistAlbumHash);
		priv->pArtistAlbumHash = NULL;
	}
	if(NULL != priv->pSongsHash)
	{
		g_hash_table_foreach_remove(priv->pSongsHash, (GHRFunc)clear_hash_entries, audioPlayer);
		g_hash_table_destroy(priv->pSongsHash);
		priv->pSongsHash = NULL;
	}
	if(NULL != priv->pListModelHash)
	{
		g_hash_table_destroy(priv->pListModelHash);
		priv->pListModelHash = NULL;
	}
	if(NULL != priv->pThumbModelHash)
	{
		g_hash_table_destroy(priv->pThumbModelHash);
		priv->pThumbModelHash = NULL;
	}
	if(NULL != priv->pDetailModelHash)
	{
		g_hash_table_destroy(priv->pDetailModelHash);
		priv->pDetailModelHash = NULL;
	}
	if(NULL != priv->pSortListHash)
	{
		g_hash_table_destroy(priv->pSortListHash);
		priv->pSortListHash = NULL;
	}
}	

/*******************************************************************
 *Function: parse_restart_state_args
 *Description: restart arguments provided in restart/show state off application
 *Paramters: GVariant*
 *Return Value: gchar **
 *******************************************************************/
static gchar **parse_restart_state_args(GVariant *arguments)
{
        gchar **argv = NULL;
        GVariantIter iter;
        gchar *value;
        gchar *key;
        gint i = 0;
        gint args;

        if(NULL == arguments)
                return NULL;

        g_variant_iter_init (&iter, arguments);
        args = g_variant_n_children (arguments);
        argv = g_new0(gchar *, args * 2 + 1);
        while (g_variant_iter_next (&iter, "{ss}", &key, &value))
        {
		if(key)
		{
                	argv[i] = g_strdup(key);
                	g_free (key);
			key = NULL;
                	i++;
		}
		if(g_strcmp0(value, "") != 0)
		{
                	argv[i] = g_strdup(value);
                	g_free (value);
			value = NULL;
                	i++;
		}
        }
	argv[i] = NULL;
        return argv;
}

# if 0
/*******************************************************************
 *Function: free_url
 *Description: free all urls
 *Paramters: gchar **
 *Return Value: void
 *******************************************************************/
static void free_url(gchar **pUrl)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	if(NULL != priv->pResultPointer)
	{
		gint i;
		for(i = 0; priv->pResultPointer[i]; i++)
		{
			g_free(priv->pResultPointer[i]);
			priv->pResultPointer[i] = NULL;	
		}
		if(NULL != priv->pResultPointer)
		{
			g_free(priv->pResultPointer);
			priv->pResultPointer = NULL;
		}
	}
}
# endif

/*******************************************************************
 *Function: free_all_memory
 *Description: free memory
 *Paramters: pAudioPlayer*
 *Return Value: void
 *******************************************************************/
void free_all_memory(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

# if 0
        if(priv->inTimeoutSource > 0)
        {
                g_source_remove(priv->inTimeoutSource);
                priv->inTimeoutSource = 0;
        }
# endif
	if(NULL != priv->pTimeline)
	{
            g_object_unref(priv->pTimeline);
			priv->pTimeline = NULL;
	}

	 if(priv->pResultPointer)
	 {
		 g_strfreev(priv->pResultPointer);
		 priv->pResultPointer = NULL;
	 }
	 if(priv->pModeUrlList)
	 {
		 g_strfreev(priv->pModeUrlList);
		 priv->pModeUrlList = NULL;
	 }
# if 0
	if(NULL != priv->pResultPointer)
        {
                /* free url array */
                free_url(priv->pResultPointer);
        }
# endif
        /* Clear the hash tables */
        clear_hash_tables(pAudioPlayer);
	/* clear list */
        clear_list(pAudioPlayer);
        /* destroy view */
        if(NULL != priv->pThornburyViewManager)
        {
                g_object_unref(G_OBJECT(priv->pThornburyViewManager));
                priv->pThornburyViewManager = NULL;
        }

	/* free current playing/loading info struct */
        g_clear_pointer (&priv->currentPlayingInfo, grassmoor_media_info_free);
        g_clear_pointer (&priv->currentLoadMediaInfo, grassmoor_media_info_free);

        /* free track duration */
        if(NULL != priv->trackDuration)
        {
                g_free(priv->trackDuration);
                priv->trackDuration = NULL;
        }

	if(NULL != priv->pDataDir)
	{
		g_free(priv->pDataDir);
		priv->pDataDir = NULL;
	}
	if(NULL != priv->pCurrentView)
        {
                g_free(priv->pCurrentView);
                priv->pCurrentView = NULL;
        }
	
	
}

/*********************************************************************************************
 * Function:    frampton_audio_player_free_str
 * Description: free char pointer
 * Parameters:  gchar *
 * Return:
 *********************************************************************************************/
void frampton_audio_player_free_str(gchar *pStr)
{
        if(NULL != pStr)
        {
                g_free(pStr);
                pStr = NULL;
        }
}

/*******************************************************************
 *Function: timeout_reset_ui
 *Description: reset ui on restart state with some delay 
 *Paramters: gchar **
 *Return Value: gboolean
 *******************************************************************/
static gboolean
timeout_reset_ui (gchar **execArgs)
{
	FramptonAudioPlayer *pAudioPlayer;
	FramptonAudioPlayerPrivate *priv;

	if(NULL == execArgs)
		return FALSE;

	pAudioPlayer = frampton_audio_player_get_default();
	priv = pAudioPlayer->priv;

# if 0
	if(priv->inTimeoutSource > 0)
	{
		g_source_remove(priv->inTimeoutSource);
		priv->inTimeoutSource = 0;
	}
# endif
	if(NULL != priv->pTimeline)
	{
        	g_object_unref(priv->pTimeline);
			priv->pTimeline = NULL;
	}
	thornbury_switch_view (priv->pThornburyViewManager, (gchar *) "ThumbView", TRUE, -1);

	/* disconnect all audio agent signals */
	disconnect_audio_agent_signals(priv->audio_proxy, pAudioPlayer);

         if(priv->pResultPointer)
         {
                 g_strfreev(priv->pResultPointer);
                 priv->pResultPointer = NULL;
         }
         if(priv->pModeUrlList)
         {
                 g_strfreev(priv->pModeUrlList);
                 priv->pModeUrlList = NULL;
         }

	/* Clear the hash tables */
	clear_hash_tables(pAudioPlayer);

	clear_list(pAudioPlayer);
	/* destroy view */
	if(NULL != priv->pThornburyViewManager)
	{
		g_object_unref(G_OBJECT(priv->pThornburyViewManager));
		priv->pThornburyViewManager = NULL;
	}

    g_clear_pointer (&priv->currentPlayingInfo, grassmoor_media_info_free);
    g_clear_pointer (&priv->currentLoadMediaInfo, grassmoor_media_info_free);

	/* free track duration */
	if(NULL != priv->trackDuration)
	{
		g_free(priv->trackDuration);
		priv->trackDuration = NULL;
	}

	if(NULL != priv->pCurrentView)
        {
                g_free(priv->pCurrentView);
                priv->pCurrentView = NULL;
        }

	if(NULL != priv->pCurrentSource)
        {
                g_free(priv->pCurrentSource);
                priv->pCurrentSource = NULL;
        }

	/* initialise hash */                        
	init_new_hash_tables(pAudioPlayer);
	/* update argv list */
	update_exec_args_list(execArgs);
	priv->enCurrentMode = get_requested_mode(frampton_audio_player_get_startup_exec_list());
	set_initial_configuration(pAudioPlayer);

	/* init views */
	initialize_views(pAudioPlayer);
	/* connect sigmnals */
	connect_audio_agent_signals(priv->audio_proxy, pAudioPlayer);
	frampton_agent_call_get_player_state(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, NULL, audio_agent_get_state_cb, pAudioPlayer);

	return FALSE;
}

/*******************************************************************
 *Function: timeout_reset_ui
 *Description: parse restart agguments and reset ui witj some delay
 *Paramters: GVariant *
 *Return Value: gboolean
 *******************************************************************/
gboolean reset_ui(GVariant *arguments)
{
	gchar **execArgs;

        if(NULL == arguments)
                return FALSE;

        execArgs = parse_restart_state_args(arguments);

        frampton_switch_ui_mode ((const gchar * const *) execArgs, FALSE);
        g_strfreev (execArgs);
        return FALSE;
}

/*
 * Schedule a delayed teardown and reconstruction of the UI state,
 * as though Frampton had been invoked with the given @execArgs
 * as its argv[1], argv[2]...
 *
 * If @force is true, always do the reset. This is done if we were asked
 * to handle a new URL.
 *
 * FIXME: Use a structured representation of the mode to which we are
 * switching and the URL (if any) we are opening, not a list of strings
 */
void
frampton_switch_ui_mode (const gchar * const *execArgs,
                         gboolean force)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	FramptonAudioPlayerMode audioMode = FRAMPTON_AUDIO_PLAYER_MODE_NONE;

        if(NULL == execArgs)
		return;

	if(priv->bResetUI)
		return;

        /* FIXME: this assumes that the arguments are in a particular order,
         * with some other pair followed by "menu-entry" and the display name,
         * which seems unlikely to be a completely safe assumption */
        if(NULL != execArgs[3])
        {
		const gchar *requestedMode = execArgs[3];
		DEBUG ("Args = %s", execArgs[3]);
                if(g_strrstr(requestedMode, FRAMPTON_AUDIO_PLAYER_ARTISTS) != NULL)
                        audioMode = FRAMPTON_AUDIO_PLAYER_MODE_ARTIST;
                else if (g_strrstr(requestedMode, FRAMPTON_AUDIO_PLAYER_ALBUM) != NULL)
                        audioMode = FRAMPTON_AUDIO_PLAYER_MODE_ALBUM;
                else if (g_strrstr(requestedMode, FRAMPTON_AUDIO_PLAYER_SONGS) != NULL)
                        audioMode = FRAMPTON_AUDIO_PLAYER_MODE_SONGS;

                if (force ||
                    (audioMode != priv->enCurrentMode &&
                     audioMode != FRAMPTON_AUDIO_PLAYER_MODE_NONE))
                {
			/* set the reset ui flag to stop population */
			priv->bResetUI = TRUE;
			g_timeout_add_full (G_PRIORITY_DEFAULT, 1000, (GSourceFunc) timeout_reset_ui, g_strdupv ((gchar **) execArgs), (GDestroyNotify) g_strfreev);
                }
        }
}

/****************************************************
 * Function : frampton_audio_player_roller_scroll_completed
 * Description: Callback function when roller scroll is completed
 * Parameters: instance and user data
 * Return value: gboolean (handled or not)
 ********************************************************/
gboolean frampton_audio_player_roller_scroll_completed(ClutterActor *actor, gpointer userData)
{
        if(FRAMPTON_IS_AUDIO_PLAYER(userData))
        {
                FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER(userData)->priv;

                if(priv->disableLoadCheck == FALSE )
                        priv->bLoad = TRUE;
        }

        return TRUE;
}

/****************************************************
 * Function : frampton_audio_player_roller_scroll_started
 * Description: Callback function when roller scroll is completed
 * Parameters: instance and user data
 * Return value: gboolean (handled or not)
 ********************************************************/
gboolean frampton_audio_player_roller_scroll_started(ClutterActor *actor, gpointer userData)
{
        if(FRAMPTON_IS_AUDIO_PLAYER(userData))
        {
                FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER(userData)->priv;
                if(priv->disableLoadCheck == FALSE)
                        priv->bLoad = FALSE;
        }

        return TRUE;
}
