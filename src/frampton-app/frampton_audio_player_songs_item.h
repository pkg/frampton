/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _FRAMPTON_AUDIO_PLAYER_SONGS_ITEM_H
#define _FRAMPTON_AUDIO_PLAYER_SONGS_ITEM_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define FRAMPTON_TYPE_AUDIO_PLAYER_SONGS_ITEM (frampton_audio_player_songs_item_get_type ())
G_DECLARE_FINAL_TYPE (FramptonAudioPlayerSongsItem,
        frampton_audio_player_songs_item,
        FRAMPTON,
        AUDIO_PLAYER_SONGS_ITEM,
        ClutterActor);

FramptonAudioPlayerSongsItem *frampton_audio_player_songs_item_new (void);

G_END_DECLS

#endif /* _FRAMPTON_AUDIO_PLAYER_SONGS_ITEM_H */
