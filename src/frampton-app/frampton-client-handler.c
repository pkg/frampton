/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "frampton.h"

extern ClutterActor *pAudioPlayerStage;
extern gchar *FRAMPTON_AUDIO_PLAYER_APP_NAME;

/*********************************************************************************************
 * Function:    termination_handler
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void termination_handler (int signum)
{
	exit(0);
}

/*********************************************************************************************
 * Function:    register_exit_functionalities
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void register_exit_functionalities(void)
{
	struct sigaction action = { 0, };
	action.sa_handler = termination_handler;
	action.sa_flags = 0;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGHUP, &action, NULL);
	sigaction(SIGTERM, &action, NULL);
	//atexit(audio_client_unregister);
}

/*********************************************************************************************
 * Function: frampton_audio_player_register_my_app_clb
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void frampton_audio_player_register_my_app_clb( GObject *source_object,
                          GAsyncResult *res,
                          gpointer user_data)
{
	gboolean regState;
	GError *error = NULL;
	FramptonAudioPlayer *audioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);

	if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
		return;

	regState = canterbury_app_manager_call_register_my_app_finish(audioPlayer->priv->app_mgr_proxy , res , &error);

	if(regState == FALSE)
	{
		if(error != NULL)
		{
			WARNING ("app register failed with error %s: ", error->message);
			g_error_free (error);
		}
		else
		{
			WARNING ("App register finish failed");
		}
		return;
	}
}

/*********************************************************************************************
 * Function:    new_app_status
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void
new_app_status( CanterburyAppManager  *object,
                const gchar             *app_name,
                gint                     new_app_state,
                GVariant                *arguments,
                gpointer                 user_data)
{
	if( g_strcmp0(FRAMPTON_AUDIO_PLAYER_APP_NAME ,app_name ) == FALSE )
	{
		switch (new_app_state)
		{
			case CANTERBURY_APP_STATE_START:
				DEBUG ("App %s state updated: started", app_name);
				//update_exec_args_list(NULL);
				break;

			case CANTERBURY_APP_STATE_BACKGROUND:
				/* see if it can do a clutter actor hide */
				DEBUG ("App %s state updated: background", app_name);
				break;

			case CANTERBURY_APP_STATE_SHOW:
			{
				DEBUG ("App %s state updated: showed", app_name);
				reset_ui( arguments);
				/* check if the application needs to resynchronize with its server */
				break;
			}
			case CANTERBURY_APP_STATE_RESTART:
				DEBUG ("App %s state updated: restarted", app_name);
				reset_ui( arguments);
				/* check if the application needs to resynchronize with its server */
				break;

			case CANTERBURY_APP_STATE_OFF:
				/* store all persistence infornmation immediately.. Free all memory */
				DEBUG ("App %s state updated: Off", app_name);
				break;

			case CANTERBURY_APP_STATE_PAUSE:
				/* store all persistence infornmation immediately..*/
				DEBUG ("App %s state updated: paused", app_name);
				break;

			default:
				WARNING ("app in unknown state = %d  ",new_app_state );
				break;
		}
	}
}


/*********************************************************************************************
 * Function:    app_mgr_proxy_clb
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void
app_mgr_proxy_clb( GObject *source_object,
                   GAsyncResult *res,
                   gpointer user_data)
{
	FramptonAudioPlayer *audioPlayer;
        FramptonAudioPlayerPrivate *priv;
	GError *error = NULL;
	if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
                return;

	audioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);
	priv = audioPlayer->priv;

	/* finishes the proxy creation and gets the proxy ptr */
 	priv->app_mgr_proxy = canterbury_app_manager_proxy_new_finish(res , &error);
	if(priv->app_mgr_proxy == NULL)
	{
		if(error != NULL)
		{
			WARNING ("app manager proxy creation failed with error: %s", error->message);
			g_clear_error (&error);
		}
		else
		{
			WARNING ("App Mgr proxy is NULL");
		}
		return;
	}

  	/* register for app manager signals */
	g_signal_connect (priv->app_mgr_proxy,
			"new-app-state",
			G_CALLBACK (new_app_status),
			user_data);

	canterbury_app_manager_call_register_my_app(priv->app_mgr_proxy,
  FRAMPTON_AUDIO_PLAYER_APP_NAME , 0, NULL, frampton_audio_player_register_my_app_clb, user_data);
}

/*********************************************************************************************
 * Function:   popup_proxy_clb
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void popup_proxy_clb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	FramptonAudioPlayer *audioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);
	GError *error = NULL;

	if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
		return;

	audioPlayer->priv->popup_proxy = barkway_service_proxy_new_finish(res , &error);
    if (error)
    {
    	WARNING ("barkway service proxy creation failed with error: %s", error->message);
    	g_error_free (error);
    	return;
    }
	//g_signal_connect(voicenotes->priv->popup_proxy,"popup-status",(GCallback)g_popup_status_clb,user_data);
	//g_signal_connect(voicenotes->priv->popup_proxy,"confirmation-result",(GCallback)g_confirmation_result_clb,user_data);
	//g_signal_connect(voicenotes->priv->popup_proxy,"error-msg",(GCallback)g_error_msg_clb,user_data);

}

/*********************************************************************************************
 * Function: app_mgr_name_appeared
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void app_mgr_name_appeared (GDBusConnection *connection,
                       const gchar     *name,
                       const gchar     *name_owner,
                       gpointer         user_data)
{
	DEBUG ("service name appeared: %s", name);

  /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_app_manager_proxy_new (
                                          connection,
                                          G_DBUS_PROXY_FLAGS_NONE,
                                          "org.apertis.Canterbury",
                                          "/org/apertis/Canterbury/AppManager",
                                          NULL,
                                          app_mgr_proxy_clb,
                                          user_data);
}

/*********************************************************************************************
 * Function: popup_name_appeared
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void popup_name_appeared (GDBusConnection *connection,
                        const gchar     *name,
                        const gchar     *name_owner,
                        gpointer         user_data)
{
	DEBUG ("service name appeared: %s", name);
    	barkway_service_proxy_new(connection,
                                   G_DBUS_PROXY_FLAGS_NONE,
                                   "org.apertis.Barkway",
                                   "/org/apertis/Barkway/Service",
                                   NULL,
                                   popup_proxy_clb,
                                   user_data);


}

/*********************************************************************************************
 * Function:    app_mgr_name_vanished
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void app_mgr_name_vanished( GDBusConnection *connection,
                       const gchar     *name,
                       gpointer         user_data)
{
        FramptonAudioPlayer *audioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);

	if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
                return;
    DEBUG ("service name vanished: %s", name);

	if(NULL != audioPlayer->priv->app_mgr_proxy)
		g_object_unref(audioPlayer->priv->app_mgr_proxy);
}

/*********************************************************************************************
 * Function:    popup_name_vanished
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void popup_name_vanished(GDBusConnection *connection,
                       const gchar     *name,
                       gpointer         user_data)
{
        FramptonAudioPlayer *audioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);
	if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
                return;

    DEBUG ("service name vanished: %s", name);
        if(NULL != audioPlayer->priv->popup_proxy)
                g_object_unref(audioPlayer->priv->popup_proxy);

}

/*********************************************************************************************
 * Function:    initialize_client_handler
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
void initialize_client_handler(FramptonAudioPlayer *pAudioPlayer,
                               GApplication *app)
{
	if ((g_application_get_flags (app) & G_APPLICATION_IS_SERVICE) == 0)
	{
		g_bus_watch_name (G_BUS_TYPE_SESSION,
				"org.apertis.Canterbury",
				G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
				app_mgr_name_appeared,
				app_mgr_name_vanished,
				pAudioPlayer,
				NULL);
	}

	g_bus_watch_name (G_BUS_TYPE_SESSION,
			"org.apertis.Barkway",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			popup_name_appeared,
			popup_name_vanished,
			pAudioPlayer,
			NULL);
	/* Starts watching name on the bus specified by bus_type */
	/* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
	g_bus_watch_name (G_BUS_TYPE_SESSION,
			FRAMPTON_AGENT_BUS_NAME,
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			audio_agent_name_appeared,
			audio_agent_name_vanished,
			pAudioPlayer,
			NULL);

	register_exit_functionalities();
}

