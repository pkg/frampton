/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "frampton.h"
extern gchar *FRAMPTON_AUDIO_PLAYER_APP_NAME;

/*********************************************************************************************
 * Internal function
 ********************************************************************************************/
static gboolean populate_item(FramptonAudioPlayer *pAudioPlayer);
static gint check_duplicate_entry (gconstpointer a, gconstpointer b);
static void update_media_overlay_text(FramptonAudioPlayer *pAudioPlayer, gint row, gchar *pUri);
static void extract_item(gpointer pData);

/*********************************************************************************************
 * Function: set_list_to_focus_on_view_change
 * Description:  set playing item in focus on view change
 * Parameters: pAudioPlayer
 * Return:   gboolean(handled or not)
 ********************************************************************************************/
gboolean set_list_to_focus_on_view_change(FramptonAudioPlayer *pAudioPlayer, gboolean bUpdateHighlight)
{
	FramptonAudioPlayerPrivate *priv;
	gint i = 0;

	if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
		return FALSE;

	priv = pAudioPlayer->priv;

	/* check if song is already getting played */
	if(NULL != priv->pCurrentTrack)
	{
		/* update the current index for load from the song getting played */
		gchar **pUrlArr = priv->pResultPointer;
		while(pUrlArr && pUrlArr[i])
		{
			if(g_strcmp0(pUrlArr[i], priv->pCurrentTrack) == 0)
			{
				/* set to focus */
				thornbury_set_property(priv->pThornburyViewManager, SONGS_ROLLER, "focused-row", i, NULL);
				if(bUpdateHighlight)
					update_highlight_arrow(pAudioPlayer, i, priv->pCurrentTrack);
				break;
			}
			i++;
		}
	}
	else
		 thornbury_set_property(priv->pThornburyViewManager, SONGS_ROLLER, "focused-row", 0, NULL);

	return FALSE;
}

/*********************************************************************************************
 * Function: set_thumb_to_focus_on_view_change
 * Description:  set playing item in focus on view change
 * Parameters: pAudioPlayer
 * Return:   gboolean(handled or not)
 ********************************************************************************************/
gboolean set_thumb_to_focus_on_view_change(gchar *pUri)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
	FramptonAudioPlayerPrivate *priv;
	gchar *value = NULL;
	gboolean bFound = FALSE;

	if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return FALSE;

	priv = pAudioPlayer->priv;

	if(priv->bAnimation)
		return FALSE;
	if(NULL == pUri)
		return FALSE;

	/* store the url */

	if(priv->pThumbRollerModel)
        {
		ThornburyModelIter *iter = thornbury_model_get_first_iter(priv->pThumbRollerModel);
		gint rowToFocus = 0;
		gint focusedRow = 0;
		
		GHashTable *propHash =  NULL;
		
		propHash = thornbury_get_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", NULL);
		focusedRow = g_value_get_int(g_hash_table_lookup(propHash, "focused-row"));

		while (!thornbury_model_iter_is_last (iter))
		{
			thornbury_model_iter_get(iter, 1, &value, -1);
			rowToFocus = thornbury_model_iter_get_row(iter);

			if(g_strcmp0(value, pUri) == 0)
			{
				//if(focusedRow != rowToFocus)
				{
					thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
					update_media_overlay(pAudioPlayer, rowToFocus, pUri);
					bFound = TRUE;
				}
				break;
			}
			iter = thornbury_model_iter_next (iter);
		}
		if(! bFound)
			update_media_overlay_text(pAudioPlayer, focusedRow, pUri);

		if(NULL != propHash)
		{
			g_hash_table_destroy(propHash);
			propHash = NULL;
		}
		if(iter)
			g_object_unref(iter);
	}
	return FALSE;
}

/*********************************************************************************************
 * Function: update_highlight_arrow
 * Description:  set playing item arrow for list item
 * Parameters: pAudioPlayer, row, pUri*
 * Return:   void
 ********************************************************************************************/
void update_highlight_arrow(FramptonAudioPlayer *pAudioPlayer, gint row, gchar *pUri)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        ThornburyModelIter *iter_master;
        gint focus = TRUE;
        GValue value = { 0 };
        GValue value2 = { 0 };
        GValue valueState = { 0 };
        GValue valuePrevState = { 0 };
        iter_master = thornbury_model_get_iter_at_row (priv->pSongRollerModel, row);

        if(NULL == iter_master)
                return;

        thornbury_model_iter_get(iter_master, SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, &focus, -1);

        g_value_init(&value, G_TYPE_BOOLEAN);
        g_value_set_boolean(&value, TRUE);

        g_value_init(&value2, G_TYPE_BOOLEAN);
        g_value_set_boolean(&value2,FALSE);

        g_value_init(&valueState, G_TYPE_BOOLEAN);
        g_value_set_boolean(&valueState, TRUE);

        g_value_init(&valuePrevState, G_TYPE_BOOLEAN);
        g_value_set_boolean(&valuePrevState, FALSE);

	if(priv->prevSongRow != -1)
        {
                thornbury_model_insert_value(priv->pSongRollerModel, priv->prevSongRow, SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, &value2);
		thornbury_model_insert_value(priv->pSongRollerModel, priv->prevSongRow, SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, &valuePrevState);
                DEBUG ("set false for row = %d", priv->prevSongRow);
        }

        thornbury_model_insert_value(priv->pSongRollerModel, row, SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, &value);
	thornbury_model_insert_value(priv->pSongRollerModel, row, SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, &valueState);
        DEBUG ("set true for row = %d", row);
        g_value_unset(&value);
        g_value_unset(&value2);
	g_value_unset(&valueState);
	g_value_unset(&valuePrevState);

        /* update the row */
        priv->prevSongRow = row;

	if(iter_master)
		g_object_unref(iter_master);
}

/*********************************************************************************************
 * Function: update_media_overlay
 * Description:  set media overlay for playing item
 * Parameters: pAudioPlayer, row, pUri*
 * Return:   void
 ********************************************************************************************/
void update_media_overlay(FramptonAudioPlayer *pAudioPlayer, gint row, gchar *pUri)
{
        FramptonAudioPlayerPrivate *priv;
        ThornburyModelIter *iter_master;
        gint focus = TRUE;
        GValue valueShow = { 0 };
        GValue valuePrevShow = { 0 };
        GValue valueState = { 0 };
        GValue valuePrevState = { 0 };
        priv = pAudioPlayer->priv;
        iter_master = thornbury_model_get_iter_at_row (priv->pThumbRollerModel, row);
        if(NULL == iter_master)
                return;

        thornbury_model_iter_get(iter_master, THUMB_ROLLER_OVERLAY_SHOW, &focus, -1);

        g_value_init(&valueShow, G_TYPE_BOOLEAN);
        g_value_set_boolean(&valueShow, TRUE);

        g_value_init(&valuePrevShow, G_TYPE_BOOLEAN);
        g_value_set_boolean(&valuePrevShow, FALSE);

        g_value_init(&valueState, G_TYPE_BOOLEAN);

	if(g_strcmp0(priv->pCurrentView, "ThumbView") == 0)
	{
		GHashTable *pHash =  NULL;
		gboolean state;

		pHash = thornbury_get_property(priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "play-state", NULL);
		state = g_value_get_boolean (g_hash_table_lookup (pHash, "play-state"));
		g_value_set_boolean(&valueState, state);
		//g_print("state = %d\n", state);
	}
	else
		g_value_set_boolean(&valueState, TRUE);

        g_value_init(&valuePrevState, G_TYPE_BOOLEAN);
        g_value_set_boolean(&valuePrevState, FALSE);
	
        if(priv->prevRow != -1)
        {
                thornbury_model_insert_value(priv->pThumbRollerModel, priv->prevRow, THUMB_ROLLER_OVERLAY_SHOW, &valuePrevShow);
		thornbury_model_insert_value(priv->pThumbRollerModel, priv->prevRow, THUMB_ROLLER_OVERLAY_STATE, &valuePrevState);
                DEBUG ("set false for row = %d", priv->prevRow);
        }
        thornbury_model_insert_value(priv->pThumbRollerModel, row, THUMB_ROLLER_OVERLAY_SHOW, &valueShow);
        thornbury_model_insert_value(priv->pThumbRollerModel, row, THUMB_ROLLER_OVERLAY_STATE, &valueState);
	/* update media overlay text */
	update_media_overlay_text(pAudioPlayer, row, pUri);

	DEBUG ("set media overlay for row = %d", row);

        g_value_unset(&valueShow);
        g_value_unset(&valuePrevShow);
	g_value_unset(&valuePrevState);
	g_value_unset(&valueState);
        /* update the row */
	priv->prevRow = row;
	
	if(iter_master)
		g_object_unref(iter_master);
}

/*********************************************************************************************
 * Function: update_media_overlay_text
 * Description:  set media overlay text 
 * Parameters: pAudioPlayer, row, pUri*
 * Return:   void
 ********************************************************************************************/
static void update_media_overlay_text(FramptonAudioPlayer *pAudioPlayer, gint row, gchar *pUri)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        GValue valueText = { 0 };
        g_value_init(&valueText, G_TYPE_STRING);
        DEBUG ("set media overlay text with uri %s", pUri);
        if(pUri)
        {
                /* FIXME: Add error handling */
                GrassmoorMediaInfo *newPlayingInfo = grassmoor_tracker_get_media_file_info (priv->tracker, pUri, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
                if (newPlayingInfo && grassmoor_media_info_get_title (newPlayingInfo))
                {
                        g_value_set_static_string(&valueText, g_strdup (grassmoor_media_info_get_title (newPlayingInfo)));
                        g_clear_pointer (&newPlayingInfo, grassmoor_media_info_free);
                }
                else
                        g_value_set_static_string(&valueText, "Unknown");
        }
        else
                        g_value_set_static_string(&valueText, "Unknown");

	DEBUG ("media overlay text = %s", g_value_get_string(&valueText));
	thornbury_model_insert_value(priv->pThumbRollerModel, row, THUMB_ROLLER_OVERLAY_TEXT, &valueText);

	g_value_unset(&valueText);
}

/*********************************************************************************************
 * Function: update_media_overlay_play_state
 * Description:  update media overlay state as play/pause
 * Parameters: state as TRUE/FALSE for play/pause
 * Return:   
 ********************************************************************************************/
void update_media_overlay_play_state(gboolean bState)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	GHashTable *propHash = NULL;
        gint row = 0;
        GValue value = { 0 };

	if(priv->prevRow >= 0)
		row = priv->prevRow;
	else
	{	
		propHash = thornbury_get_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", NULL);
		row = g_value_get_int(g_hash_table_lookup(propHash, "focused-row"));
	}
	
        g_value_init(&value, G_TYPE_BOOLEAN);
        g_value_set_boolean(&value, bState);

	thornbury_model_insert_value(priv->pThumbRollerModel, row, THUMB_ROLLER_OVERLAY_STATE, &value);

	g_value_unset(&value);
	if(NULL != propHash)
	{
		g_hash_table_destroy(propHash);
		propHash = NULL;
	}

}

/*********************************************************************************************
 * Function: update_highlight_arrow_state
 * Description:  update arrow state as play/pause
 * Parameters:  state as TRUE/FALSE for play/pause
 * Return:  
 ********************************************************************************************/
void update_highlight_arrow_state(gboolean bState)
{
        FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        GHashTable *propHash = NULL;
        gint row = 0;
        GValue value = { 0 };

        if(priv->prevSongRow >= 0)
                row = priv->prevSongRow;
        else
        {
                propHash = thornbury_get_property(priv->pThornburyViewManager, SONGS_ROLLER, "focused-row", NULL);
                row = g_value_get_int(g_hash_table_lookup(propHash, "focused-row"));
        }

        g_value_init(&value, G_TYPE_BOOLEAN);
        g_value_set_boolean(&value, bState);

        thornbury_model_insert_value(priv->pSongRollerModel, row, SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, &value);

        g_value_unset(&value);
        if(NULL != propHash)
        {
                g_hash_table_destroy(propHash);
                propHash = NULL;
        }

}

/*********************************************************************************************
 * Function: set_thumb_roller_item_to_focus
 * Description:  To bring first item to focus on the roller
 * Parameters:
 * Return:   gboolean(handled or not)
 ********************************************************************************************/
static gboolean
set_thumb_roller_item_to_focus (const gchar *pWidgetName)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	if(priv->bResetUI)
		return FALSE;

	if(NULL != pWidgetName && NULL != priv->pThornburyViewManager)
	{
		thornbury_set_property(priv->pThornburyViewManager, pWidgetName, "focused-row", priv->inMidIndex, NULL);
	}
    	return FALSE;
}

/*********************************************************************************************
 * Function: set_list_roller_item_to_focus
 * Description:  To bring first item to focus on the roller
 * Parameters:
 * Return:   gboolean(handled or not)
 ********************************************************************************************/
static gboolean
set_list_roller_item_to_focus (const gchar *pWidgetName)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	if(priv->bResetUI)
                return FALSE;

	if(NULL != pWidgetName && NULL != priv->pThornburyViewManager)
	{
		thornbury_set_property(priv->pThornburyViewManager, pWidgetName, "focused-row", priv->inMidIndex, NULL);
	}
    	return FALSE;
}

/*********************************************************************************************
 * Function: set_roller_focus
 * Description:  To bring first item to focus on the roller
 * Parameters:
 * Return:   gboolean(handled or not)
 ********************************************************************************************/
static gboolean set_roller_focus(gchar *pWidgetName)
{
        FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

        if(priv->bResetUI)
                return FALSE;

	set_thumb_roller_item_to_focus(THUMB_ROLLER);
	if(priv->enCurrentMode == FRAMPTON_AUDIO_PLAYER_MODE_SONGS)
		set_list_roller_item_to_focus(SONGS_ROLLER);
	else
		set_list_roller_item_to_focus(ARTIST_ALBUM_ROLLER);

	return FALSE;
}

/*********************************************************************************************
 * Function: create_artist_album_roller_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_artist_album_roller_model (void)
{
	ThornburyModel *model = NULL;
        model = (ThornburyModel*)thornbury_list_model_new (ARTISTALBUM_ROLLER_COL_LAST,
					G_TYPE_STRING, NULL,
					G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
					G_TYPE_STRING, NULL,
					G_TYPE_OBJECT, NULL,
                                        -1);
        return model;
}

/*********************************************************************************************
 * Function: create_song_roller_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_song_roller_model (void)
{
	ThornburyModel *model = NULL;

	 model = (ThornburyModel*)thornbury_list_model_new (SONGS_ROLLER_COLUMN_LAST,
					G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
					G_TYPE_BOOLEAN, NULL,
					G_TYPE_BOOLEAN, NULL,
					G_TYPE_INT, NULL,
                                        -1);
        return model;
}

/*********************************************************************************************
 * Function: create_thumb_roller_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_thumb_roller_model (void)
{
	ThornburyModel *model = NULL;

	model = (ThornburyModel*)thornbury_list_model_new (THUMB_ROLLER_COLUMN_LAST,
                                        //COGL_TYPE_HANDLE, NULL,
					G_TYPE_OBJECT, NULL,
					G_TYPE_STRING, NULL,
					G_TYPE_BOOLEAN, NULL,
					G_TYPE_BOOLEAN, NULL,
					G_TYPE_STRING, NULL,
					-1);
        return model;
}

/*********************************************************************************************
 * Function: create_meta_roller_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_meta_roller_model (void)
{
	ThornburyModel *model = NULL;

	model = (ThornburyModel*)thornbury_list_model_new (SONGS_ROLLER_COLUMN_LAST,
					G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
					G_TYPE_BOOLEAN, NULL,
					G_TYPE_BOOLEAN, NULL,
					G_TYPE_INT, NULL,
                                        -1);
        return model;
}

/*********************************************************************************************
 * Function: create_bottom_bar_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *create_bottom_bar_model(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE (pAudioPlayer);
	ThornburyModel *model = NULL;
	gchar *pIconShuffleAC = NULL;
	gchar *pIconShuffleIN = NULL;
	gchar *pIconRepeatAC = NULL;
	gchar *pIconRepeatIN = NULL;

	model = (ThornburyModel*)thornbury_list_model_new (BOTTOM_BAR_LAST,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          G_TYPE_STRING, NULL,
                                          -1);

	pIconShuffleAC = g_strconcat(priv->pDataDir,"/icon_shuffle_AC.png", NULL);
	pIconShuffleIN = g_strconcat(priv->pDataDir,"/icon_shuffle_IN.png", NULL);
	pIconRepeatAC = g_strconcat(priv->pDataDir,"/icon_repeat_AC.png", NULL);
	pIconRepeatIN = g_strconcat(priv->pDataDir,"/icon_repeat_IN.png", NULL);
	thornbury_model_append(model,
			BOTTOM_BAR_ONE_ACTIVE, pIconShuffleAC,
			BOTTOM_BAR_ONE_INACTIVE, pIconShuffleIN,
			BOTTOM_BAR_ONE_TEXTURE_NAME, SHUFFLE,
			BOTTOM_BAR_TWO_ACTIVE, pIconRepeatAC,
			BOTTOM_BAR_TWO_INACTIVE, pIconRepeatIN,
			BOTTOM_BAR_TWO_TEXTURE_NAME, REPEAT,
			BOTTOM_BAR_THREE_ACTIVE, NULL,
			BOTTOM_BAR_THREE_INACTIVE, NULL,
			BOTTOM_BAR_THREE_TEXTURE_NAME, NULL,
			BOTTOM_BAR_FOUR, "0:00",
			-1);

	frampton_audio_player_free_str(pIconShuffleAC);
	frampton_audio_player_free_str(pIconShuffleIN);
	frampton_audio_player_free_str(pIconRepeatAC);
	frampton_audio_player_free_str(pIconRepeatIN);

        return model;
}

/*********************************************************************************************
 * Function: create_info_header_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *create_info_header_model(FramptonAudioPlayer *pAudioPlayer, gchar *pIcon)
{
	
	//FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE(pAudioPlayer);
	ThornburyModel *model = NULL;

        model = (ThornburyModel*)thornbury_list_model_new (INFO_ROLLER_HEADER_NONE,
                                        G_TYPE_POINTER, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        -1);

        thornbury_model_append (model,
                                        INFO_ROLLER_HEADER_LEFT_ICON_TEXT, pIcon,
                                        INFO_ROLLER_HEADER_MID_TEXT, NULL,
                                        INFO_ROLLER_HEADER_RIGHT_ICON_TEXT, NULL,
                                         -1);
	return model;
}

/*********************************************************************************************
 * Function: create_info_roller_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *create_info_roller_model(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE (pAudioPlayer);
	ThornburyModel *model = NULL;
	gchar *pIconPath = NULL;

	model = (ThornburyModel*)thornbury_list_model_new (SONGS_ROLLER_COLUMN_LAST,
					G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
					G_TYPE_BOOLEAN, NULL,
					G_TYPE_BOOLEAN, NULL,
					G_TYPE_INT, NULL,
                                        -1);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_duration.png", NULL);
	thornbury_model_append (model,
                                        SONGS_ROLLER_COLUMN_ID, NULL,
                                        SONGS_ROLLER_COLUMN_ICON, pIconPath,
                                        SONGS_ROLLER_COLUMN_LABEL, "Unknown",
					SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
					SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
                                         -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_released.png", NULL);
	thornbury_model_append (model,
                                        SONGS_ROLLER_COLUMN_ID, NULL,
                                        SONGS_ROLLER_COLUMN_ICON, pIconPath,
                                        SONGS_ROLLER_COLUMN_LABEL, "Unknown",
					SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
					SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
                                         -1);
	frampton_audio_player_free_str(pIconPath);
	
	pIconPath = g_strconcat(priv->pDataDir, "/icon_no.ofCDs.png", NULL);
	thornbury_model_append (model,
                                        SONGS_ROLLER_COLUMN_ID, NULL,
                                        SONGS_ROLLER_COLUMN_ICON, pIconPath,
                                        SONGS_ROLLER_COLUMN_LABEL, "Unknown",
					SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
					SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
                                         -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_artists.png", NULL);
	thornbury_model_append (model,
                                        SONGS_ROLLER_COLUMN_ID, NULL,
                                        SONGS_ROLLER_COLUMN_ICON, pIconPath,
                                        SONGS_ROLLER_COLUMN_LABEL, "Unknown",
					SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
					SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
                                         -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_songs.png", NULL);
	thornbury_model_append (model,
                                        SONGS_ROLLER_COLUMN_ID, NULL,
                                        SONGS_ROLLER_COLUMN_ICON, pIconPath,
                                        SONGS_ROLLER_COLUMN_LABEL, "Unknown",
					SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
					SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
                                         -1);
	frampton_audio_player_free_str(pIconPath);
        return model;
}

/*********************************************************************************************
 * Function: create_artist_album_roller_footer_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel * create_artist_album_roller_footer_model(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	ThornburyModel *model = NULL;
	const gchar *pMidText = NULL;
	const gchar *pRightText = NULL;
	gboolean bArrow = TRUE;
	gchar *pIconPath;

        model = (ThornburyModel*)thornbury_list_model_new (ARTISTALBUM_LAST,
					G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);

	if(FRAMPTON_AUDIO_PLAYER_MODE_ARTIST == priv->enCurrentMode)
	{
		pMidText = FRAMPTON_AUDIO_PLAYER_ARTISTS;
		pRightText = FRAMPTON_AUDIO_PLAYER_ALBUM;
	}
	else
	{
		pMidText = FRAMPTON_AUDIO_PLAYER_ALBUM;
		pRightText = FRAMPTON_AUDIO_PLAYER_ARTISTS;
	}

	if(FRAMPTON_AUDIO_PLAYER_MODE_SONGS == priv->enCurrentMode)
		bArrow = FALSE;

	pIconPath = g_strconcat (priv->pDataDir, "/icon_pictures_albums_IN.png", NULL);
	thornbury_model_append (model,
                                        ARTISTALBUM_FOOTER_NAME, "footer",
                                        ARTISTALBUM_FOOTER, TRUE,
                                        ARTISTALBUM_ICON_NAME, "icon",
                                        ARTISTALBUM_ICON, pIconPath,
                                        ARTISTALBUM_MID_TEXT_NAME, "mid-text",
                                        ARTISTALBUM_MID_TEXT, pMidText,
                                        ARTISTALBUM_RIGHT_TEXT_NAME, "right-text",
                                        ARTISTALBUM_RIGHT_TEXT, pRightText,
                                        ARTISTALBUM_ARROW_UP_NAME, "arrow-up",
                                        ARTISTALBUM_ARROW_UP, TRUE,
					ARTISTALBUM_SHOW_ARROW_NAME, "show-arrow",
					ARTISTALBUM_SHOW_ARROW, bArrow,
                                        -1);

	frampton_audio_player_free_str(pIconPath);
	return model;
}

/*********************************************************************************************
 * Function: create_songs_roller_footer_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *create_songs_roller_footer_model(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	ThornburyModel *model = NULL;
	gboolean bArrow = FALSE;
	gchar *pIconPath;


        model = (ThornburyModel*)thornbury_list_model_new (SONGS_ROLLER_LAST,
					G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
					G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);

	if(FRAMPTON_AUDIO_PLAYER_MODE_SONGS == priv->enCurrentMode)
	{
		bArrow = TRUE;
	}

	pIconPath = g_strconcat (priv->pDataDir, "/icon_music_songs_IN.png", NULL);
	thornbury_model_append (model,
				SONGS_ROLLER_FOOTER_NAME, "footer",
			        SONGS_ROLLER_FOOTER, TRUE,
			        SONGS_ROLLER_ICON_NAME, "icon",
			        SONGS_ROLLER_ICON, pIconPath,
			        SONGS_ROLLER_TEXT_NAME, "text",
			        SONGS_ROLLER_TEXT, FRAMPTON_AUDIO_PLAYER_SONGS,
			        SONGS_ROLLER_ARROW_UP_NAME, "arrow-up",
			        SONGS_ROLLER_ARROW_UP, TRUE,
				SONGS_ROLLER_SHOW_ARROW_NAME, "show-arrow",
				SONGS_ROLLER_SHOW_ARROW, bArrow,
        			-1);

	frampton_audio_player_free_str(pIconPath);
	return model;
}


/*********************************************************************************************
 * Function: create_sort_roller_model
 * Description:
 * Parameters:
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_sort_roller_model (void)
{
	ThornburyModel *model;
	int i;
	gchar *pNum = NULL;
	gchar *pLabel = NULL;

	model = THORNBURY_MODEL (thornbury_list_model_new (SORT_ROLLER_COLUMN_LAST,
			G_TYPE_STRING, NULL,
			COGL_TYPE_HANDLE, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL, 
			G_TYPE_INT, NULL,
			-1));

	/* alphabet from A to Z */
	i = 65;
	while (i <= 90)
	{
		pNum = g_strdup_printf("item number %c", i);
		pLabel = g_strdup_printf("%c", i);

		thornbury_model_append(model,
				SORT_ROLLER_COLUMN_NAME, pNum,
				SORT_ROLLER_COLUMN_LABEL, pLabel,
				SORT_ROLLER_COLUMN_TYPE, "alphabet",
				SORT_ROLLER_COLUMN_FONT_SIZE, 32,
				-1);

		g_free(pNum);
		g_free(pLabel);

		i++;
	}
	return model;
}

/*********************************************************************************************
 * Function:    create_thumb_views_drawer_model
 * Description:
 * Parameters:
 * Return:      ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *create_thumb_views_drawer_model(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE (pAudioPlayer);
	ThornburyModel *model = NULL;
	gchar *pIconPath = NULL;

        model = (ThornburyModel*)thornbury_list_model_new (DRAWER_COLUMN_LAST,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_view_details.png", NULL);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, "DETAIL-VIEW",
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "DETAIL",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_view_listwiththumbs_IN.png", NULL);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, "LIST-VIEW",
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "LIST",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);


	pIconPath = g_strconcat(priv->pDataDir, "/icon_view_thumbs.png", NULL);
        thornbury_model_append (model,  DRAWER_COLUMN_NAME, "THUMB",
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "THUMB",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);

	frampton_audio_player_free_str(pIconPath);

        return model;
}

/*********************************************************************************************
 * Function:    create_detail_views_drawer_model
 * Description:
 * Parameters:
 * Return:      ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *create_detail_views_drawer_model(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE (pAudioPlayer);
	ThornburyModel *model = NULL;
	gchar *pIconPath = NULL;

        model = (ThornburyModel*)thornbury_list_model_new (DRAWER_COLUMN_LAST,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_view_thumbs.png", NULL);
        thornbury_model_append (model,  DRAWER_COLUMN_NAME, "THUMB",
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "THUMB",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_view_details.png", NULL);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, "DETAIL-VIEW",
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "DETAIL",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_view_listwiththumbs_IN.png", NULL);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, "LIST-VIEW",
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "LIST",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	
	frampton_audio_player_free_str(pIconPath);
        return model;
}

/*********************************************************************************************
 * Function:    create_list_context_drawer_model
 * Description:
 * Parameters:  void
 * Return:      ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *create_list_context_drawer_model(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE (pAudioPlayer);
	ThornburyModel *model = NULL;
	gchar *pIconPath = NULL;

        model = (ThornburyModel*)thornbury_list_model_new (DRAWER_COLUMN_LAST,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_search.png", NULL);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, SEARCH,
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "SEARCH",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_trash.png", NULL);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, DELETE,
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "DELETE",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_youtube_share_all_AC.png", NULL);
        thornbury_model_append (model,  DRAWER_COLUMN_NAME, SHARE,
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "SHARE",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_related.png", NULL);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, RELATED,
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "RELATED",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);

	pIconPath = g_strconcat(priv->pDataDir, "/icon_playlists.png", NULL);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, TO_PLAYLIST,
                                        DRAWER_COLUMN_ICON, pIconPath,
                                        DRAWER_COLUMN_TOOLTIP_TEXT, "TO PLAYLIST",
                                        DRAWER_COLUMN_REACTIVE, TRUE,
                                        -1);
	frampton_audio_player_free_str(pIconPath);

        return model;
}

/*********************************************************************************************
 * Function:    reposition_list_rollers
 * Description: Add elements to the list view of audio player
 * Parameters:  Pointer to the audio player element
 * Return:      void
 ********************************************************************************************/
void reposition_list_rollers (FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

        /* Add elements to the list view. The positioning of the rollers are
         * different for different sorting orders*/
        if(FRAMPTON_AUDIO_PLAYER_MODE_SONGS != priv->enCurrentMode)
        {
		DEBUG ("artist album roller");
        	thornbury_set_property(priv->pThornburyViewManager, ARTIST_ALBUM_ROLLER, "x", 0.0, NULL);
		thornbury_set_property(priv->pThornburyViewManager, SONGS_ROLLER, "x", 416.0, NULL);
		//g_object_set(priv->pAlbumArtistRoller, "x", 0.0, "y", 0.0, NULL);
                //g_object_set(priv->pSongRoller, "x", 414.0, "y", 0.0, NULL);
        }
        else
        {
		thornbury_set_property(priv->pThornburyViewManager, ARTIST_ALBUM_ROLLER, "x", 246.0, NULL);
		thornbury_set_property(priv->pThornburyViewManager, SONGS_ROLLER, "x", 0.0, NULL);
                //g_object_set(priv->pAlbumArtistRoller, "x", 244.0, "y", 0.0, NULL);
                //g_object_set(priv->pSongRoller, "x", 0.0, "y", 0.0, NULL);
        }
}

/*********************************************************************************************
 * Function:    create_views_drawer_signals
 * Description: signal hash for views drawer
 * Parameters:  none
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_views_drawer_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
        GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;

        ThornburyViewManagerSignalData *sig_data = g_new0(ThornburyViewManagerSignalData,1);
        /* FIXME: Make Thornbury const-correct so that we don't need casts
         * like this */
        sig_data->signalname = (gchar *) "drawer-button-released";
        sig_data->func = G_CALLBACK(views_drawer_button_released_cb);
        sig_data->data = pAudioPlayer;

        lstsignals = g_list_append(lstsignals,sig_data);

        g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;
}

/*********************************************************************************************
 * Function:    create_context_drawer_signals
 * Description: signal hash for context drawer
 * Parameters:  none
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_context_drawer_signals(const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
        GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;

        ThornburyViewManagerSignalData *sig_data = g_new0(ThornburyViewManagerSignalData,1);
        sig_data->signalname = (gchar *) "drawer-button-released";
        sig_data->func = G_CALLBACK(list_context_drawer_button_released_cb);
        sig_data->data = pAudioPlayer;

        lstsignals = g_list_append(lstsignals,sig_data);

        g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);


        return signals;
}

/*********************************************************************************************
 * Function:    create_artist_album_roller_signals
 * Description: signal hash for artist-album roller items
 * Parameters:  pWidgetName* , pAudioPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_artist_album_roller_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;
        ThornburyViewManagerSignalData *sig_data1;
        ThornburyViewManagerSignalData *sig_data2;

        sig_data1 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data1->signalname = (gchar *) "roller-item-activated",
	sig_data1->func = G_CALLBACK(albumartist_roller_item_activated_cb),
	sig_data1->data = pAudioPlayer;

	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data1);

        sig_data2 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data2->signalname = (gchar *) "roller-locking-finished",
        sig_data2->func = G_CALLBACK(albumartist_roller_locking_finished_cb),
        sig_data2->data = pAudioPlayer;

        /* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data2);

        g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;
}

/*********************************************************************************************
 * Function:    create_songs_roller_signals
 * Description: signal hash for artist-album roller items
 * Parameters:  pWidgetName* , pAudioPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_songs_roller_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;
        ThornburyViewManagerSignalData *sig_data1;
        ThornburyViewManagerSignalData *sig_data2;

        sig_data1 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data1->signalname = (gchar *) "roller-item-activated",
	sig_data1->func = G_CALLBACK(song_roller_item_activated_cb),
	sig_data1->data = pAudioPlayer;

	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data1);

        sig_data2 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data2->signalname = (gchar *) "roller-locking-finished",
        sig_data2->func = G_CALLBACK(song_roller_locking_finished_cb),
        sig_data2->data = pAudioPlayer;

	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data2);

	/* add list to hash */
	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;
}

/*********************************************************************************************
 * Function:    create_thumb_roller_signals
 * Description: signal hash for thumb roller items
 * Parameters:  pWidgetName* , pAudioPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_thumb_roller_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;
        ThornburyViewManagerSignalData *sig_data1;
        ThornburyViewManagerSignalData *sig_data3;
        ThornburyViewManagerSignalData *sig_data4;

        sig_data1 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data1->signalname = (gchar *) "roller-item-activated",
	sig_data1->func = G_CALLBACK(thumb_roller_item_activated_cb),
	sig_data1->data = pAudioPlayer;

	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data1);
# if 0
	ThornburyViewManagerSignalData *sig_data2 = g_new0(ThornburyViewManagerSignalData,1);
        sig_data2->signalname = "roller-locking-finished",
        sig_data2->func = G_CALLBACK(thumb_roller_locking_finished_cb),
        sig_data2->data = pAudioPlayer;

	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data2);
# endif
        sig_data3 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data3->signalname = (gchar *) "roller-scroll-started",
        sig_data3->func = G_CALLBACK(frampton_audio_player_roller_scroll_started),
        sig_data3->data = pAudioPlayer;

        lstsignals = g_list_append(lstsignals, sig_data3);

        sig_data4 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data4->signalname = (gchar *) "roller-locking-finished",
        sig_data4->func = G_CALLBACK(frampton_audio_player_roller_scroll_completed),
        sig_data4->data = pAudioPlayer;

        lstsignals = g_list_append(lstsignals, sig_data4);

	/* add list to hash */
        g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;
}

/*********************************************************************************************
 * Function:    create_sort_roller_signals
 * Description: signal hash for sort roller items
 * Parameters:  pWidgetName* , pAudioPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_sort_roller_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
        GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;
        ThornburyViewManagerSignalData *sig_data1;
        ThornburyViewManagerSignalData *sig_data2;

        sig_data1 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data1->signalname = (gchar *) "roller-item-activated",
        sig_data1->func = G_CALLBACK(sort_roller_item_activated_cb),
        sig_data1->data = pAudioPlayer;

	 /* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data1);

        sig_data2 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data2->signalname = (gchar *) "roller-locking-finished",
        sig_data2->func = G_CALLBACK(sort_roller_locking_finished_cb),
        sig_data2->data = pAudioPlayer;

        /* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data2);

	 /* add list to hash */
        g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;

}

/*********************************************************************************************
 * Function:    create_bottom_bar_signals
 * Description: signal hash for bottom bar
 * Parameters:  pWidgetName* , pAudioPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_bottom_bar_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
        GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;
        ThornburyViewManagerSignalData *sig_data1;
        ThornburyViewManagerSignalData *sig_data2;

        sig_data1 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data1->signalname = (gchar *) "action-press",
        sig_data1->func = G_CALLBACK(bottom_bar_pressed),
        sig_data1->data = pAudioPlayer;
        /* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data1);

        sig_data2 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data2->signalname = (gchar *) "action-release",
        sig_data2->func = G_CALLBACK(bottom_bar_released),
        sig_data2->data = pAudioPlayer;
        /* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data2);

	/* add list to hash */
        g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;
}

/*********************************************************************************************
 * Function:    create_progress_bar_signals
 * Description: signal hash for progress bar
 * Parameters:  pWidgetName* , pAudioPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_progress_bar_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;
        ThornburyViewManagerSignalData *sig_data1;
        ThornburyViewManagerSignalData *sig_data2;
        ThornburyViewManagerSignalData *sig_data3;
        ThornburyViewManagerSignalData *sig_data4;

        sig_data1 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data1->signalname = (gchar *) "play-requested",
	sig_data1->func = G_CALLBACK(progress_bar_play_requested_cb),
	sig_data1->data = pAudioPlayer;
	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data1);

        sig_data2 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data2->signalname = (gchar *) "pause-requested",
	sig_data2->func = G_CALLBACK(progress_bar_pause_requested_cb),
	sig_data2->data = pAudioPlayer;
	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data2);

        sig_data3 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data3->signalname = (gchar *) "seek-start",
	sig_data3->func = G_CALLBACK(progress_bar_seek_started_cb),
	sig_data3->data = pAudioPlayer;
	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data3);

        sig_data4 = g_new0 (ThornburyViewManagerSignalData, 1);
        sig_data4->signalname = (gchar *) "seek-end",
	sig_data4->func = G_CALLBACK(progress_bar_seek_updated_cb),
	sig_data4->data = pAudioPlayer;

	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data4);

	/* add list to hash */
        g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;
}

/*********************************************************************************************
 * Function:    create_meta_roller_signals
 * Description: signal hash for meta roller items
 * Parameters:  pWidgetName* , pAudioPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_meta_roller_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;

        ThornburyViewManagerSignalData *sig_data1 = g_new0(ThornburyViewManagerSignalData,1);
        sig_data1->signalname = (gchar *) "roller-item-activated",
	sig_data1->func = G_CALLBACK(meta_roller_item_activated_cb),
	sig_data1->data = pAudioPlayer;

	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data1);

	/* add list to hash */
	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;
}

/*********************************************************************************************
 * Function:    create_info_roller_signals
 * Description: signal hash for info roller items
 * Parameters:  pWidgetName* , pAudioPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_info_roller_signals (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
        GList *lstsignals = NULL;

        ThornburyViewManagerSignalData *sig_data1 = g_new0(ThornburyViewManagerSignalData,1);
        sig_data1->signalname = (gchar *) "info-roller-item-selected",
	sig_data1->func = G_CALLBACK(info_roller_item_selected_cb),
	sig_data1->data = pAudioPlayer;

	/* append signal to list */
        lstsignals = g_list_append(lstsignals, sig_data1);

	/* add list to hash */
	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

        return signals;
}

/*********************************************************************************************
 * Function:    sort_roller_add_attributtes
 * Description: add attributes to sort roller
 * Parameters:  none
 * Return:      void
 ********************************************************************************************/
static void
sort_roller_add_attributtes (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{

	if(NULL != pWidgetName)
	{
		FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
		thornbury_set_property(priv->pThornburyViewManager, pWidgetName,
					"label", SORT_ROLLER_COLUMN_LABEL,
					"icon", SORT_ROLLER_COLUMN_ICON,
					"column-type", SORT_ROLLER_COLUMN_TYPE,
					"font-size", SORT_ROLLER_COLUMN_FONT_SIZE,
					NULL);

	}
}


static GHashTable *
create_animations (const gchar *pWidgetName,
    FramptonAudioPlayer *pAudioPlayer)
{
	GHashTable *pHash = g_hash_table_new (g_str_hash, g_str_equal);
	ThornburyViewManagerViewSwitchAnimData *pVMAnimData;
# if 0
	ThornburyViewManagerAnimData *pVMAnimData = g_new0(ThornburyViewManagerAnimData, 1);
	pVMAnimData->view_name = "ThumbView";
	//pVMAnimData->signal_name = "roller-item-activated";
	//pVMAnimData->effect = "cover_to_detail",
	pVMAnimData->exit_effect = "cover-to-detail",
	pVMAnimData->entry_effect = NULL;//"detail-to-cover";
	pVMAnimData->stage = 1;

	GList *param = NULL;
	param = g_list_append(param, &(pVMAnimData->stage) );
	pVMAnimData->anim_params = param;

	g_hash_table_insert(pHash, pWidgetName, pVMAnimData);
# endif

	pVMAnimData = g_new0 (ThornburyViewManagerViewSwitchAnimData, 1);
	pVMAnimData->view_name = (gchar *) "ThumbView";
	pVMAnimData->exit_effect = (gchar *) "cover_to_detail";
	pVMAnimData->entry_effect = (gchar *) "detail_to_cover";

	g_hash_table_insert (pHash, (gchar *) "ThumbView", pVMAnimData);
	return pHash;
	
	
}

/*********************************************************************************************
 * Function:    create_list_view
 * Description: list view creation
 * Parameters:  Pointer to the audio player
 * Return:      void
 ********************************************************************************************/
static void create_list_view(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv;
        ThornburyModel *pContextModel;
        ThornburyModel *pViewsModel;
        GHashTable *pContextSignalHash;
        GHashTable *pViewsSignalHash;
        GHashTable *pArtistAlbumHash;
        GHashTable *pSongsHash;
        ThornburyModel *pArtistAlbumFooterModel;
        ThornburyModel *pSongsFooterModel;

        if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return;

        priv = pAudioPlayer->priv;

        /* add context drawer model */
        pContextModel = create_list_context_drawer_model (pAudioPlayer);
        g_hash_table_insert (priv->pListModelHash,
            (gchar *) LIST_CONTEXT_DRAWER, pContextModel);

# if 1
        /* add views drawer model */
        pViewsModel = create_thumb_views_drawer_model (pAudioPlayer);
        g_hash_table_insert (priv->pListModelHash,
            (gchar *) LIST_VIEWS_DRAWER, pViewsModel);

# endif
        /* add sort roller model */
        //ThornburyModel *pSortRollerModel = create_sort_roller_model();
        //g_hash_table_insert(priv->pListModelHash, LIST_SORT_ROLLER, pSortRollerModel);

        /* add album-artist roller model */
        priv->pAlbumArtistRollerModel = create_artist_album_roller_model();
        g_hash_table_insert (priv->pListModelHash,
            (gchar *) ARTIST_ALBUM_ROLLER, priv->pAlbumArtistRollerModel);

	/* add songs roller model */
        priv->pSongRollerModel = create_song_roller_model();
        g_hash_table_insert (priv->pListModelHash,
            (gchar *) SONGS_ROLLER, priv->pSongRollerModel);

        /* set model hash to view manager */
        thornbury_set_widgets_models(priv->pThornburyViewManager, priv->pListModelHash);

        /* add signals */
        pContextSignalHash = create_context_drawer_signals (LIST_CONTEXT_DRAWER, pAudioPlayer);
        pViewsSignalHash = create_views_drawer_signals (LIST_VIEWS_DRAWER, pAudioPlayer);
        pArtistAlbumHash = create_artist_album_roller_signals (ARTIST_ALBUM_ROLLER, pAudioPlayer);
        pSongsHash = create_songs_roller_signals (SONGS_ROLLER, pAudioPlayer);

        /* set signals to view manager */
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pContextSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pViewsSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pArtistAlbumHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pSongsHash);

	/* add attributes to list sort roller */
	//sort_roller_add_attributtes(LIST_SORT_ROLLER, pAudioPlayer);

	/* initial focus for sort roller */
	//g_timeout_add(800, (GSourceFunc)set_sort_roller_item_to_focus, LIST_SORT_ROLLER);

	/* attributes to add in list rollers */
	thornbury_set_property(priv->pThornburyViewManager, ARTIST_ALBUM_ROLLER,
					//"icon", ARTISTALBUM_ROLLER_COL_ICON,
					"mid-text", ARTISTALBUM_ROLLER_COL_MID_TEXT,
					"right-text", ARTISTALBUM_ROLLER_COL_RIGHT_TEXT,
					"image-data", ARTISTALBUM_ROLLER_COL_CONTENT,
					NULL);
	/* add attributes to songs roller */
	thornbury_set_property(priv->pThornburyViewManager, SONGS_ROLLER,
					"number", SONGS_ROLLER_COLUMN_NUM,
					"text", SONGS_ROLLER_COLUMN_LABEL,
					"highlight-show", SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW,
					"highlight-arrow", SONGS_ROLLER_HIGHLIGHT_ARROW_STATE,
					NULL);

	/* The positioning of the audio player rollers depends on the
	 * configuration with which wwe have launched it */
	reposition_list_rollers(pAudioPlayer);

	/* add list roller footers: artist-album roller footer */
	pArtistAlbumFooterModel = create_artist_album_roller_footer_model (pAudioPlayer);
	thornbury_set_property(priv->pThornburyViewManager, ARTIST_ALBUM_ROLLER, "footer-model", pArtistAlbumFooterModel, NULL);
	/* songs footer */
	pSongsFooterModel = create_songs_roller_footer_model (pAudioPlayer);
	thornbury_set_property(priv->pThornburyViewManager, SONGS_ROLLER, "footer-model", pSongsFooterModel, NULL);

    if(pContextSignalHash != NULL)
                g_hash_table_destroy(pContextSignalHash);
    if(pViewsSignalHash != NULL)
                g_hash_table_destroy(pViewsSignalHash);
	if(pArtistAlbumHash)
		g_hash_table_destroy(pArtistAlbumHash);
	if(pSongsHash)
		g_hash_table_destroy(pSongsHash);

}

/*********************************************************************************************
 * Function:    create_thumb_view
 * Description: thumb view creation
 * Parameters:  Pointer to the audio player
 * Return:      void
 ********************************************************************************************/
static void create_thumb_view(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv;
        ThornburyModel *pViewsModel;
        GHashTable *pViewsSignalHash;
        GHashTable *pThumbSignalHash;
        GHashTable *pSortSignalHash;
        GHashTable *pThumbAnim;

        if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return;

        priv = pAudioPlayer->priv;

        /* add views drawer model */
        pViewsModel = create_thumb_views_drawer_model (pAudioPlayer);
        g_hash_table_insert (priv->pThumbModelHash,
            (gchar *) THUMB_VIEWS_DRAWER, pViewsModel);

        /* add sort roller model */
        priv->pSortRollerModel = create_sort_roller_model ();
        g_hash_table_insert (priv->pThumbModelHash,
            (gchar *) THUMB_SORT_ROLLER, priv->pSortRollerModel);

	/* add thumb roller model */
        priv->pThumbRollerModel = create_thumb_roller_model();
        g_hash_table_insert (priv->pThumbModelHash,
            (gchar *) THUMB_ROLLER, priv->pThumbRollerModel);

        /* set model hash to view manager */
        thornbury_set_widgets_models(priv->pThornburyViewManager, priv->pThumbModelHash);

        /* add signals */
        pViewsSignalHash = create_views_drawer_signals (THUMB_VIEWS_DRAWER, pAudioPlayer);
        pThumbSignalHash = create_thumb_roller_signals (THUMB_ROLLER, pAudioPlayer);
        pSortSignalHash = create_sort_roller_signals (THUMB_SORT_ROLLER, pAudioPlayer);
        pThumbAnim = create_animations (THUMB_ROLLER, pAudioPlayer);

        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pViewsSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pThumbSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pSortSignalHash);
        thornbury_set_view_switch_animation(priv->pThornburyViewManager, pThumbAnim);

	/* add sort rollerattributes */
        sort_roller_add_attributtes(THUMB_SORT_ROLLER, pAudioPlayer);

	 /* initial focus for sort roller */
         //g_timeout_add(800, (GSourceFunc)set_sort_roller_item_to_focus, THUMB_SORT_ROLLER);

	 /* attributes to add in thumb rollers */
        thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER,
				"media-state", THUMB_ROLLER_OVERLAY_STATE,
				"show", THUMB_ROLLER_OVERLAY_SHOW,
				"image-content", THUMB_ROLLER_ICON,
				"overlay-text", THUMB_ROLLER_OVERLAY_TEXT,
				//"cogl-texture", THUMB_ROLLER_ICON,
				//"width", THUMB_ROLLER_WIDTH,
                                //"height", THUMB_ROLLER_HEIGHT,
				NULL);

	if(pViewsSignalHash)
		g_hash_table_destroy(pViewsSignalHash);
	if(pThumbSignalHash)
		g_hash_table_destroy(pThumbSignalHash);
	if(pSortSignalHash)
		g_hash_table_destroy(pSortSignalHash);
	//if(pThumbAnim)
	//	g_free(pThumbAnim);

}

/*********************************************************************************************
 * Function:    create_detail_view
 * Description: detail view creation
 * Parameters:  Pointer to the audio player
 * Return:      void
 ********************************************************************************************/
static void create_detail_view(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv;
        ThornburyModel *pContextModel;
        ThornburyModel *pViewsModel;
        GHashTable *pContextSignalHash;
        GHashTable *pViewsSignalHash;
        GHashTable *pMetaSignalHash;
        GHashTable *pInfoSignalHash;
        GHashTable *pProgressBarSignalHash;
        GHashTable *pBottomBarSignalHash;
        gchar *pIcon;

        if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return;

        priv = pAudioPlayer->priv;

# if 1
        /* add context drawer model */
        pContextModel = create_list_context_drawer_model (pAudioPlayer);
        g_hash_table_insert (priv->pDetailModelHash,
            (gchar *) DETAIL_CONTEXT_DRAWER, pContextModel);

        /* add views drawer model */
        pViewsModel = create_detail_views_drawer_model (pAudioPlayer);
        g_hash_table_insert (priv->pDetailModelHash,
            (gchar *) DETAIL_VIEWS_DRAWER, pViewsModel);

# endif
        /* add sort roller model */
        //ThornburyModel *pSortRollerModel = create_sort_roller_model();
        //g_hash_table_insert(priv->pDetailModelHash, DETAIL_SORT_ROLLER, pSortRollerModel);

        /* add info roller model */
        priv->pInfoRollerModel = create_info_roller_model(pAudioPlayer);
        g_hash_table_insert (priv->pDetailModelHash,
            (gchar *) DETAIL_INFO_ROLLER, priv->pInfoRollerModel);

        /* add meta roller model */
        priv->pMetaRollerModel = create_meta_roller_model();
        g_hash_table_insert (priv->pDetailModelHash,
            (gchar *) DETAIL_META_ROLLER, priv->pMetaRollerModel);

	priv->pBottomBarModel = create_bottom_bar_model(pAudioPlayer);
        g_hash_table_insert (priv->pDetailModelHash,
            (gchar *) DETAIL_BOTTOMBAR, priv->pBottomBarModel);

        /* set model hash to view manager */
        thornbury_set_widgets_models(priv->pThornburyViewManager, priv->pDetailModelHash);

	/* add signals */
        pContextSignalHash = create_context_drawer_signals (DETAIL_CONTEXT_DRAWER, pAudioPlayer);
        pViewsSignalHash = create_views_drawer_signals (DETAIL_VIEWS_DRAWER, pAudioPlayer);
        pMetaSignalHash = create_meta_roller_signals (DETAIL_META_ROLLER, pAudioPlayer);
        pInfoSignalHash = create_info_roller_signals (DETAIL_INFO_ROLLER, pAudioPlayer);
        pProgressBarSignalHash = create_progress_bar_signals (DETAIL_PROGRESSBAR, pAudioPlayer);
        pBottomBarSignalHash = create_bottom_bar_signals (DETAIL_BOTTOMBAR, pAudioPlayer);

        /* set signals to view manager */
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pContextSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pViewsSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pMetaSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pInfoSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pProgressBarSignalHash);
        thornbury_set_widgets_controllers(priv->pThornburyViewManager, pBottomBarSignalHash);

	/* add attributes to meta roller */
        thornbury_set_property(priv->pThornburyViewManager, DETAIL_META_ROLLER,
                                        "number", SONGS_ROLLER_COLUMN_NUM,
                                        "text", SONGS_ROLLER_COLUMN_LABEL,
					"highlight-show", SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW,
					"highlight-arrow", SONGS_ROLLER_HIGHLIGHT_ARROW_STATE,
                                        NULL);

	/* add attributes to meta roller */
        thornbury_set_property(priv->pThornburyViewManager, DETAIL_INFO_ROLLER,
                                        "icon", SONGS_ROLLER_COLUMN_ICON,
                                        "text", SONGS_ROLLER_COLUMN_LABEL,
                                        NULL);

	/*thornbury_set_property(priv->pThornburyViewManager, DETAIL_INFO_ROLLER,
				"info-icon", COLUMN_ICON,
				"row", INFO_COLUMN_ROW_NO,
				"info-label", COLUMN_TEXT,
				NULL);*/

	pIcon = g_strconcat (priv->pDataDir, "/icon_music_albums_AC.png", NULL);
	priv->pInfoHeaderModel = create_info_header_model(pAudioPlayer, pIcon);
	thornbury_set_property(priv->pThornburyViewManager, DETAIL_INFO_ROLLER,
				"header-model", priv->pInfoHeaderModel,
				NULL);

	frampton_audio_player_free_str(pIcon);

	 if(pContextSignalHash != NULL)
                g_hash_table_destroy(pContextSignalHash);
     if(pViewsSignalHash)
                g_hash_table_destroy(pViewsSignalHash);
     if(pMetaSignalHash)
                g_hash_table_destroy(pMetaSignalHash);
     if(pInfoSignalHash)
                g_hash_table_destroy(pInfoSignalHash);
	 if(pProgressBarSignalHash)
		g_hash_table_destroy(pProgressBarSignalHash);
	 if(pBottomBarSignalHash)
		g_hash_table_destroy(pBottomBarSignalHash);

}

/*********************************************************************************************
 * Function:    initialize_views
 * Description: initialize views
 * Parameters:  Pointer to the audio player
 * Return:      void
 ********************************************************************************************/
void initialize_views(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv;
	gchar *path = NULL;
	if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return;

	priv = pAudioPlayer->priv;

	if(priv->enCurrentMode == FRAMPTON_AUDIO_PLAYER_MODE_SONGS)
		path = g_strconcat(priv->pDataDir, "/songs-view/", NULL);
	else
		path = g_strconcat(priv->pDataDir, "/artist-album-views/", NULL);

	DEBUG ("view path = %s", path);

        priv->appdata = g_new0(ThornburyViewManagerAppData,1);
        priv->appdata->app= CLUTTER_ACTOR(pAudioPlayer);
        priv->appdata->default_view = g_strdup("ThumbView");
        priv->appdata->viewspath = path;
	priv->appdata->lang_basefolderpath = g_strconcat(priv->pDataDir, "/locale", NULL);
	priv->appdata->lang_basefilename = g_strdup(DOMAINNAME);
        priv->appdata->app_name = g_strdup(FRAMPTON_AUDIO_PLAYER_APP_NAME);
        priv->appdata->pAppBackFunc = NULL ;
        priv->appdata->pScreenShotPath = NULL ;

	priv->pThornburyViewManager = thornbury_view_manager_new(priv->appdata );
	//priv->pThornburyViewManager = view_manager_new(priv->appdata);

        thornbury_build_all_views(priv->pThornburyViewManager);
	/* view switch callback */
        g_signal_connect(priv->pThornburyViewManager,"view_switch_end",G_CALLBACK(view_switched), pAudioPlayer);
	g_signal_connect(priv->pThornburyViewManager, "view_switch_begin", G_CALLBACK(view_switch_begin_cb), pAudioPlayer);

	/* create views */
	create_list_view(pAudioPlayer);
	create_thumb_view(pAudioPlayer);
	create_detail_view(pAudioPlayer);

	priv->pCurrentView = g_strdup("ThumbView");

}

/*********************************************************************************************
 * Function:    update_current_track_details
 * Description: Update the urrent track details
 * Parameters:  Audio Player and the current playing track
 * Return:      void
 ********************************************************************************************/
void update_current_track_details (FramptonAudioPlayer *pAudioPlayer, gchar *playingtrack)
{
        FramptonAudioPlayerPrivate *priv;
        GrassmoorMediaInfo *newPlayingInfo;

	if(NULL == playingtrack)
		return;

        priv = pAudioPlayer->priv;

	/* Get all meta information for the current file */
        /* FIXME: Add error handling */
        newPlayingInfo = grassmoor_tracker_get_media_file_info (priv->tracker, playingtrack, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
        DEBUG ("updating track to: %s", playingtrack);

	/* Populate the meta info roller */
        populate_meta_roller(pAudioPlayer, newPlayingInfo, playingtrack);
	populate_info_roller(pAudioPlayer, newPlayingInfo);

	/* free previous playing info */
    g_clear_pointer (&priv->currentPlayingInfo, grassmoor_media_info_free);

    /* Update the current playing with this newly obtained information */
	priv->currentPlayingInfo = newPlayingInfo;

	/* update current playing track */
	if(priv->pCurrentTrack != NULL)
	{
		g_free(priv->pCurrentTrack);
		priv->pCurrentTrack = NULL;
	}
	priv->pCurrentTrack = g_strdup(playingtrack);
}

/*********************************************************************************************
 * Function:    update_progress_bar_play_state
 * Description: Update the progress bar play state
 * Parameters:  state
 * Return:      void
 ********************************************************************************************/
void update_progress_bar_play_state(gboolean bPlayState)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();

        if(FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
        {
                FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

		thornbury_set_property(priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "play-state", bPlayState, NULL);
	}
}

/*********************************************************************************************
 * Function:    update_player_view_timer
 * Description: Update timer in detail view bottom bar
 * Parameters:  pAudioPlayer, progress track time
 * Return:      void
 ********************************************************************************************/
static void update_player_view_timer(FramptonAudioPlayer *pAudioPlayer, gdouble trackPosition)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        gint mins = 0;
        gint secs = 0;
        gchar *displaySecs = NULL;
        gchar *displayMins = NULL;
        gchar *newTime = NULL ;
        guint timer = 0;
        GValue value = { 0 };

	if(NULL == priv->currentPlayingInfo || grassmoor_media_info_get_duration (priv->currentPlayingInfo) == 0)
		return;

        timer = trackPosition * grassmoor_media_info_get_duration (priv->currentPlayingInfo);
        if (timer > grassmoor_media_info_get_duration (priv->currentPlayingInfo))
                return;

        if(timer >= 60)
        {
                mins = timer/60;
                secs = timer%60;
        }
        else
        {
                mins = 0;
                secs = timer;
        }

        if(mins < 10)
                displayMins = g_strdup_printf("%s""%d", "0", mins);
        else
                displayMins = g_strdup_printf("%d", mins);
        if(secs < 10)
                displaySecs = g_strdup_printf("%s""%d", "0", secs);
        else
                displaySecs = g_strdup_printf("%d", secs);

        newTime = g_strjoin(":", displayMins, displaySecs, NULL);

	g_value_init(&value, G_TYPE_STRING);
	g_value_set_string(&value, newTime);

	/* update bottom bar model with new time */
	if(NULL != priv->pBottomBarModel)
		thornbury_model_insert_value(priv->pBottomBarModel, 0, BOTTOM_BAR_FOUR, &value);

	if(NULL != displayMins)
        {
                g_free(displayMins);
                displayMins = NULL;
        }
        if(NULL != displaySecs)
        {
                g_free(displaySecs);
                displaySecs = NULL;
        }
	if(NULL != newTime)
	{
		g_free(newTime);
		newTime = NULL;
	}
	g_value_unset(&value);	
}

/*********************************************************************************************
 * Function:    update_bottom_bar
 * Description: Update the bottom bar icon state
 * Parameters:  pAudioPlayer, bShuffle, bRepeat
 * Return:      void
 ********************************************************************************************/
void update_bottom_bar(FramptonAudioPlayer *pAudioPlayer, gboolean bShuffle, gboolean bRepeat)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	
	priv->bShuffle = bShuffle;
	priv->bRepeat = bRepeat;
	thornbury_set_property(priv->pThornburyViewManager, DETAIL_BOTTOMBAR, "button-one-state", priv->bShuffle, NULL);
	thornbury_set_property(priv->pThornburyViewManager, DETAIL_BOTTOMBAR, "button-two-state", priv->bRepeat, NULL);
}

/*********************************************************************************************
 * Function: updatemeta__highlight_arrow_state
 * Description:  update arrow state as play/pause
 * Parameters:  state as TRUE/FALSE for play/pause
 * Return:  
 ********************************************************************************************/
void update_meta_highlight_arrow_state(gboolean bState)
{
        FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        GHashTable *propHash = NULL;
        gint row = 0;
        GValue value = { 0 };

        if(priv->prevMetaRow >= 0)
                row = priv->prevMetaRow;
        else
        {
               propHash = thornbury_get_property(priv->pThornburyViewManager, DETAIL_META_ROLLER, "focused-row", NULL);
               row = g_value_get_int(g_hash_table_lookup(propHash, "focused-row"));
        }

        g_value_init(&value, G_TYPE_BOOLEAN);
        g_value_set_boolean(&value, bState);

        thornbury_model_insert_value(priv->pMetaRollerModel, row, SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, &value);

        g_value_unset(&value);
        if(NULL != propHash)
        {
                g_hash_table_destroy(propHash);
                propHash = NULL;
        }

}

/*********************************************************************************************
 * Function:    update_meta_highlight_arrow
 * Description: Update meta roller highlight arrow
 * Parameters:  pAudioPlayer, playingtrack
 * Return:      void
 ********************************************************************************************/
static void update_meta_highlight_arrow(FramptonAudioPlayer *pAudioPlayer, gchar *playingtrack)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gchar *track_id = NULL;
	gint rowToFocus = 0;
	gint focus = FALSE;
	ThornburyModelIter *iter;

	if(NULL == priv->pMetaRollerModel)
		return;

	iter = thornbury_model_get_first_iter(priv->pMetaRollerModel);

        while (!thornbury_model_iter_is_last (iter))
     	{   
		thornbury_model_iter_get (iter, SONGS_ROLLER_COLUMN_ID, &track_id, -1);
		rowToFocus = thornbury_model_iter_get_row(iter);
		if (g_strcmp0 (track_id, playingtrack) == 0)
		{
			GValue value = { 0 };
			GValue value2 = { 0 };
			GValue valueState = { 0 };
			GValue valuePrevState = { 0 };

			//g_print("meta item %s current url %s row = %d\n", value, playingtrack,  rowToFocus);
			thornbury_set_property(priv->pThornburyViewManager, DETAIL_META_ROLLER, "focused-row", rowToFocus, NULL);

			thornbury_model_iter_get(iter, SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, &focus, -1);
			g_value_init(&value, G_TYPE_BOOLEAN);
			g_value_set_boolean(&value, TRUE);

			g_value_init(&value2, G_TYPE_BOOLEAN);
			g_value_set_boolean(&value2,FALSE);

			g_value_init(&valueState, G_TYPE_BOOLEAN);
			g_value_set_boolean(&valueState, TRUE);

			g_value_init(&valuePrevState, G_TYPE_BOOLEAN);
			g_value_set_boolean(&valuePrevState, FALSE);

			if(priv->prevMetaRow != -1)
			{
				thornbury_model_insert_value(priv->pMetaRollerModel, priv->prevMetaRow, SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, &value2);
				thornbury_model_insert_value(priv->pMetaRollerModel, priv->prevMetaRow, SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, &valuePrevState);
				DEBUG ("set false for row = %d", priv->prevMetaRow);
			}
			thornbury_model_insert_value(priv->pMetaRollerModel, rowToFocus, SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, &value);
			thornbury_model_insert_value(priv->pMetaRollerModel, rowToFocus, SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, &valueState);
			DEBUG ("set highlight arrow for row = %d", rowToFocus);
			g_value_unset(&value);
			g_value_unset(&value2);
			g_value_unset(&valuePrevState);
			g_value_unset(&valueState);

			/* update the row */
			priv->prevMetaRow = rowToFocus;
			break;
		}
		iter = thornbury_model_iter_next (iter);
	}
	if(iter)
		g_object_unref(iter);	
}

/*********************************************************************************************
 * Function:    frampton_audio_player_update_progress_bar
 * Description: Update the progress bar
 * Parameters:  progress track time
 * Return:      void
 ********************************************************************************************/
void frampton_audio_player_update_progress_bar(gdouble trackPosition)
{
        FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default();

        if(FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
        {
                FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

                /* Set the time only if the time value recieved is greater than 0 */
                //if(trackPosition > 0)
                {
			/* if seek is going on, no need to update the progress bar */
			if(priv->pThornburyViewManager)	
			{
				if(! priv->bSeekStarted)
					thornbury_set_property(priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "current-duration", trackPosition, NULL);

				/* update the time */
				update_player_view_timer(pAudioPlayer, trackPosition);
			}
                }

        }
}

/*********************************************************************************************
 * Function:    set_progress_duration
 * Description: Set the progress duration
 * Parameters:  duration
 * Return:      Void
 ********************************************************************************************/
static gchar* set_progress_duration(gint duration, FramptonAudioPlayer *audioPlayer)
{
        //FramptonAudioPlayerPrivate *priv = audioPlayer->priv;

        gint hours = 0;
        gint mins = 0;
        gint secs = 0;
        gchar *displayHours = NULL;
        gchar *displayMins = NULL;
        gchar *displaySecs = NULL;
        gchar *trackDuration;

        /* Calculation for display of hours */
        if(duration >= 3600)
        {
                hours = duration/3600;
                duration -= hours*3600;
        }
        else
                hours = 0;
        if(hours < 10)
                displayHours = g_strdup_printf("%s""%d", "0", hours);
        else
                displayHours = g_strdup_printf("%d", hours);

        /* Calculation for display of minutes */
        if(duration > 60)
        {
                mins = duration/60;
                duration -= mins*60;
        }
        else
                mins = 0;
        if(mins < 10)
                displayMins = g_strdup_printf("%s""%d", "0", mins);
        else
                displayMins = g_strdup_printf("%d", mins);

        /* Calculation for display of seconds */
        secs = duration;
        if(secs < 10)
                displaySecs = g_strdup_printf("%s""%d", "0", secs);
        else
                displaySecs = g_strdup_printf("%d",secs);

        /* Set the final update on the meta info roller */
        trackDuration = g_strjoin (":", displayHours, displayMins, displaySecs, NULL);

	/* free memory */
        if(NULL != displayHours)
	{
                g_free(displayHours);
		displayHours = NULL;
	}

        if(NULL != displayMins)
	{
		g_free(displayMins);
		displayMins = NULL;
	}
        if(NULL != displaySecs)
	{
                g_free(displaySecs);
		displaySecs = NULL;
	}

	return trackDuration;
}

/*********************************************************************************************
 * Function:    update_info_roller_items
 * Description: update_info_roller_items
 * Parameters:  Pointer to the audio player element, text to be displayed, row
 * Return:      void
 ********************************************************************************************/
static void update_info_roller_items (FramptonAudioPlayer *pAudioPlayer,
    const gchar *item, gint row)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	GValue value = {0};
        g_value_init(&value, G_TYPE_STRING);
        g_value_set_static_string (&value,  item);

        thornbury_model_insert_value(priv->pInfoRollerModel, row,
                        SONGS_ROLLER_COLUMN_LABEL,  &value);
                        //COLUMN_TEXT,  &value);

        g_value_unset (&value);
}

/*********************************************************************************************
 * Function:    populate_info_roller
 * Description: Popluate the info roller
 * Parameters:  Pointer to the audio player element
 * Return:      void
 ********************************************************************************************/
void populate_info_roller(FramptonAudioPlayer *pAudioPlayer, GrassmoorMediaInfo *newPlayingInfo)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	GValue pName = { 0 };

	//gint index;
# if 0
	if(NULL != newPlayingInfo)
        {
                if(g_strcmp0(priv->pCurrentTrack, newPlayingInfo->sUrl->str) == 0)
                        return;
        }
# endif
	if(NULL != priv->trackDuration)
        {
                g_free(priv->trackDuration);
                priv->trackDuration = NULL;
        }
	/* update header */
        g_value_init(&pName, G_TYPE_STRING);

	if(NULL == newPlayingInfo)
		g_value_set_string(&pName, "Unknown");
	else if (grassmoor_media_info_get_album (newPlayingInfo))
		g_value_set_string (&pName, grassmoor_media_info_get_album (newPlayingInfo));
	else
		g_value_set_string(&pName, "Unknown");

	thornbury_model_insert_value(priv->pInfoHeaderModel, 0, INFO_ROLLER_HEADER_MID_TEXT, &pName);

	/* update info roller */
	if(NULL == newPlayingInfo)
	{
		update_info_roller_items(pAudioPlayer, "", 0);
		update_info_roller_items(pAudioPlayer, "", 1);
		update_info_roller_items(pAudioPlayer, "", 2);
		update_info_roller_items(pAudioPlayer, "", 3);
		update_info_roller_items(pAudioPlayer, "", 4);
	}
	else
	{
		gchar *trackNumber;

		/* free track duration */
		if(NULL != priv->trackDuration)
		{
			g_free(priv->trackDuration);
			priv->trackDuration = NULL;
		}
		/* 1. Set duration */
		priv->trackDuration = set_progress_duration (grassmoor_media_info_get_duration (newPlayingInfo), pAudioPlayer);
		update_info_roller_items(pAudioPlayer, priv->trackDuration, 0);
		/* 2. Set year */
		if(NULL != grassmoor_media_info_get_year (newPlayingInfo))
			update_info_roller_items (pAudioPlayer, grassmoor_media_info_get_year (newPlayingInfo), 1);

		/* 3. Set artist */
		if (grassmoor_media_info_get_artist (newPlayingInfo))
			update_info_roller_items(pAudioPlayer, grassmoor_media_info_get_artist (newPlayingInfo), 2);

		/* 4. Set album */
		if (grassmoor_media_info_get_artist (newPlayingInfo))
			update_info_roller_items(pAudioPlayer, grassmoor_media_info_get_artist (newPlayingInfo), 3);
		/* 5. Set duration */
		trackNumber = g_strdup_printf("%d", grassmoor_media_info_get_track_number (newPlayingInfo));
		update_info_roller_items(pAudioPlayer, trackNumber, 4);

		if(trackNumber)
		{
			g_free(trackNumber);
			trackNumber = NULL;
		}
	}
	/* show info roller */
	//thornbury_set_property(priv->pThornburyViewManager, DETAIL_INFO_ROLLER, "show", TRUE, NULL);
}

/*********************************************************************************************
 * Function:    sort_metainfo_list
 * Description: Sort the meta info list
 * Parameters:  Pointers to compare and the userdata
 * Return:      result of the comparison
 ********************************************************************************************/
static gint sort_metainfo_list(gconstpointer a, gconstpointer b, gpointer userData)
{
        if( NULL != a && NULL != b)
	{
                GrassmoorMediaInfo *playingInfo1 = (GrassmoorMediaInfo *)a;
		GrassmoorMediaInfo *playingInfo2 = (GrassmoorMediaInfo *)b;

		return g_strcmp0 (grassmoor_media_info_get_title (playingInfo1), grassmoor_media_info_get_title (playingInfo2));
	}
        return 0;
}

/*********************************************************************************************
 * Function:    populate_meta_roller
 * Description: Popluate the meta roller
 * Parameters:  Pointer to the audio player element
 * Return:      void
 ********************************************************************************************/
void populate_meta_roller(FramptonAudioPlayer *pAudioPlayer, GrassmoorMediaInfo *newPlayingInfo, gchar *playingtrack)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gint rows = 0;
	guint iCount = 0;
	GList *tempList = NULL;

# if 0
	if(NULL != newPlayingInfo)
	{
		if(g_strcmp0(priv->pCurrentTrack, newPlayingInfo->sUrl->str) == 0)
			return;
	}
# endif
	rows = thornbury_model_get_n_rows(priv->pMetaRollerModel) - 1;

	for(;rows >=0;rows--)
	{
    		thornbury_model_remove(priv->pMetaRollerModel,rows );
	}

	/* reset the mea previous highlight row */
	priv->prevMetaRow = -1;

	/* free the old track array */
	if(NULL != priv->similarTracks)
	{
		g_ptr_array_free(priv->similarTracks, TRUE);
		priv->similarTracks = NULL;
	}

	/* get new tracks array */
	if (newPlayingInfo && grassmoor_media_info_get_artist (newPlayingInfo) && grassmoor_media_info_get_album (newPlayingInfo))
	{
		if(FRAMPTON_AUDIO_PLAYER_MODE_ARTIST == priv->enCurrentMode)
			/* FIXME: Add error handling */
			priv->similarTracks = grassmoor_tracker_get_meta_list_for (priv->tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST, grassmoor_media_info_get_artist (newPlayingInfo), GRASSMOOR_TRACKER_MEDIA_INFO_URL, 0, 50, NULL);
		else if(FRAMPTON_AUDIO_PLAYER_MODE_ALBUM == priv->enCurrentMode)
			/* FIXME: Add error handling */
			priv->similarTracks = grassmoor_tracker_get_meta_list_for (priv->tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM, grassmoor_media_info_get_album (newPlayingInfo), GRASSMOOR_TRACKER_MEDIA_INFO_URL, 0, 50, NULL);

		if(NULL != priv->similarTracks && priv->similarTracks->len > 0)
		{
			GList *newList;
			gchar *title = NULL;
			gchar *url = NULL;

			for(iCount = 0; iCount < priv->similarTracks->len; iCount++)
			{
				const gchar *uri = g_ptr_array_index(priv->similarTracks, iCount);
				if( NULL != uri )
				{
					/* FIXME: Add error handling */
					GrassmoorMediaInfo *mediaInfo = grassmoor_tracker_get_media_file_info (priv->tracker, uri, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
					if (mediaInfo && grassmoor_media_info_get_title (mediaInfo))
						tempList = g_list_append(tempList, mediaInfo);
				}
			}
			if(tempList == NULL)
				return;
			/* sort title */
			if(g_list_length(tempList) > 0)
				tempList = g_list_sort_with_data(tempList, (GCompareDataFunc)sort_metainfo_list, pAudioPlayer);

			/* Populate the title list */
			newList = g_list_first(tempList);

			while(newList && newList->data)
			{
				gchar *songNumber = NULL;
				GrassmoorMediaInfo *mediaInfo = NULL;

				songNumber = g_strdup_printf("%d", iCount + 1);
				mediaInfo = (GrassmoorMediaInfo *)newList->data;
				url = g_strdup (grassmoor_media_info_get_url (mediaInfo));

				title = g_strdup (grassmoor_media_info_get_title (mediaInfo));

				thornbury_model_append (priv->pMetaRollerModel,
						SONGS_ROLLER_COLUMN_ID, url,
						SONGS_ROLLER_COLUMN_NUM, songNumber,
						SONGS_ROLLER_COLUMN_LABEL, title,
						SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
						SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
						-1);
				if(url )
				{
					g_free(url);
					url = NULL;
				}
				if(title)
				{
					g_free(title);
					title = NULL;
				}
				if(songNumber)
				{
					g_free(songNumber);
					songNumber = NULL;
				}
				iCount++;
				newList = g_list_next(newList);
			}
			newList = g_list_first(tempList);
			while(newList)
			{
				g_clear_pointer (&newList->data, grassmoor_media_info_free);
				newList = g_list_next(newList);
			}	
			if(tempList)
			{
				g_list_free(tempList);
				tempList = NULL; 
			}
		}
		else
		{
			gchar *songNumber = NULL;
			songNumber = g_strdup_printf("%d", iCount + 1);
			thornbury_model_append (priv->pMetaRollerModel,
                                                SONGS_ROLLER_COLUMN_ID, grassmoor_media_info_get_url (newPlayingInfo),
						SONGS_ROLLER_COLUMN_NUM, songNumber,
						SONGS_ROLLER_COLUMN_LABEL, grassmoor_media_info_get_title (newPlayingInfo),
						SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
						SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
						-1);

			if(songNumber)
			{
				g_free(songNumber);
				songNumber = NULL;
			}
		}
	}
	else
	{
		gchar *songNumber = NULL;
                songNumber = g_strdup_printf("%d", iCount + 1);
		thornbury_model_append (priv->pMetaRollerModel,
				SONGS_ROLLER_COLUMN_ID, playingtrack,
				SONGS_ROLLER_COLUMN_NUM, songNumber,
				SONGS_ROLLER_COLUMN_LABEL, "Unknown",
				SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
				SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
				-1);

		if(songNumber)
		{
			g_free(songNumber);
			songNumber = NULL;
		}

	}

	thornbury_set_property(priv->pThornburyViewManager, DETAIL_META_ROLLER, "focused-row", 0, NULL);
	update_meta_highlight_arrow(pAudioPlayer, playingtrack);
}


/*********************************************************************************************
 * Function:    check_duplicate_entry
 * Description: check duplicte thumbs in case of artist/album mode
 * Parameters:  gconstpointer, gconstpointer
 * Return:      gint
 ********************************************************************************************/
static gint check_duplicate_entry (gconstpointer a, gconstpointer b)
{
        if(NULL != a && NULL != b && (0 == g_strcmp0( (gchar *)a, (gchar *)b)))
                return 0;
        else
                return -1;
}


/*********************************************************************************************
 * Function:    check_file_exists
 * Description: check whether albumart file exists
 * Parameters:  gchar *
 * Return:      gboolean
 ********************************************************************************************/
static gboolean check_file_exists (gchar *pThumbPath)
{
	GFile *pFile = NULL;
	if(NULL != pThumbPath)
	{
		/* check whether file exists */
		pFile = g_file_new_for_commandline_arg (pThumbPath);
		if (pFile != NULL)
		{
			if(g_file_query_exists(pFile, NULL))
			{
				g_object_unref(pFile);
				return TRUE;
			}
		}
	}
	if(pFile)
		g_object_unref(pFile);
	return FALSE;
}

/*********************************************************************************************
 * Function:    add_thumb_item
 * Description: Check if we need to add a thumb item
 * Parameters:  Pointer to the audio player element, the current
 *                              artist/album
 * Return:              gboolean (TRUE if we need to add, FALSE if not)
 ********************************************************************************************/
static gboolean add_thumb_item(FramptonAudioPlayer *pAudioPlayer, gchar *newItem)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

        if(NULL == newItem)
                return FALSE;

        if(NULL != g_list_find_custom(priv->pCoverThumbList , newItem, (GCompareFunc)check_duplicate_entry))
                return FALSE;

        return TRUE;
}

static void populate_thumb_roller_for_open_with(gchar *pUrl, FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	GError *error = NULL;
	gchar *pThumbPath = NULL;
	gchar *pTitle = NULL;
	gchar *album_art_url = NULL;
	GrassmoorMediaInfo *mediaInfo;
	ClutterContent *pContent;

	if(NULL == pUrl)
		return;

	/* FIXME: Add error handling */
	mediaInfo = grassmoor_tracker_get_media_file_info (priv->tracker, pUrl, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
	if(NULL == mediaInfo)
	{
		pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
		pTitle = g_strdup("Unknown");	
	}
	else
	{
		if(NULL != mediaInfo)
                {
                        gchar *pUri = NULL;
			if (grassmoor_media_info_get_title (mediaInfo))
				pTitle = g_strdup (grassmoor_media_info_get_title (mediaInfo));
			
			if (grassmoor_media_info_get_album (mediaInfo) &&
                            grassmoor_media_info_get_artist (mediaInfo))
                	{

                        	album_art_url = grassmoor_tracker_get_albumart_url (pUrl, grassmoor_media_info_get_album (mediaInfo), grassmoor_media_info_get_artist (mediaInfo));
			}
			if (album_art_url && strlen (album_art_url) > 0)
			{

                        	/* sometimes tracker gives the file which does not exists */
                                GFile *pFile = g_file_new_for_commandline_arg (album_art_url);
                                if (pFile != NULL)
                                {
                                        pUri = g_file_get_path (pFile);
										g_object_unref(pFile);
                                }

                                if(check_file_exists(pUri) == FALSE)
                                        pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
                                else
                                        pThumbPath = g_strdup(pUri);
                        }
                        else
                        {
                                pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
                        }
                        if(NULL != pUri)
                        {
                                g_free(pUri);
                                pUri = NULL;
                        }

                        g_free (album_art_url);

                        g_clear_pointer (&mediaInfo,  grassmoor_media_info_free);
		}
	}

	/* create image content */
        //ClutterContent *pContent = create_texture_content(pThumbPath, 164, 164);
	pContent = thornbury_texture_create_content_sync(pThumbPath, 164, 164, &error);
	if(error)
	{
		WARNING ("thumb creation error: %s", error->message);
		g_error_free(error);
	}
	thornbury_model_append (priv->pThumbRollerModel,
                                        THUMB_ROLLER_ICON, CLUTTER_IMAGE(pContent),
                                        THUMB_ROLLER_URL, pUrl,
                                        THUMB_ROLLER_OVERLAY_SHOW, TRUE,
                                        THUMB_ROLLER_OVERLAY_TEXT, pTitle,
                                        THUMB_ROLLER_OVERLAY_STATE, TRUE,
                                        -1);


	if(NULL != pTitle)
		g_free(pTitle);
	if(pContent)
		g_object_unref(pContent);		
}	

/*********************************************************************************************
 * Function:    populate_list_roller_for_open_with
 * Description: Create the list roller item
 * Parameters:  pUrl*, pAudioPlayer*
 * Return:      void
 ********************************************************************************************/
static void populate_list_roller_for_open_with (gchar *pUrl, FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gchar *album = NULL;
        gchar *artist = NULL;
	gchar *pThumbPath = NULL;
	gchar *keyName = NULL;
	gchar *pMidText = NULL;
	gchar *pRightText = NULL;
	GError *error = NULL;
	GrassmoorMediaInfo *mediaInfo;
	ClutterContent *pContent;

	if(NULL == pUrl)
		return;

	/* FIXME: Add error handling */
	mediaInfo = grassmoor_tracker_get_media_file_info (priv->tracker, pUrl, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
        if(NULL == mediaInfo)
        {
		pMidText = g_strdup("Unknown");
		pRightText = g_strdup("Unknown");
		pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
	}
        else
        {
                gchar *album_art_url = NULL;

		if (grassmoor_media_info_get_album (mediaInfo) &&
			grassmoor_media_info_get_artist (mediaInfo))
		{
			album_art_url =  grassmoor_tracker_get_albumart_url (pUrl, grassmoor_media_info_get_album (mediaInfo), grassmoor_media_info_get_artist (mediaInfo));
		}
		if (album_art_url)
		{
			pThumbPath = album_art_url; /* pass ownershop */
		}
		else
			pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);;

                /* The text for the album */
                if (grassmoor_media_info_get_album (mediaInfo))
                        album = g_strdup (grassmoor_media_info_get_album (mediaInfo));
                if (album == NULL)
                        album = g_strdup("Unknown");

                /* The text for the artist */
                if (grassmoor_media_info_get_artist (mediaInfo))
                        artist = g_strdup (grassmoor_media_info_get_artist (mediaInfo));
                if (artist == NULL)
                        artist = g_strdup("Unknown");


		if(strlen(artist) <= 0)
		{
			g_free(artist);
			artist = NULL;
			artist = g_strdup("Unknown");
		}	
		if(strlen(album) <= 0)
                {
                        g_free(album);
                        album = NULL;
                        album = g_strdup("Unknown");
                }   
			
		pMidText = artist;
		pRightText = album;

        g_clear_pointer (&mediaInfo,  grassmoor_media_info_free);
	}

	keyName = g_strdup("Unknown");
	//ClutterContent *pContent = create_texture_content(pThumbPath, 64, 64);
	pContent = thornbury_texture_create_content_sync(pThumbPath, 64, 64, &error);
        if(error)
        {
        	WARNING ("thumb creation error = %s: code %d: %s", g_quark_to_string(error->domain), error->code, error->message);
                g_error_free(error);
        }

	thornbury_model_append (priv->pAlbumArtistRollerModel,
                                ARTISTALBUM_ROLLER_COL_ID, keyName ,
                                ARTISTALBUM_ROLLER_COL_MID_TEXT, pMidText,
                                ARTISTALBUM_ROLLER_COL_RIGHT_TEXT, pRightText,
                                ARTISTALBUM_ROLLER_COL_CONTENT, CLUTTER_IMAGE(pContent),
                                -1);

	if(keyName)
		g_free(keyName);
	if(pMidText)
		g_free(pMidText);
	if(pRightText)
		g_free(pRightText);
	if(pContent)
		g_object_unref(pContent);
}

/*********************************************************************************************
 * Function:    populate_song_roller_for_open_with
 * Description: Popluate the song roller
 * Parameters:  pUrl*, Pointer to the audio player element
 * Return:      void
 ********************************************************************************************/
static void populate_song_roller_for_open_with (gchar *pUrl, FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gchar *title = NULL;
	gchar *songName = NULL;
	GrassmoorMediaInfo *mediaInfo;

	if(NULL == pUrl)
                return;

	/* FIXME: Add error handling */
	mediaInfo = grassmoor_tracker_get_media_file_info (priv->tracker, pUrl, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
        if(NULL == mediaInfo)
	{
		title = g_strdup("Unknown");
	}
	if(NULL != mediaInfo && NULL != grassmoor_media_info_get_url (mediaInfo))
	{
		if (grassmoor_media_info_get_title (mediaInfo))
			title = g_strdup (grassmoor_media_info_get_title (mediaInfo));
		if(NULL == title)
			title = g_strdup("Unknown");

        g_clear_pointer (&mediaInfo,  grassmoor_media_info_free);
	}

	songName = g_strdup(pUrl);

	thornbury_model_append (priv->pSongRollerModel,
                                        SONGS_ROLLER_COLUMN_ID, songName,
                                        SONGS_ROLLER_COLUMN_NUM, "1",
                                        SONGS_ROLLER_COLUMN_LABEL, title,
                                        SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
                                        SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
                                         -1);

	if(songName)
		g_free(songName);
	if(title)
		g_free(title);
}

/*********************************************************************************************
 * Function:    populate_audio_thumb_roller
 * Description: Popluate the audio thumbnail roller
 * Parameters:  Pointer to the audio player element
 * Return:              void
 ********************************************************************************************/
static gboolean populate_audio_thumb_roller(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	//gchar *pThumbPath = NULL;
	gchar *newItem = NULL;
	GValue value = { 0 };
	GValue value2 = { 0 };
	GValue value3 = { 0 };

	/* sometimes metatracker does not provide meta data, then show default thumb */
	if(NULL == priv->currentLoadMediaInfo)
	{
		//pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
		newItem = g_strdup_printf("Unknown%d", priv->inUnknownCount);
	}
	else
	{
		if(priv->currentLoadMediaInfo && grassmoor_media_info_get_url (priv->currentLoadMediaInfo) &&
					grassmoor_media_info_get_album (priv->currentLoadMediaInfo) &&
					grassmoor_media_info_get_artist (priv->currentLoadMediaInfo))
		{
                        gboolean addItemCheck = TRUE;

                        /* In the album art roller, we need to have only one
                         * thumbnail shown for every artist in case of selection by
                         * artist and only one thumbnail shown for an album in case
                         * of selection by album, only one entry per album */
                        if(FRAMPTON_AUDIO_PLAYER_MODE_ALBUM == priv->enCurrentMode)
                                newItem = g_strdup (grassmoor_media_info_get_album (priv->currentLoadMediaInfo));
                        else if (FRAMPTON_AUDIO_PLAYER_MODE_ARTIST == priv->enCurrentMode)
                                newItem = g_strdup (grassmoor_media_info_get_artist (priv->currentLoadMediaInfo));
                        else
                                newItem = g_strdup (grassmoor_media_info_get_url (priv->currentLoadMediaInfo));

			/* check for duplicate item */
                        addItemCheck = add_thumb_item(pAudioPlayer, newItem);
                        if(FALSE == addItemCheck)
                        {
                                priv->duplicateCount = priv->duplicateCount + 1;
				if(NULL != newItem)
				{
					g_free(newItem);
					newItem = NULL;	
				}
                                return FALSE;
                        }

		}
		else
		{
			newItem = g_strdup("Unknown");
		}
	}

	priv->pCoverThumbList = g_list_append(priv->pCoverThumbList, newItem);
	/* create cogl handle */
        g_value_init(&value, G_TYPE_OBJECT);
        g_value_set_object (&value, CLUTTER_IMAGE(priv->pContent));
	thornbury_model_insert_value(priv->pThumbRollerModel, priv->inThumbCount, THUMB_ROLLER_ICON, &value);

	g_value_unset (&value);

	/* store the url */
	g_value_init(&value2, G_TYPE_STRING);
	g_value_set_static_string (&value2, g_strdup(priv->pCurrentLoadUrl));

	if(thornbury_model_get_n_rows(priv->pThumbRollerModel) > 0)
	thornbury_model_insert_value(priv->pThumbRollerModel, priv->inThumbCount ,//g_list_length(priv->pCoverThumbList) - 1,
			THUMB_ROLLER_URL, &value2);

	g_value_unset (&value2);

	g_value_init(&value3, G_TYPE_BOOLEAN);
	g_value_set_boolean (&value3, FALSE);
	
	thornbury_model_insert_value(priv->pThumbRollerModel, priv->inThumbCount, THUMB_ROLLER_OVERLAY_SHOW, &value3);
	g_value_unset (&value3);

	/* increment the count index */
	priv->inThumbCount++;

	return TRUE;
}

/*********************************************************************************************
 * Function:    create_album_artist_roller_item
 * Description: Create the album artist roller item
 * Parameters:  pAudioPlayer*
 * Return:      void
 ********************************************************************************************/
static void create_album_artist_roller_item (FramptonAudioPlayer *pAudioPlayer)
{
	gchar *album = NULL;
	gchar *artist = NULL;
	gchar *keyName = NULL;
	gchar *pMidText = NULL;
	gchar *pRightText = NULL;
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gchar *pUrl;
	GValue value = { 0 };
	GValue valueLabel1 = { 0 };
	GValue valueLabel2 = { 0 };
	GValue valueLabel3 = { 0 };
	GValue valueLabel4 = { 0 };


	if(NULL == priv->currentLoadMediaInfo)
	{
		pMidText = g_strdup("Unknown");
		pRightText = g_strdup("Unknown");
		keyName = g_strdup_printf("Unknown%d", priv->inUnknownCount);
	}
# if 0
        else if(NULL != priv->currentLoadMediaInfo && NULL != priv->currentLoadMediaInfo->sUrl &&
                                        NULL != priv->currentLoadMediaInfo->Info.AudioVideoFileInfo.sAlbum &&
                                        NULL != priv->currentLoadMediaInfo->Info.AudioVideoFileInfo.sArtist)
	{
		if( priv->enCurrentMode != FRAMPTON_AUDIO_PLAYER_MODE_SONGS)
                {
                        /* We want to avoid duplicate album art on the HMI. So make
                         * a check for duplicate. If found, do not add this
                         * thumbnail on the HMI */
			DEBUG (" %s", priv->pThumbPath);
                        if(NULL != g_list_find_custom(priv->pThumbsList , priv->pThumbPath, (GCompareFunc)check_duplicate_entry))
			{
                               DEBUG (" %s duplicate_entry %d", priv->pThumbPath, priv->inCurrentIndex);
				return;
			}
                        else
			{
                                priv->pThumbsList = g_list_append(priv->pThumbsList, g_strdup(priv->pThumbPath));

			}
                }
	}	
# endif

	else
	{
		if (grassmoor_media_info_get_album (priv->currentLoadMediaInfo) && grassmoor_media_info_get_artist (priv->currentLoadMediaInfo))
		{
			if(FRAMPTON_AUDIO_PLAYER_MODE_ARTIST == priv->enCurrentMode)
			{
				keyName = g_strjoin (NULL, grassmoor_media_info_get_artist (priv->currentLoadMediaInfo), grassmoor_media_info_get_album (priv->currentLoadMediaInfo), NULL);
			}
			else
			{
				keyName = g_strjoin(NULL, grassmoor_media_info_get_album (priv->currentLoadMediaInfo), grassmoor_media_info_get_artist (priv->currentLoadMediaInfo), NULL);
			}
		}
		else
			keyName = g_strdup_printf("Unknown%d", priv->inUnknownCount);

		/* The text for the album */
		if (grassmoor_media_info_get_album (priv->currentLoadMediaInfo))
			album = g_strdup (grassmoor_media_info_get_album (priv->currentLoadMediaInfo));
		if (album == NULL || (g_strcmp0(album, "") == 0) )
			album = g_strdup("Unknown");

		/* The text for the artist */
		if (grassmoor_media_info_get_artist (priv->currentLoadMediaInfo))
			artist = g_strdup (grassmoor_media_info_get_artist (priv->currentLoadMediaInfo));
		if (artist == NULL || (g_strcmp0(artist, "") == 0) )
			artist = g_strdup("Unknown");
		/* set the mode */
		if(FRAMPTON_AUDIO_PLAYER_MODE_ARTIST == priv->enCurrentMode)
		{
			pMidText = artist;
			pRightText = album;
		}
		else
		{
			pMidText = album;
			pRightText = artist;
		}
	}

	/* update the hash */
	if(FRAMPTON_AUDIO_PLAYER_MODE_ARTIST == priv->enCurrentMode ||
			FRAMPTON_AUDIO_PLAYER_MODE_ALBUM == priv->enCurrentMode)
	{
		if(NULL != keyName && (NULL == g_hash_table_lookup(priv->pArtistAlbumHash, keyName)))
		{
			g_hash_table_insert(priv->pArtistAlbumHash, g_strdup(keyName), g_strdup(priv->pCurrentLoadUrl));
		}
	}

	pUrl = g_strdup(priv->pCurrentLoadUrl);

	g_value_init(&value, G_TYPE_STRING);
	g_value_set_static_string (&value,  keyName);

	g_value_init(&valueLabel1, G_TYPE_STRING);
	g_value_set_static_string (&valueLabel1, pMidText);

	g_value_init(&valueLabel2, G_TYPE_STRING);
	g_value_set_static_string (&valueLabel2, pRightText);

	g_value_init(&valueLabel3, G_TYPE_STRING);
	g_value_set_static_string (&valueLabel3, pUrl);

        g_value_init(&valueLabel4, G_TYPE_OBJECT);
        g_value_set_object (&valueLabel4, CLUTTER_IMAGE(priv->pContent));

	thornbury_model_insert_value(priv->pAlbumArtistRollerModel, priv->inArtistAlbumCount,
			ARTISTALBUM_ROLLER_COL_ID, &value);
	thornbury_model_insert_value(priv->pAlbumArtistRollerModel, priv->inArtistAlbumCount,
			ARTISTALBUM_ROLLER_COL_MID_TEXT, &valueLabel1);
	thornbury_model_insert_value(priv->pAlbumArtistRollerModel, priv->inArtistAlbumCount,
			ARTISTALBUM_ROLLER_COL_RIGHT_TEXT, &valueLabel2);
	thornbury_model_insert_value(priv->pAlbumArtistRollerModel, priv->inArtistAlbumCount,
                        ARTISTALBUM_ROLLER_COL_FIRST_URL, &valueLabel3);
	thornbury_model_insert_value(priv->pAlbumArtistRollerModel, priv->inArtistAlbumCount,
                        ARTISTALBUM_ROLLER_COL_CONTENT, &valueLabel4);

	g_value_unset (&value);
	g_value_unset (&valueLabel1);
	g_value_unset (&valueLabel2);
	g_value_unset (&valueLabel3);
        g_value_unset (&valueLabel4);

	if(keyName)
		g_free(keyName);
	if(pMidText)
		g_free(pMidText);
	if(pRightText)
		g_free(pRightText);
	if(pUrl)
		g_free(pUrl);
	/* increment the count index */
	priv->inArtistAlbumCount++;
}

/*********************************************************************************************
 * Function:    populate_artist_album_roller
 * Description: Popluate the artist al
 * Parameters:  Pointer to the audio player element
 * Return:      void
 ********************************************************************************************/
static void populate_artist_album_roller (FramptonAudioPlayer *pAudioPlayer)
{
        if( FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer) )
        {
		create_album_artist_roller_item(pAudioPlayer);
        }
}

/*********************************************************************************************
 * Function:    populate_song_roller
 * Description: Popluate the song roller
 * Parameters:  Pointer to the audio player element
 * Return:      void
 ********************************************************************************************/
static void populate_song_roller (FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gchar *title = NULL;
	gchar *songName = NULL;
	gchar *keyName = NULL;
	gchar *songNum = NULL;
	GValue value = { 0 };
	GValue valueNum = { 0 };
	GValue valueLabel = { 0 };
	GValue valueIndex = { 0 };

	if(NULL == priv->currentLoadMediaInfo)
	{
		title = g_strdup("Unknown");
		keyName = g_strdup_printf("Unknown%d", priv->inUnknownCount);
	}
	if (priv->currentLoadMediaInfo && grassmoor_media_info_get_url (priv->currentLoadMediaInfo))
	{
		if (grassmoor_media_info_get_title (priv->currentLoadMediaInfo))
			title = g_strdup (grassmoor_media_info_get_title (priv->currentLoadMediaInfo));
		if(NULL == title ||(g_strcmp0(title, "") == 0 ) )
			title = g_strdup("Unknown");


		if (grassmoor_media_info_get_album (priv->currentLoadMediaInfo) && grassmoor_media_info_get_artist (priv->currentLoadMediaInfo))
		{
			if(FRAMPTON_AUDIO_PLAYER_MODE_ARTIST == priv->enCurrentMode)
			{
				keyName = g_strjoin (NULL, grassmoor_media_info_get_artist (priv->currentLoadMediaInfo), grassmoor_media_info_get_album (priv->currentLoadMediaInfo), NULL);
			}
			else
			{
				keyName = g_strjoin (NULL, grassmoor_media_info_get_album (priv->currentLoadMediaInfo), grassmoor_media_info_get_artist (priv->currentLoadMediaInfo), NULL);
			}
		}
	}

	songName = g_strdup(priv->pCurrentLoadUrl);
	songNum = g_strdup_printf("%d",priv->inSongCount + 1);

	g_hash_table_insert(priv->pSongsHash, g_strdup(songName), g_strdup(keyName));

	g_value_init(&value, G_TYPE_STRING);
	g_value_set_static_string (&value, songName );

	g_value_init(&valueNum, G_TYPE_STRING);
	g_value_set_static_string (&valueNum,  songNum);

	g_value_init(&valueLabel, G_TYPE_STRING);
	g_value_set_static_string (&valueLabel,  title);

        g_value_init(&valueIndex, G_TYPE_INT);
	g_value_set_int(&valueIndex, priv->inThumbCount - 1);

	thornbury_model_insert_value(priv->pSongRollerModel, priv->inSongCount,
			SONGS_ROLLER_COLUMN_ID,  &value);
	thornbury_model_insert_value(priv->pSongRollerModel, priv->inSongCount,
			SONGS_ROLLER_COLUMN_NUM, &valueNum);
	thornbury_model_insert_value(priv->pSongRollerModel, priv->inSongCount,
			SONGS_ROLLER_COLUMN_LABEL, &valueLabel);
	thornbury_model_insert_value(priv->pSongRollerModel, priv->inSongCount,
			SONGS_ROLLER_RELATED_THUMB_INDEX, &valueIndex);

	g_value_unset (&value);
	g_value_unset (&valueLabel);
	g_value_unset (&valueNum);
	g_value_unset (&valueIndex);

	if(songName)
		g_free(songName);
	if(title)
		g_free(title);
	if(songNum)
		g_free(songNum);
	/* increment the song count */
	priv->inSongCount++;
}

/*********************************************************************************************
 * Function:    add_remove_thumbs_divisible_by_4
 * Description: extra thumbs to be added/ removed
 * Parameters:  Pointer to the audio player element, bAddThumbs
 * Return:      void
 ********************************************************************************************/
void add_remove_thumbs_divisible_by_4(FramptonAudioPlayer *pAudioPlayer, gboolean bAddThumbs)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	if(priv->extraThumbItems > 0)
	{
		gint inCount = priv->extraThumbItems;	
		for(; inCount > 0; inCount--)
		{
			if(bAddThumbs)
			{
				DEBUG ("added dummy thumb = %d", thornbury_model_get_n_rows(priv->pThumbRollerModel));
				/* add dummy items */
				thornbury_model_append (priv->pThumbRollerModel,
						THUMB_ROLLER_ICON, NULL,
						THUMB_ROLLER_URL, NULL,
						THUMB_ROLLER_OVERLAY_SHOW, FALSE,
						THUMB_ROLLER_OVERLAY_TEXT, NULL,
						THUMB_ROLLER_OVERLAY_STATE, FALSE,
						-1);
				priv->bRemoveState = TRUE;
			}	
			else
			{
				DEBUG ("removed dummy thumb = %d", thornbury_model_get_n_rows(priv->pThumbRollerModel) - 1);
				/* remove dummy items */
				thornbury_model_remove(priv->pThumbRollerModel, thornbury_model_get_n_rows(priv->pThumbRollerModel) - 1 );
				priv->bRemoveState = FALSE;
			}
		}
	}
}

static void create_sort_list(ThornburyModel *pModel, gint inColumn, FramptonAudioPlayer *pAudioPlayer)
{
	//FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        ThornburyModelIter *iter = thornbury_model_get_first_iter(pModel);

	gchar *value = NULL;
        while (!thornbury_model_iter_is_last (iter))
        {
                        thornbury_model_iter_get(iter, inColumn, &value, -1);

			update_sort_list(value, thornbury_model_iter_get_row(iter), pAudioPlayer);
			//g_object_unref(iter);
			iter = thornbury_model_iter_next (iter);
	}
	if(iter)
		g_object_unref(iter);
}

static void timeline_cb(ClutterTimeline *timeline, ClutterActor *pData)
{
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(pData);
        populate_item(pAudioPlayer);
}

/********************************************************
 * function : set_content_data
 * description: setting the image data for content
 * parameters:  ClutterContent*, GdkPixbuf*, GError**
 * return value: gboolean(handled or not)
 ********************************************************/
static void
set_content_data (ClutterContent *pImage,
    GdkPixbuf *pixbuf,
    GError *pErr)
{
        clutter_image_set_data(CLUTTER_IMAGE (pImage),
                                gdk_pixbuf_get_pixels (pixbuf),
                                gdk_pixbuf_get_has_alpha (pixbuf)? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
                                gdk_pixbuf_get_width (pixbuf),
                                gdk_pixbuf_get_height (pixbuf),
                                gdk_pixbuf_get_rowstride (pixbuf),
                                &pErr);
}

static ClutterContent *create_content(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	GError *error = NULL;
	ClutterContent *pContent = NULL;

	if(priv->pGdkPixbuf)
	{
		/* create image content */
		pContent = clutter_image_new();
		set_content_data(pContent, priv->pGdkPixbuf, error);
		if(priv->pGdkPixbuf)
                {
                        g_object_unref(priv->pGdkPixbuf);
                        priv->pGdkPixbuf = NULL;
                }
		
	}
	return pContent;
}

/********************************************************
 * function : v_create_texture
 * description: texture creation
 * parameters:  gchar*, gfloat, gfloat, GError**
 * return value: GdkPixbuf*
 ********************************************************/
static GdkPixbuf *create_pixbuf(gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError *pErr)
{
        GdkPixbuf *pixbuf = NULL;
        gchar *pUri = NULL;
        GFile *pFile;

        if(NULL == pFilePath)
        {
                        WARNING ("File path is null");
                        return NULL;
        }

        pFile = g_file_new_for_commandline_arg (pFilePath);

        if (pFile == NULL)
        {
                        WARNING ("File not found");
                        return NULL;
        }
        if (g_file_has_uri_scheme (pFile, "file"))
        {
                pUri = g_file_get_path (pFile);
        }
        else
        {
                pUri = g_strdup(pFilePath);
        }

        if(flWidth == 0 || flHeight == 0)
        {
               pixbuf = gdk_pixbuf_new_from_file(pUri, &pErr);
        }
        else
        {
               pixbuf = gdk_pixbuf_new_from_file_at_scale (pUri, flWidth, flHeight, FALSE, &pErr);
        }

        if(NULL != pUri)
        {
                g_free (pUri);
                pUri = NULL;
        }

        if(pFile)
                g_object_unref(pFile);

        return pixbuf;
}

static gchar *create_content_path(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	gchar *pUri = NULL;
	gchar *pThumbPath = NULL;
	gchar *string =  NULL;

        /* sometimes metatracker does not provide meta data, then show default thumb */
        if(NULL == priv->currentLoadMediaInfo)
        {
                pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
	}
	else
	{
		if (grassmoor_media_info_get_album (priv->currentLoadMediaInfo) &&
                        grassmoor_media_info_get_artist (priv->currentLoadMediaInfo))
                {

			string =  grassmoor_tracker_get_albumart_url (grassmoor_media_info_get_url (priv->currentLoadMediaInfo),  grassmoor_media_info_get_album (priv->currentLoadMediaInfo), grassmoor_media_info_get_artist (priv->currentLoadMediaInfo));
		}
		/* sometimes tracker gives the file which does not exists */
		if (string)
		{
			GFile *pFile = g_file_new_for_commandline_arg (string);
			if (pFile != NULL)
			{
				pUri = g_file_get_path (pFile);
				g_object_unref(pFile);
			}

			if(check_file_exists(pUri) == FALSE)
				pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
			else
				pThumbPath = g_strdup(pUri);
		}
		else
		{
			pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
		}
		if(NULL != pUri)
		{
			g_free(pUri);
			pUri = NULL;
		}

		g_free (string);
	}
	return pThumbPath;
}

static void timeline_idle_cb(ClutterTimeline *timeline, FramptonAudioPlayer *pAudioPlayer)
{
        //FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(pData);
        extract_item((gpointer)pAudioPlayer);
}

/*********************************************************************************************
 * Function:    extract_item
 * Description: extract info from tracker to create item
 * Parameters:  Pointer to the audio player element
 * Return:      void
 ********************************************************************************************/
static void extract_item(gpointer pData)
{
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (pData);
        FramptonAudioPlayerPrivate *priv;
        gchar *pThumbPath = NULL;
        if(! FRAMPTON_IS_AUDIO_PLAYER(pData))
                return;

        priv = pAudioPlayer->priv;

	if(priv->bLoad == FALSE)
        {
                 if(NULL != priv->pTimeline)
                                g_object_unref(priv->pTimeline);
                        priv->pTimeline =  clutter_timeline_new (100);//anim_duration);
                        g_signal_connect (G_OBJECT (priv->pTimeline), "completed", G_CALLBACK (timeline_idle_cb),pAudioPlayer );
                        clutter_timeline_start(priv->pTimeline);
                        return;
        }
	if(priv->inCurrentIndex < priv->inTempTotal)
	{
		DEBUG ("populate url = %s , index =%d", priv->pResultPointer[priv->inCurrentIndex], priv->inCurrentIndex );

		if(NULL != priv->pResultPointer || NULL != priv->pResultPointer[priv->inCurrentIndex])
		{
			g_clear_pointer (&priv->currentLoadMediaInfo, grassmoor_media_info_free);
			/* update the current load url */
			priv->pCurrentLoadUrl = priv->pResultPointer[priv->inCurrentIndex];
			/* get the meta data */
			/* FIXME: Add error handling */
			priv->currentLoadMediaInfo = grassmoor_tracker_get_media_file_info (priv->tracker, priv->pResultPointer[priv->inCurrentIndex++], GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
			pThumbPath = create_content_path(pAudioPlayer);			
			priv->pGdkPixbuf = create_pixbuf(pThumbPath, 164, 164, NULL);
			if(pThumbPath)
			{
				g_free(pThumbPath);
				pThumbPath = NULL;
			}
		}
		/* populate the item */
		if(priv->bResetUI == FALSE)
		{
			if(NULL != priv->pTimeline)
                                g_object_unref(priv->pTimeline);
                        priv->pTimeline =  clutter_timeline_new (10);//anim_duration);
                        g_signal_connect (G_OBJECT (priv->pTimeline), "completed", G_CALLBACK (timeline_cb),pAudioPlayer );
                        clutter_timeline_start(priv->pTimeline);

		}
			//priv->inTimeoutSource = clutter_threads_add_timeout(10, (GSourceFunc)populate_item, pAudioPlayer);
			//priv->inTimeoutSource = g_timeout_add(10, (GSourceFunc)populate_item, pAudioPlayer);
	}
	else if(priv->inStartIndex < priv->inMidIndex)
	{

		DEBUG ("populate url = %s , index %d", priv->pResultPointer[priv->inStartIndex], priv->inStartIndex );
		if(priv->bResetUI == FALSE)
		{
			/* update the current load url */
			priv->pCurrentLoadUrl = priv->pResultPointer[priv->inStartIndex];
		}
		/* reset the current index from start */
		priv->inCurrentIndex = priv->inStartIndex;
		/* reset total count to mid count */
		priv->inTempTotal = priv->inMidIndex;
		/* reset ui count from start */
		priv->inThumbCount = priv->inStartIndex;
		priv->inArtistAlbumCount = priv->inStartIndex;
		priv->inSongCount = priv->inStartIndex;

		/* reset start index so that it will not enter in this block again */
		priv->inStartIndex = priv->inMidIndex;

		if(priv->bResetUI == FALSE)
		{
            g_clear_pointer (&priv->currentLoadMediaInfo,
                             grassmoor_media_info_free);
			/* get the meta data */
			/* FIXME: Add error handling */
			priv->currentLoadMediaInfo = grassmoor_tracker_get_media_file_info (priv->tracker, g_strdup(priv->pResultPointer[priv->inCurrentIndex++]), GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
			pThumbPath = create_content_path(pAudioPlayer);
			priv->pGdkPixbuf = create_pixbuf(pThumbPath, 164, 164, NULL);
			if(pThumbPath)
			{
				g_free(pThumbPath);
				pThumbPath = NULL;
			}
		}
		/* populate the item */
		if(priv->bResetUI == FALSE)
		{
			if(NULL != priv->pTimeline)
                                g_object_unref(priv->pTimeline);
                        priv->pTimeline =  clutter_timeline_new (10);//anim_duration);
                        g_signal_connect (G_OBJECT (priv->pTimeline), "completed", G_CALLBACK (timeline_cb),pAudioPlayer );
                        clutter_timeline_start(priv->pTimeline);

		}
			//priv->inTimeoutSource = clutter_threads_add_timeout(10, (GSourceFunc)populate_item, pAudioPlayer);
			//priv->inTimeoutSource = g_timeout_add(10, (GSourceFunc)populate_item, pAudioPlayer);
	}
	else
	{
		gint rows = 0;
		gint lastRemoveRow = 0;
		ThornburyModel *model = NULL;
		gint inColumn = 0;
		gint rowToFocus;

		priv->disableLoadCheck = TRUE;
		if(priv->inExtraDummy > 0)
		{
			for(; priv->inExtraDummy > 0; priv->inExtraDummy--)
				thornbury_model_remove(priv->pThumbRollerModel, thornbury_model_get_n_rows(priv->pThumbRollerModel) - 1);
		}	

		/* from artist-album roller */
                if(FRAMPTON_AUDIO_PLAYER_MODE_ARTIST  == priv->enCurrentMode ||
                        FRAMPTON_AUDIO_PLAYER_MODE_ALBUM  == priv->enCurrentMode)
                {
			if(priv->pArtistAlbumHash)
				rows = g_hash_table_size(priv->pArtistAlbumHash);

			lastRemoveRow = priv->inMidIndex - 1;
			for(; lastRemoveRow >= priv->inArtistAlbumCount; lastRemoveRow--)
			{
				if(priv->pAlbumArtistRollerModel)
				{
					thornbury_model_remove(priv->pAlbumArtistRollerModel, lastRemoveRow);
					if(priv->prevSongRow > -1)
	                                        priv->prevSongRow--;
				}
			}

			lastRemoveRow = priv->inTotalCount - 1;
			for(; lastRemoveRow >= rows; lastRemoveRow --)
			{
				if(priv->pAlbumArtistRollerModel)
				{
					thornbury_model_remove(priv->pAlbumArtistRollerModel, lastRemoveRow);
				}
			}
		}

		rows = 0;
		/* from thumb roller */
		if(priv->pCoverThumbList)
			rows = g_list_length( priv->pCoverThumbList);

		lastRemoveRow = priv->inMidIndex - 1;
		for(; lastRemoveRow >= priv->inThumbCount; lastRemoveRow--)
		{
			if(priv->pThumbRollerModel)
			{
				thornbury_model_remove(priv->pThumbRollerModel, lastRemoveRow);
				if(priv->prevRow > -1)
					priv->prevRow--;	
			}
		}
		lastRemoveRow = priv->inTotalCount - 1;
		for(; lastRemoveRow >= rows; lastRemoveRow--)
                {
			if(priv->pThumbRollerModel)
			{
	                        thornbury_model_remove(priv->pThumbRollerModel, lastRemoveRow);
			}	
                }
		/* Make thumbs divisible by 4 */
		priv->extraThumbItems = thornbury_model_get_n_rows(priv->pThumbRollerModel) % 4 ;
		if(priv->extraThumbItems > 0)
			priv->extraThumbItems = 4 - priv->extraThumbItems;

		add_remove_thumbs_divisible_by_4(pAudioPlayer, TRUE);

		if(FRAMPTON_AUDIO_PLAYER_MODE_SONGS == priv->enCurrentMode)
		{
			model = priv->pSongRollerModel;
			inColumn = 3; 
		}
		else
		{
			model = priv->pAlbumArtistRollerModel;
			inColumn = 2;
	        }

		rowToFocus = get_current_playing_item_number(priv->pThumbRollerModel, pAudioPlayer, 1);
		if(rowToFocus >= 0)
			thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", rowToFocus, NULL);
	
		create_sort_list(model, inColumn, pAudioPlayer);

	}
	return;
}

/*********************************************************************************************
 * Function:    populate_item
 * Description: Popluate the next item on the roller
 * Parameters:  Pointer to the audio player element
 * Return:              boolean value
 ********************************************************************************************/
static gboolean populate_item(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv;
        if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return FALSE;

	priv = pAudioPlayer->priv;
	if(priv->bResetUI)
		return FALSE;

	if(priv->pContent)
	{
                g_object_unref(priv->pContent);
                priv->pContent = NULL;
	}

	priv->pContent = create_content(pAudioPlayer);

	/* ti know how many url meta data is null */
	if(NULL == priv->currentLoadMediaInfo)
		priv->inUnknownCount++;
	if(priv->bResetUI)
		return FALSE;

	/* populate thumb roller */
	if(populate_audio_thumb_roller(pAudioPlayer))
	{
		if(priv->bResetUI)
			return FALSE;

		/* populate list rollers */
		populate_artist_album_roller(pAudioPlayer);
	}
	if(priv->bResetUI)
		return FALSE;

	/* populate songs roller */
	populate_song_roller(pAudioPlayer);
	if(priv->bResetUI)
		return FALSE;

	/* set the focus */
	if(priv->pSongsHash && g_hash_table_size(priv->pSongsHash) == 1)
	{
		g_timeout_add(500, (GSourceFunc)set_roller_focus, pAudioPlayer);

		if(NULL != priv->pCurrentTrack)
		{
			update_media_overlay(pAudioPlayer, priv->inMidIndex, priv->pCurrentTrack);
			update_highlight_arrow(pAudioPlayer, priv->inMidIndex, priv->pCurrentTrack);
			if(priv->inLUMPlayState == FRAMPTON_AGENT_PAUSED)
			{
				update_media_overlay_play_state(FALSE);
				update_highlight_arrow_state(FALSE);
			}
		}
	}
	if(priv->bResetUI)
		return FALSE;

	/* get data in thread from tracker */
	//extract_item(pAudioPlayer);
	priv->pThread = g_thread_new("item", (GThreadFunc)extract_item, pAudioPlayer) ;

	return FALSE;
}

# if 0
/*********************************************************************************************
 * Function:    add_dummy_items
 * Description: add dummy items to all rollers to avoid flickering during load
 * Parameters:  Pointer to the audio player element
 *
 * Return:      void
 ********************************************************************************************/
static void add_dummy_items(FramptonAudioPlayer *pAudioPlayer)
{
        if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return;

        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	gint index;
	gchar *pThumbPath = NULL;
	GError *error = NULL;

	priv->inExtraDummy = priv->inTotalCount % 4;
	if(priv->inExtraDummy > 0)
	{
		priv->inExtraDummy = 4 - priv->inExtraDummy;
	}


	pThumbPath = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
	ClutterContent *pContent = thornbury_texture_create_content_sync(pThumbPath, 164, 164, &error);
	if(error)
	{
		g_print("thumb creation error = %s\n", error->message);
		g_error_free(error);
	}

	/* add dummy items to all rollers */
	for(index = 0; index < priv->inTotalCount; index++)
	{
		if(priv->bResetUI)
			return;
		
                thornbury_model_append (priv->pThumbRollerModel,
                                        THUMB_ROLLER_ICON, CLUTTER_IMAGE(pContent),
					THUMB_ROLLER_URL, NULL,
					THUMB_ROLLER_OVERLAY_SHOW, FALSE,
					THUMB_ROLLER_OVERLAY_TEXT, NULL,
					THUMB_ROLLER_OVERLAY_STATE, FALSE,
                                        -1);
		if(priv->bResetUI)
			return;

		thornbury_model_append (priv->pSongRollerModel,
                                        SONGS_ROLLER_COLUMN_ID, NULL,
                                        SONGS_ROLLER_COLUMN_NUM, NULL,
                                        SONGS_ROLLER_COLUMN_LABEL, NULL,
					SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
					SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
					SONGS_ROLLER_RELATED_THUMB_INDEX, 0,
                                         -1);

		if(priv->bResetUI)
			return;

		thornbury_model_append (priv->pAlbumArtistRollerModel,
                                ARTISTALBUM_ROLLER_COL_ID,NULL ,
                                ARTISTALBUM_ROLLER_COL_MID_TEXT, NULL,
                                ARTISTALBUM_ROLLER_COL_RIGHT_TEXT, NULL,
				ARTISTALBUM_ROLLER_COL_CONTENT, CLUTTER_IMAGE(pContent),
                                -1);
	}

	for(index = 0; index < priv->inExtraDummy; index++)
	{

		thornbury_model_append (priv->pThumbRollerModel,
				THUMB_ROLLER_ICON, CLUTTER_IMAGE(pContent),
				THUMB_ROLLER_URL, NULL,
				THUMB_ROLLER_OVERLAY_SHOW, FALSE,
				THUMB_ROLLER_OVERLAY_TEXT, NULL,
				THUMB_ROLLER_OVERLAY_STATE, FALSE,
				-1);
	}

	if(pThumbPath)
        {
                g_free(pThumbPath);
                pThumbPath = NULL;
        }
        if(pContent)
        {
                g_object_unref(pContent);
                pContent = NULL;
        }

	
}
# endif

static void add_thumb_dummy_item(FramptonAudioPlayer *pAudioPlayer, ClutterContent *pContent, gint index)
{
        FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE(pAudioPlayer);

         thornbury_model_append (priv->pThumbRollerModel,
                         THUMB_ROLLER_ICON, CLUTTER_IMAGE(pContent),
                         THUMB_ROLLER_URL,NULL,// priv->pModeUrlList[index],
                         THUMB_ROLLER_OVERLAY_SHOW, FALSE,
                         THUMB_ROLLER_OVERLAY_TEXT, NULL,
                         THUMB_ROLLER_OVERLAY_STATE, FALSE,
                         -1);
}

static void add_song_dummy_item(FramptonAudioPlayer *pAudioPlayer, gint index)
{
        FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE(pAudioPlayer);

        thornbury_model_append (priv->pSongRollerModel,
                        SONGS_ROLLER_COLUMN_ID, NULL,
                        SONGS_ROLLER_COLUMN_NUM, NULL,
                        SONGS_ROLLER_COLUMN_LABEL, NULL,
                        SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW, FALSE,
                        SONGS_ROLLER_HIGHLIGHT_ARROW_STATE, FALSE,
                        SONGS_ROLLER_RELATED_THUMB_INDEX, 0,
                        -1);
}

static void add_album_artist_dummy_item(FramptonAudioPlayer *pAudioPlayer, ClutterContent *pContent, gint index)
{
        FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE(pAudioPlayer);
        thornbury_model_append (priv->pAlbumArtistRollerModel,
                        ARTISTALBUM_ROLLER_COL_ID,NULL ,
                        ARTISTALBUM_ROLLER_COL_MID_TEXT, NULL,
                        ARTISTALBUM_ROLLER_COL_RIGHT_TEXT, NULL,
                        ARTISTALBUM_ROLLER_COL_CONTENT, CLUTTER_IMAGE(pContent),
                        -1);
}

/*********************************************************************************************
 * Function:    frampton_audio_player_create_dummy_items
 * Description: add dummy items for all view of current tab 
 * Parameters:  FramptonAudioPlayer object
 * Return:      void
 ********************************************************************************************/
static void frampton_audio_player_create_dummy_items(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = FRAMPTON_AUDIO_PLAYER_PRIVATE(pAudioPlayer);
        gint index;
        GError *error = NULL;
        gchar *pThumb = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
        ClutterContent *pContent = thornbury_texture_create_content_sync(pThumb, 164, 164, &error);
        if(error)
        {
                g_error_free(error);
        }

        if(priv->bResetUI)
                return;

        for(index = 0; index < priv->inModeUrlCount; index++)
        {
                add_thumb_dummy_item(pAudioPlayer, pContent, index);
                add_album_artist_dummy_item(pAudioPlayer, pContent, index);

        }
        for(index = 0; index < priv->inTotalCount; index++)
        {
                add_song_dummy_item(pAudioPlayer, index);
        }

        if(pThumb)
        {
                g_free(pThumb);
                pThumb = NULL;
        }
        if(pContent)
        {
                g_object_unref(pContent);
                pContent = NULL;
        }
	        /* add extra dummy thumb to fill the row multiple by 4 to overcome last row come in visible area partially */
        for(index = 0; index < priv->inExtraDummy; index++)
        {
                pThumb = g_strconcat(priv->pDataDir, "/IconBig_Music.png", NULL);
                pContent = thornbury_texture_create_content_sync(pThumb, 164, 164, &error);
                if(error)
                {
                	WARNING ("thumb creation error: %s", error->message);
                        g_error_free(error);
                }


                thornbury_model_append (priv->pThumbRollerModel,
                                THUMB_ROLLER_ICON, CLUTTER_IMAGE(pContent),
                                THUMB_ROLLER_URL, NULL,
                                THUMB_ROLLER_OVERLAY_SHOW, FALSE,
                                THUMB_ROLLER_OVERLAY_TEXT, NULL,
                                THUMB_ROLLER_OVERLAY_STATE, FALSE,
                                -1);

                if(pThumb)
                {
                        g_free(pThumb);
                        pThumb = NULL;
                }
                if(pContent)
                {
                        g_object_unref(pContent);
                        pContent = NULL;
                }
        }
        // Force set focus to dummy item 0
        thornbury_set_property(priv->pThornburyViewManager, THUMB_ROLLER, "focused-row", 0, NULL);
}

/*********************************************************************************************
 * Function:    get_extra_dummy_to_add_in_thumb_roller
 * Description: extra dummy to fill full roller resolve focus for last line 
 * Parameters:  FramptonAudioPlayer object
 * Return:      void
 ********************************************************************************************/
static gint get_extra_dummy_to_add_in_thumb_roller(gint inTotalItems)
{
        gint inDummyItems = 0;
        if(inTotalItems > 0)
        {
                inDummyItems = inTotalItems % 4;
                if(inDummyItems > 0)
                        inDummyItems = 4 - inDummyItems;
        }
        return inDummyItems;
}

/*********************************************************************************************
 * Function:    frampton_audio_player_display_tracks_list
 * Description: Display the tracks list on the UI
 * Parameters:  Pointer to the audio player element and the array of
 *              elements
 * Return:      void
 ********************************************************************************************/
static void
frampton_audio_player_display_tracks_list (FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv;
        if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return;

        priv = pAudioPlayer->priv;
	if(NULL != priv->pResultPointer)
	{
		gint i = 0;

		priv->inCurrentIndex = 0;
		priv->inStartIndex = 0;
		priv->inThumbCount = 0;
		priv->inArtistAlbumCount = 0;
		priv->inUnknownCount = 0;
		priv->inSongCount = 0;
# if 1
		/* add dummy items */
                if(priv->enCurrentMode == FRAMPTON_AUDIO_PLAYER_MODE_ALBUM || priv->enCurrentMode == FRAMPTON_AUDIO_PLAYER_MODE_ARTIST)
                        priv->inExtraDummy = get_extra_dummy_to_add_in_thumb_roller(priv->inModeUrlCount);
                else
                        priv->inExtraDummy = get_extra_dummy_to_add_in_thumb_roller(priv->inTotalCount);
                 frampton_audio_player_create_dummy_items(pAudioPlayer);

# endif
		/* add dummy items */
		//add_dummy_items(pAudioPlayer);
		/* check if song is already getting played */
		if(NULL != priv->pCurrentTrack)
		{
			/* update the current index for load from the song getting played */
			gchar **pUrlArr = priv->pResultPointer;
			while(pUrlArr && pUrlArr[i])
			{
				if(g_strcmp0(pUrlArr[i], priv->pCurrentTrack) == 0)
				{
					break;
				}

				i++;
			}
		}
		/* initialize the current index */
		priv->inCurrentIndex = i;
		/* set the index from where the ui population will start */
		priv->inMidIndex = i;
		/* set the ui count index */
		priv->inThumbCount = priv->inMidIndex;
		priv->inArtistAlbumCount = priv->inMidIndex;
		priv->inSongCount = priv->inMidIndex;

		/* copy the total count in temp */
		priv->inTempTotal = priv->inTotalCount;


		/*Create the thread for the retrieval of data for the first item */
		priv->pThread = g_thread_new("item", (GThreadFunc)extract_item, pAudioPlayer);
		//extract_item(pAudioPlayer);
	}
}

void launch_ui_for_open_with(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
        GError *error = NULL;

        if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return;

        priv = pAudioPlayer->priv;

	/* Initialise the meta tracker */
        //grassmoor_tracker_init(error, NULL);

	if(NULL == error)
	{
		/* if no error/ populate ui */
		GHashTable *pHash = frampton_audio_player_get_open_with_hash();
		if(pHash)
		{
			gchar *pUrl = frampton_audio_player_get_open_with_url(pHash);
			if(NULL != pUrl)
			{
				//priv->pCurrentTrack = g_strdup(pUrl);
				priv->bResetUI = FALSE;
				populate_thumb_roller_for_open_with(pUrl, pAudioPlayer);
				populate_list_roller_for_open_with(pUrl, pAudioPlayer);
				populate_song_roller_for_open_with(pUrl, pAudioPlayer);
				//frampton_audio_player_initiate_play(pAudioPlayer, pUrl);
				thornbury_switch_view_no_animation(priv->pThornburyViewManager, (gchar *) "DetailView", TRUE, 0);
			}
		}
	}

}

/*********************************************************************************************
 * Function:    build_ui
 * Description: Build the audio player UI
 * Parameters:  Audio Player
 * Return:      Bool value (to indicate. if TRUE, this function is
 *              called repeatedly again and again, if FALSE stop after
 *              load
 ********************************************************************************************/
gboolean build_ui(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
		return FALSE;

	//GError *error = NULL;
	priv = pAudioPlayer->priv;

	priv->bResetUI = FALSE;

	if(FALSE == priv->externalModeChange)
	{
		/* Initialise the meta tracker */
		//grassmoor_tracker_init(error, NULL);
	}

	/* if no error/ populate ui */
	if(NULL != priv->pResultPointer)
	{
		/* populate ui */
		frampton_audio_player_display_tracks_list(pAudioPlayer);
	}
	else
	{
		;/* TBD: show popup */
		//g_timeout_add(1, (GSourceFunc)show_no_media_popup, NULL);
	}

	return FALSE;
}

/*******************************************************************
 *Function: init_new_hash_tables
 *Description: initialise new hash tables
 *Paramters: FramptonAudioPlayer element
 *Return Value: void
 *******************************************************************/
void init_new_hash_tables(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	/* hash for list rollercoupling */
        priv->pArtistAlbumHash = g_hash_table_new (g_str_hash, g_str_equal);
        priv->pSongsHash = g_hash_table_new (g_str_hash, g_str_equal);
	/* model hash for each view widgets */
	priv->pListModelHash = g_hash_table_new(g_str_hash,g_str_equal);
	priv->pThumbModelHash = g_hash_table_new(g_str_hash,g_str_equal);
	priv->pDetailModelHash = g_hash_table_new(g_str_hash,g_str_equal);
	
	priv->pSortListHash = g_hash_table_new(g_str_hash,g_str_equal);
}

/*******************************************************************
 *Function: set_initial_configuration
 *Description: Set all initial config params
 *Paramters: Pointer to audio player
 *Return Value: None
 *******************************************************************/
void set_initial_configuration(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;


        /* flags needed to control the list view behavior */
        priv->setAlbumArtistMenu = TRUE;
        priv->setSongsMenu = TRUE;
	priv->externalModeChange =FALSE;
	priv->pResultPointer = NULL;

	priv->pTimeline = NULL;

	if(NULL != priv->appdata)
	{
		if(NULL != priv->appdata->app)
		{
			//clutter_actor_destroy(priv->appdata->app);
			//priv->appdata->app = NULL;
		}
		if(NULL != priv->appdata->default_view)
		{
			g_free(priv->appdata->default_view);
			priv->appdata->default_view = NULL;
		}
		if(NULL != priv->appdata->viewspath)
		{
			g_free(priv->appdata->viewspath);
			priv->appdata->viewspath = NULL;
		}
		if(NULL != priv->appdata->app_name)
		{
			g_free(priv->appdata->app_name );
			priv->appdata->app_name  = NULL;
		}
		if(NULL != priv->appdata)
		{
			g_free(priv->appdata);
			priv->appdata = NULL;
		}
	}

	priv->currentPlayingInfo = NULL;
	priv->currentLoadMediaInfo = NULL;
	priv->pCurrentTrack = NULL;
	priv->pCurrentLoadUrl = NULL;
        priv->pCoverThumbList = NULL;
        priv->pThumbsList = NULL;
	priv->trackDuration = NULL;
	priv->pThread = NULL;
	priv->pGdkPixbuf = NULL;
	priv->pCurrentView = NULL;
	priv->duplicateCount = 0;
	priv->inStartIndex = 0;
 	priv->inThumbCount = 0;
	priv->inArtistAlbumCount = 0;
	priv->inTempTotal = 0;
	priv->inMidIndex = 0;
	//priv->inTimeoutSource = 0;
	priv->inLUMPlayState = FRAMPTON_AGENT_NONE;
	priv->extraThumbItems = 0;
	priv->inExtraDummy = 0;

	priv->bSeekStarted = FALSE;
	priv->disableLoadCheck = FALSE;
    priv->bLoad = TRUE;
	priv->bNewUri = FALSE;

        /* Set the shuffle and repeat states to FALSE initially */
    priv->bRepeat = FALSE;
    priv->bShuffle = FALSE;
	priv->bAnimation = FALSE;
	priv->bResetUI = TRUE;
	priv->bRemoveState = FALSE;
	priv->prevRow = -1;
	priv->prevSongRow = -1;
	priv->prevMetaRow = -1;
}

/*********************************************************************************************
 * Function:    frampton_audio_player_init_ui
 * Description: Load the UI
 * Parameters:  Pointer to the audio player
 * Return:      void
 ********************************************************************************************/
void frampton_audio_player_init_ui(FramptonAudioPlayer *pAudioPlayer)
{
	FramptonAudioPlayerPrivate *priv;
        GList *execList;

        if(! FRAMPTON_IS_AUDIO_PLAYER(pAudioPlayer))
                return;

	priv = pAudioPlayer->priv;

	/* Get the exec mode */
        execList = frampton_audio_player_get_startup_exec_list();
        if(NULL != execList)
        {
		priv->enCurrentMode = get_requested_mode(execList);
		DEBUG ("current mode = %d", priv->enCurrentMode);
	}
	else
		priv->enCurrentMode = FRAMPTON_AUDIO_PLAYER_MODE_NONE;

	 /* Init all Hash tables */
        init_new_hash_tables(pAudioPlayer);
	/* Set all required initial configurations */
        set_initial_configuration(pAudioPlayer);

	/* init views */
	initialize_views(pAudioPlayer);
}


