/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "frampton.h"

extern gchar *FRAMPTON_AUDIO_PLAYER_APP_NAME;

/*********************************************************************************************
 * Function:  shuffle_message_info_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void shuffle_message_info_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
        GError *error = NULL;
        frampton_agent_call_set_shuffle_state_finish((FramptonAgent *)source_object, res, &error);
        if (error)
        {
        	WARNING ("ERROR is %s", error->message);
        	g_error_free (error);
        }
}

/*********************************************************************************************
 * Function:  repeat_message_info_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void repeat_message_info_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
        GError *error = NULL;
        frampton_agent_call_set_repeat_state_finish((FramptonAgent *)source_object, res, &error);
        if (error)
        {
        	WARNING ("ERROR is %s", error->message);
        	g_error_free (error);
        }
}

/*********************************************************************************************
 * Function:  audio_agent_seek_info_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void audio_agent_seek_info_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
        GError *error = NULL;
        frampton_agent_call_track_seek_finish((FramptonAgent *)source_object, res, &error);
        if (error)
        {
        	WARNING ("ERROR is %s", error->message);
        	g_error_free (error);
        }
}

/*********************************************************************************************
 * Function:  update_seek_to_audio_agent
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void update_seek_to_audio_agent(gfloat position, FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	frampton_agent_call_track_seek(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, position, NULL, audio_agent_seek_info_cb, pAudioPlayer);
}

/*********************************************************************************************
 * Function:  audio_agent_resume_info_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void audio_agent_resume_info_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
        GError *error = NULL;
        frampton_agent_call_resume_playback_finish((FramptonAgent *)source_object, res, &error);
        if (error)
        {
        	WARNING ("ERROR is %s", error->message);
        	g_error_free (error);
        }
}

/*********************************************************************************************
 * Function:  update_resume_to_audio_agent
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void update_resume_to_audio_agent(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

        frampton_agent_call_resume_playback(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, NULL, audio_agent_resume_info_cb, pAudioPlayer);
}

/*********************************************************************************************
 * Function:  audio_agent_pause_info_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void audio_agent_pause_info_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
        GError *error = NULL;
        frampton_agent_call_pause_playback_finish((FramptonAgent *)source_object, res, &error);
        if (error)
        {
        	WARNING ("ERROR is %s", error->message);
        	g_error_free (error);
        }
}

/*********************************************************************************************
 * Function:  update_pause_to_audio_agent
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void update_pause_to_audio_agent(FramptonAudioPlayer *pAudioPlayer)
{
        FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

        frampton_agent_call_pause_playback(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, NULL, audio_agent_pause_info_cb, pAudioPlayer);
}

/*********************************************************************************************
 * Function:  play_message_info_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void play_message_info_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
        GError *error = NULL;
        frampton_agent_call_start_playback_finish((FramptonAgent *)source_object, res, &error);
        if (error)
        {
        	WARNING ("ERROR is %s", error->message);
        	g_error_free (error);
        }
}

/*********************************************************************************************
 * Function:  play_url_info_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void play_url_info_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
        GError *error = NULL;
        frampton_agent_call_play_url_finish((FramptonAgent *)source_object, res, &error);
        if (error)
        {
        	WARNING ("ERROR is %s", error->message);
        	g_error_free (error);
        }
}

/*********************************************************************************************
 * Function:  frampton_audio_player_initiate_play
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void frampton_audio_player_initiate_play(FramptonAudioPlayer *pAudioPlayer, gchar *pUrl)
{
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	
	if(NULL != pUrl)
	{
		frampton_agent_call_start_playback(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, pUrl, NULL, play_message_info_cb, pAudioPlayer);
	}
}

/*********************************************************************************************
 * Function:  audio_agent_play_started_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void audio_agent_play_started_cb( FramptonAgent *object,
			    const gchar *arg_app_name,
			    const gchar *arg_track_name)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default ();
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	DEBUG ("play started for track %s", arg_track_name);
	if(g_strcmp0(priv->pCurrentView, "DetailView") == 0)
		update_current_track_details(pAudioPlayer, (gchar*)arg_track_name);

	
	update_progress_bar_play_state(TRUE);
	//update_media_overlay_play_state(TRUE);
	/* Set the focus */
	set_thumb_to_focus_on_view_change((gchar*)arg_track_name);
	set_list_to_focus_on_view_change(pAudioPlayer, TRUE);
}

/*********************************************************************************************
 * Function:  audio_agent_play_completed_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void audio_agent_play_completed_cb( FramptonAgent *object,
			    const gchar *arg_app_name,
			    const gchar *arg_track_name)
{
	FramptonAudioPlayer *pAudioPlayer = frampton_audio_player_get_default ();
	FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;
	GHashTable *pHash;
    DEBUG ("play completed for track %s", arg_track_name);
	/* reset the progress bar */
	frampton_audio_player_update_progress_bar(0.0);

	if(NULL != priv->pCurrentTrack)
	{
		g_free(priv->pCurrentTrack);
		priv->pCurrentTrack = NULL;
	}

	pHash = frampton_audio_player_get_open_with_hash();
	if(pHash)
	{
		disconnect_audio_agent_signals(priv->audio_proxy, pAudioPlayer);
	}
}

/*********************************************************************************************
 * Function:  audio_agent_play_paused_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void audio_agent_play_paused_cb( FramptonAgent *object,
		    const gchar *arg_app_name,
		    const gchar *arg_track_name,
		    gdouble arg_track_pos)
{
    DEBUG ("Track name = %s, track pos =  %lf",arg_track_name,arg_track_pos);
	if(g_strcmp0(arg_app_name, FRAMPTON_AUDIO_PLAYER_APP_NAME) == 0)
	{
		update_progress_bar_play_state(FALSE);
		update_media_overlay_play_state(FALSE);
		update_highlight_arrow_state(FALSE);
		update_meta_highlight_arrow_state(FALSE);
	}
}

/*********************************************************************************************
 * Function:  audio_agent_play_resumed_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void audio_agent_play_resumed_cb(FramptonAgent *object,
		    const gchar *arg_app_name,
		    const gchar *arg_track_name,
		    gdouble arg_track_pos)
{
    DEBUG ("track name = %s, track pos = %lf",arg_track_name,arg_track_pos);
	if(g_strcmp0(arg_app_name, FRAMPTON_AUDIO_PLAYER_APP_NAME) == 0)
	{
        	update_progress_bar_play_state(TRUE);
		update_media_overlay_play_state(TRUE);
		update_highlight_arrow_state(TRUE);
		update_meta_highlight_arrow_state(TRUE);
	}
}

/*********************************************************************************************
 * Function:  audio_agent_play_position_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
static void audio_agent_play_position_cb(FramptonAgent *object,
		    	const gchar *arg_app_name,
		    	const gchar *arg_track_name,
    			gdouble arg_track_pos)
{
	if(g_strcmp0(arg_app_name, FRAMPTON_AUDIO_PLAYER_APP_NAME) == 0)
	{
		frampton_audio_player_update_progress_bar(arg_track_pos);
	}
}

/*********************************************************************************************
 * Function:  audio_agent_playback_error_cb
 * Description:         
 * Parameters:          
 * Return:      void    
 ********************************************************************************************/
static void audio_agent_playback_error_cb( FramptonAgent *object,
		    const gchar *arg_app_name,
		    const gchar *arg_track_name,
		    const gchar *arg_err_msg)
{
        WARNING ("Error during playing track %s in app %s: %s", arg_track_name, arg_app_name, arg_err_msg);
}

# if 0
/*********************************************************************************************
 * Function:  audio_agent_repeat_message_info_cb
 * Description:         
 * Parameters:          
 * Return:      void    
 ********************************************************************************************/
static void audio_agent_repeat_message_info_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
        GError *error = NULL;
        frampton_agent_call_set_repeat_state_finish((FramptonAgent *)source_object,res,&error);
        if (error)
        {
        	WARNING ("error is : %s", error->message);
        	g_error_free (error);
        }
}
# endif

/*********************************************************************************************
 * Function: audio_agent_get_url_message_info_cb 
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void audio_agent_get_url_message_info_cb(GObject *source_object,
                          GAsyncResult *res,
                          gpointer user_data)
{
        FramptonAudioPlayer *audioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);
        FramptonAudioPlayerPrivate *priv;
        GError *error = NULL;
        gchar **pResultPointer = NULL;
        gchar **pModeUrlList = NULL;

        if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
                return;

        priv = audioPlayer->priv;

        if(NULL != priv->pResultPointer)
        {
                /* TBD: free the list */
                priv->pResultPointer = NULL;
        }

        frampton_agent_call_get_track_list_finish(priv->audio_proxy, &pResultPointer, &priv->inTotalCount, &pModeUrlList, &priv->inModeUrlCount, res, &error);
        if(error)
        {
                WARNING ("get track list : Error: %s: code %d: %s", g_quark_to_string(error->domain), error->code, error->message);
                g_error_free (error);
        }
	
	if(pResultPointer)
        {
                DEBUG ("total url len = %d mode url len = %d", priv->inTotalCount, priv->inModeUrlCount);
                if(NULL == priv->pResultPointer)
                {
                        priv->pResultPointer = pResultPointer;
                        priv->pModeUrlList = pModeUrlList;

                        /* update ui */
						build_ui(FRAMPTON_AUDIO_PLAYER(user_data));
                }
                else
                {
                        if(priv->pResultPointer)
                        {
                                g_strfreev(priv->pResultPointer);
                                priv->pResultPointer = NULL;
                        }
                        if(priv->pModeUrlList)
                        {
                                g_strfreev(priv->pModeUrlList);
                                priv->pModeUrlList = NULL;
                        }

                        priv->pResultPointer = pResultPointer;
                        priv->pModeUrlList = pModeUrlList;

                        /* update ui */
			build_ui(FRAMPTON_AUDIO_PLAYER(user_data));
                }
        }
}

void audio_agent_get_state_cb ( GObject *object, GAsyncResult *res, gpointer user_data)

{
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);
        FramptonAudioPlayerPrivate *priv;
        gboolean out_shuffle_state;
        gboolean out_repeat_state;
        gchar *out_track_name;
        gdouble out_play_positon;
        gint out_info_type;
        gint out_query_type;
        GrassmoorMediaInfoType mediaInfoType;
	if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
	{
		return;
	}
        priv = pAudioPlayer->priv;


	frampton_agent_call_get_player_state_finish ( priv->audio_proxy, &out_shuffle_state,
		&out_repeat_state,
    		&out_track_name,
    		&out_play_positon,
		&out_info_type,
    		&out_query_type,
		&priv->inLUMPlayState,
    		res,
    		NULL);

	/* update bottom bar icon */
	update_bottom_bar(pAudioPlayer, out_shuffle_state, out_repeat_state);

	DEBUG ("Last user mode play state = %d", priv->inLUMPlayState);
	if(g_strcmp0(out_track_name, "") == 0)
		out_track_name = NULL;	

	 if(NULL != out_track_name)
        {
                DEBUG ("track name =%s", out_track_name);
                update_current_track_details(pAudioPlayer, out_track_name);
		/* set list roller item to the focus */
        	//g_timeout_add(500, (GSourceFunc)set_list_to_focus_on_view_change, pAudioPlayer);
		//set_list_to_focus_on_view_change(pAudioPlayer);
		
        }


	/* media mode and source variable */

	/* externalMode Change is set active only when at runtime a new
	 * sorting order has been selected */
	if(FALSE == priv->externalModeChange)
	{
		/* Get the exec mode */
		GList *execList = frampton_audio_player_get_startup_exec_list();

		if(NULL != execList)
		{
			priv->enCurrentMode = get_requested_mode(execList);
			if(g_strcmp0("NULL",g_list_nth_data(execList, 3)) != 0)
				priv->pCurrentSource = g_strdup(g_list_nth_data(execList, 3));

		}
		DEBUG ("CurrentSource = %s", priv->pCurrentSource );
	}

	if(FRAMPTON_AUDIO_PLAYER_MODE_NONE != priv->enCurrentMode)
	{
		/* TBD: replace value with agent enum */ 
		if(FRAMPTON_AUDIO_PLAYER_MODE_SONGS == priv->enCurrentMode)
			mediaInfoType = FRAMPTON_AGENT_MEDIA_INFO_TITLE;//4;
		else if (FRAMPTON_AUDIO_PLAYER_MODE_ARTIST == priv->enCurrentMode)
			mediaInfoType = FRAMPTON_AGENT_MEDIA_INFO_ARTIST;//5;
		else
			mediaInfoType = FRAMPTON_AGENT_MEDIA_INFO_ALBUM;//6;

		DEBUG ("media info type = %d current source = %s", mediaInfoType, priv->pCurrentSource);

		/* get url list based on current selected mode */
		frampton_agent_call_get_track_list(pAudioPlayer->priv->audio_proxy,
				FRAMPTON_AUDIO_PLAYER_APP_NAME, priv->pCurrentSource == NULL ? "local" : priv->pCurrentSource,
				FRAMPTON_AGENT_SORT_ASCENDING,//0,//FRAMPTON_AGENT_SORT_ASCENDING,
				mediaInfoType, 
				NULL,
				audio_agent_get_url_message_info_cb,
				user_data);
	}	

}

/*********************************************************************************************
 * Function: audio_agent_registration_info_message_cb 
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void audio_agent_registration_info_message_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
        FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);
        FramptonAudioPlayerPrivate *priv;
        gboolean regState;

        if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
                return;

        priv = pAudioPlayer->priv;

        frampton_agent_call_register_finish(priv->audio_proxy, &regState, res , NULL);

        if(regState)
        {
                GHashTable *pHash;
                DEBUG ("registration for %s to Audio service is successfull", FRAMPTON_AUDIO_PLAYER_APP_NAME);

		pHash = frampton_audio_player_get_open_with_hash ();
		if(pHash)
		{

			gchar *pUrl = frampton_audio_player_get_open_with_url(pHash);
			if(NULL != pUrl && (g_strcmp0(priv->pCurrentView,"DetailView") != 0))
			{
				launch_ui_for_open_with(pAudioPlayer);
				frampton_agent_call_play_url(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, pUrl, NULL, play_url_info_cb, pAudioPlayer);
			}
		}
		else
		{
			frampton_agent_call_get_player_state(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, NULL, audio_agent_get_state_cb, pAudioPlayer);
		}
        }
        else
                WARNING("registration for %s to Audio service is failed! ", FRAMPTON_AUDIO_PLAYER_APP_NAME);
}

void disconnect_audio_agent_signals(FramptonAgent *audio_proxy, FramptonAudioPlayer *audioPlayer)
{
        if(NULL != audio_proxy)
        {
		g_signal_handlers_disconnect_by_func(audio_proxy, G_CALLBACK(audio_agent_play_started_cb), audioPlayer);
		g_signal_handlers_disconnect_by_func(audio_proxy, G_CALLBACK(audio_agent_play_completed_cb), audioPlayer);
		g_signal_handlers_disconnect_by_func(audio_proxy, G_CALLBACK(audio_agent_play_paused_cb), audioPlayer);
		g_signal_handlers_disconnect_by_func(audio_proxy, G_CALLBACK(audio_agent_play_resumed_cb), audioPlayer);
		g_signal_handlers_disconnect_by_func(audio_proxy, G_CALLBACK(audio_agent_play_position_cb), audioPlayer);
		g_signal_handlers_disconnect_by_func(audio_proxy, G_CALLBACK(audio_agent_playback_error_cb), audioPlayer);
	}
}

void connect_audio_agent_signals(FramptonAgent *audio_proxy, FramptonAudioPlayer *audioPlayer)
{
	if(NULL != audio_proxy)
	{
		/* connect sigmnals */
		g_signal_connect(audio_proxy, "play-started", G_CALLBACK(audio_agent_play_started_cb), audioPlayer);
		g_signal_connect(audio_proxy, "play-completed", G_CALLBACK(audio_agent_play_completed_cb), audioPlayer);
		g_signal_connect(audio_proxy, "play-paused", G_CALLBACK(audio_agent_play_paused_cb), audioPlayer);
		g_signal_connect(audio_proxy, "play-resumed", G_CALLBACK(audio_agent_play_resumed_cb), audioPlayer);
		g_signal_connect(audio_proxy, "play-position", G_CALLBACK(audio_agent_play_position_cb), audioPlayer);
		g_signal_connect(audio_proxy, "play-error", G_CALLBACK(audio_agent_playback_error_cb), audioPlayer);
	}
}

/*********************************************************************************************
 * Function:   audio_agent_service_proxy_cb
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void audio_agent_service_proxy_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
        FramptonAudioPlayer *audioPlayer = FRAMPTON_AUDIO_PLAYER(user_data);
        FramptonAudioPlayerPrivate *priv;
        GError *error = NULL;

         if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
                return;

        priv = audioPlayer->priv;
        priv->audio_proxy =  frampton_agent_proxy_new_finish(res , &error);

        if( priv->audio_proxy== NULL)
        {
                WARNING ("audio agent proxy error: %s: code %d: %s", g_quark_to_string(error->domain), error->code, error->message);
                g_error_free (error);
        }
        else
        {
		gchar *pIconPath = NULL;

		connect_audio_agent_signals(priv->audio_proxy, audioPlayer);
		pIconPath = g_strconcat(priv->pDataDir, "/icon_status_AC.png", NULL);
		/* register to audio agent */
		frampton_agent_call_register(priv->audio_proxy, FRAMPTON_AUDIO_PLAYER_APP_NAME, pIconPath, NULL, audio_agent_registration_info_message_cb, user_data);
		frampton_audio_player_free_str(pIconPath);
        }


}

/*********************************************************************************************
 * Function: audio_agent_name_appeared 
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void audio_agent_name_appeared (GDBusConnection *connection,
                                const gchar *name,
                                const gchar *name_owner,
                                gpointer user_data)
{
		DEBUG ("service name appeared: %s", name);
		/* Asynchronously creates a proxy for the D-Bus interface */
		frampton_agent_proxy_new (connection,
				G_DBUS_PROXY_FLAGS_NONE,
				FRAMPTON_AGENT_BUS_NAME,
				FRAMPTON_AGENT_OBJECT_PATH,
				NULL,
				audio_agent_service_proxy_cb,
				user_data);
}

/*********************************************************************************************
 * Function:    audio_agent_name_vanished
 * Description: 
 * Parameters:  
 * Return:      void
 ********************************************************************************************/
void audio_agent_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
        FramptonAudioPlayer *audioPlayer = FRAMPTON_AUDIO_PLAYER (user_data);

        if(! FRAMPTON_IS_AUDIO_PLAYER(user_data))
                return;

		DEBUG ("service name vanished: %s", name);

        if(NULL != audioPlayer->priv->audio_proxy)
            g_object_unref(audioPlayer->priv->audio_proxy);
}

