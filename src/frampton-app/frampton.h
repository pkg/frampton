/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**************************************************************************************************
* Filename          : AudioPlayer.h
* Package           : FRAMPTON
* Application       : AudioPlayer
* Author            : Abhiruchi Gupta
* Copyright         :
* Description       : Audio Client
**************************************************************************************************/
/* inclusion guard */
#ifndef __FRAMPTON_H__
#define __FRAMPTON_H__

#include <frampton-agent.h>

#include <canterbury.h>
#include <barkway-enums.h>
#include "barkway.h"
#include <mildenhall/mildenhall.h>
#include "libgrassmoor-tracker.h"
#include "canterbury_app_handler.h"
#include <thornbury/thornbury.h>

#include "frampton-app.h"
#include "frampton-mode.h"

G_BEGIN_DECLS

#define FRAMPTON_TYPE_AUDIO_PLAYER frampton_audio_player_get_type()

#define FRAMPTON_AUDIO_PLAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  FRAMPTON_TYPE_AUDIO_PLAYER, FramptonAudioPlayer))

#define FRAMPTON_AUDIO_PLAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  FRAMPTON_TYPE_AUDIO_PLAYER, FramptonAudioPlayerClass))

#define FRAMPTON_IS_AUDIO_PLAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  FRAMPTON_TYPE_AUDIO_PLAYER))

#define FRAMPTON_IS_AUDIO_PLAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  FRAMPTON_TYPE_AUDIO_PLAYER))

#define FRAMPTON_AUDIO_PLAYER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  FRAMPTON_TYPE_AUDIO_PLAYER, FramptonAudioPlayerClass))

typedef struct _FramptonAudioPlayer         FramptonAudioPlayer;
typedef struct _FramptonAudioPlayerClass    FramptonAudioPlayerClass;
typedef struct _FramptonAudioPlayerPrivate  FramptonAudioPlayerPrivate;

#define FRAMPTON_AUDIO_PLAYER_ARTISTS "A R T I S T S"
#define FRAMPTON_AUDIO_PLAYER_ALBUM "A L B U M S"
#define FRAMPTON_AUDIO_PLAYER_SONGS "S O N G S"

#define SEARCH          "search"
#define DELETE          "delete"
#define TO_PLAYLIST     "to-playlist"
#define SHARE           "share"
#define RELATED         "related"

#define LIST_SORT_ROLLER	"SortRoller"
#define ARTIST_ALBUM_ROLLER	"ArtistAlbumRoller"
#define SONGS_ROLLER		"SongsRoller"
#define LIST_CONTEXT_DRAWER	"ListContextDrawer"
#define LIST_VIEWS_DRAWER	"ListViewsDrawer"

#define THUMB_SORT_ROLLER	"SortRoller"
#define THUMB_ROLLER		"ThumbRoller"
#define THUMB_CONTEXT_DRAWER	"ThumbContextDrawer"
#define THUMB_VIEWS_DRAWER	"ThumbViewsDrawer"

#define DETAIL_PROGRESSBAR	"ProgressBar"
#define DETAIL_BOTTOMBAR	"BottomBar"
#define DETAIL_SORT_ROLLER	"SortRoller"
#define DETAIL_CONTEXT_DRAWER	"DetailContextDrawer"
#define DETAIL_VIEWS_DRAWER	"DetailViewsDrawer"
#define DETAIL_META_ROLLER	"MetaRoller"
#define DETAIL_INFO_ROLLER	"InfoRoller"

#define SHUFFLE		"shuffle"
#define REPEAT		"repeat"

struct _FramptonAudioPlayer
{
	ClutterActor parent;

	FramptonAudioPlayerPrivate *priv;
};

struct _FramptonAudioPlayerClass
{
	ClutterActorClass parent_class;
};

struct _FramptonAudioPlayerPrivate
{
	FramptonApp *app;

	ThornburyViewManagerAppData *appdata;
	/* proxy */
	BarkwayService *popup_proxy;
	CanterburyAppManager *app_mgr_proxy;
	FramptonAgent *audio_proxy;
	GrassmoorTracker *tracker;

	/* widgets pointer */
	ClutterActor *pThumbRollerPtr;

	ClutterTimeline *pTimeline;
	/* roller models */
	ThornburyModel *pSortRollerModel;
	ThornburyModel *pSongRollerModel;
	ThornburyModel *pAlbumArtistRollerModel;
	ThornburyModel *pThumbRollerModel;
	ThornburyModel *pMetaRollerModel;
	ThornburyModel *pInfoRollerModel;
	ThornburyModel *pBottomBarModel;
	ThornburyModel *pInfoHeaderModel;

	/* current mode */
	FramptonAudioPlayerMode enCurrentMode;

	gchar **pResultPointer;
	gchar **pModeUrlList;

	GList *startUpExecList;
	GList *pThumbsList;
	GList *pCoverThumbList;

	GdkPixbuf *pGdkPixbuf;
	ClutterContent *pContent;
	GPtrArray *similarTracks;
	GThread *pThread;

	/* current source */
	gchar *pCurrentSource;
	gchar *pCurrentView;
	gchar *pCurrentTrack;
	gchar *pCurrentLoadUrl;
	gchar *trackDuration;
	gchar *pDataDir;

	gint inCurrentIndex;
	gint inStartIndex;
	gint inMidIndex;
	gint duplicateCount;
	gint inTotalCount;
	gint inThumbCount;
	gint inArtistAlbumCount;
	gint inTempTotal;
	gint inUnknownCount;
	gint inSongCount;
	gint inModeUrlCount;
	//gint inTimeoutSource;
	gint prevRow;
	gint prevSongRow;
	gint prevMetaRow;
	gint inLUMPlayState;
	gint extraThumbItems;
	gint inExtraDummy;	

	gboolean externalModeChange;
	/* flag for coupling */
	gboolean setAlbumArtistMenu;
	gboolean setSongsMenu;
	gboolean QuickMenufocus;
	gboolean MainMenufocus;
	gboolean bSeekStarted;
	gboolean bShuffle;
	gboolean bRepeat;
	gboolean bAnimation;
	gboolean bResetUI;
	gboolean bListSortfocus;
	gboolean bSortItemfocus;
	gboolean bRemoveState;
	gboolean disableLoadCheck;
    gboolean bLoad;
	gboolean bNewUri;

	GHashTable *pUrlMetaDataHash;
	/* model hash set to view manager */
	GHashTable *pListModelHash;
	GHashTable *pThumbModelHash;
	GHashTable *pDetailModelHash;
	/* list roller item hash for coupling */
	GHashTable *pArtistAlbumHash;
	GHashTable *pSongsHash;
	GHashTable *pSortListHash;

	/* view manager */
	ThornburyViewManager *pThornburyViewManager;

	GrassmoorMediaInfo *currentLoadMediaInfo;
	GrassmoorMediaInfo *currentPlayingInfo;
	//gboolean repeat;
	//gboolean shuffle;

};

#define FRAMPTON_AUDIO_PLAYER_PRIVATE(o) \
        (G_TYPE_INSTANCE_GET_PRIVATE ((o), FRAMPTON_TYPE_AUDIO_PLAYER, FramptonAudioPlayerPrivate))

enum
{
    SORT_ROLLER_COLUMN_NAME,
    SORT_ROLLER_COLUMN_ICON,
    SORT_ROLLER_COLUMN_LABEL,
    SORT_ROLLER_COLUMN_TYPE,
    SORT_ROLLER_COLUMN_FONT_SIZE,	
    SORT_ROLLER_COLUMN_LAST

};

/* drawer model columns */
enum
{
        DRAWER_COLUMN_NAME,
        DRAWER_COLUMN_ICON,
        DRAWER_COLUMN_TOOLTIP_TEXT,
        DRAWER_COLUMN_REACTIVE,
        DRAWER_COLUMN_LAST
};

/* artist-album model columns */
enum
{
        ARTISTALBUM_ROLLER_COL_ID,
        ARTISTALBUM_ROLLER_COL_ICON,
        ARTISTALBUM_ROLLER_COL_MID_TEXT,
        ARTISTALBUM_ROLLER_COL_RIGHT_TEXT,
        ARTISTALBUM_ROLLER_COL_FIRST_URL,
        ARTISTALBUM_ROLLER_COL_CONTENT,
        ARTISTALBUM_ROLLER_COL_LAST
};

/* artis album roller footer model column */
enum
{
        ARTISTALBUM_FOOTER_NAME,
        ARTISTALBUM_FOOTER,
        ARTISTALBUM_ICON_NAME,
        ARTISTALBUM_ICON,
        ARTISTALBUM_MID_TEXT_NAME,
        ARTISTALBUM_MID_TEXT,
        ARTISTALBUM_RIGHT_TEXT_NAME,
        ARTISTALBUM_RIGHT_TEXT,
        ARTISTALBUM_ARROW_UP_NAME,
        ARTISTALBUM_ARROW_UP,
        ARTISTALBUM_SHOW_ARROW_NAME,
        ARTISTALBUM_SHOW_ARROW,
        ARTISTALBUM_LAST
};

/* songs model columns */
enum
{
        SONGS_ROLLER_COLUMN_ID,
        SONGS_ROLLER_COLUMN_NUM,
        SONGS_ROLLER_COLUMN_ICON,
        SONGS_ROLLER_COLUMN_LABEL,
	SONGS_ROLLER_HIGHLIGHT_ARROW_SHOW,	
	SONGS_ROLLER_HIGHLIGHT_ARROW_STATE,	
	SONGS_ROLLER_RELATED_THUMB_INDEX,
        SONGS_ROLLER_COLUMN_LAST
};

/* songs roller footer model column */
enum
{
        SONGS_ROLLER_FOOTER_NAME,
        SONGS_ROLLER_FOOTER,
        SONGS_ROLLER_ICON_NAME,
        SONGS_ROLLER_ICON,
        SONGS_ROLLER_TEXT_NAME,
        SONGS_ROLLER_TEXT,
        SONGS_ROLLER_ARROW_UP_NAME,
        SONGS_ROLLER_ARROW_UP,
        SONGS_ROLLER_SHOW_ARROW_NAME,
        SONGS_ROLLER_SHOW_ARROW,
        SONGS_ROLLER_LAST
};

/* thumb roller model column */
enum
{
        THUMB_ROLLER_ICON,
        THUMB_ROLLER_URL,
	THUMB_ROLLER_OVERLAY_STATE,
	THUMB_ROLLER_OVERLAY_SHOW,	
	THUMB_ROLLER_OVERLAY_TEXT,
        THUMB_ROLLER_COLUMN_LAST
};

/* info roller header model columns */
enum
{
        INFO_ROLLER_HEADER_LEFT_ICON_TEXT,
        INFO_ROLLER_HEADER_MID_TEXT,
        INFO_ROLLER_HEADER_RIGHT_ICON_TEXT,
        INFO_ROLLER_HEADER_NONE
};

enum
{
        BOTTOM_BAR_ONE_ACTIVE,
        BOTTOM_BAR_ONE_INACTIVE,
        BOTTOM_BAR_ONE_TEXTURE_NAME,
        BOTTOM_BAR_TWO_ACTIVE,
        BOTTOM_BAR_TWO_INACTIVE,
        BOTTOM_BAR_TWO_TEXTURE_NAME,
        BOTTOM_BAR_THREE_ACTIVE,
        BOTTOM_BAR_THREE_INACTIVE,
        BOTTOM_BAR_THREE_TEXTURE_NAME,
        BOTTOM_BAR_FOUR,
        BOTTOM_BAR_LAST
};

GType frampton_audio_player_get_type (void) G_GNUC_CONST;

/* main.c */
ClutterActor *frampton_audio_player_create (void);
FramptonAudioPlayer *frampton_audio_player_get_default (void);
GList *frampton_audio_player_get_startup_exec_list (void);
GHashTable* frampton_audio_player_get_open_with_hash (void);
gchar *frampton_audio_player_get_open_with_url(GHashTable *pHash);
void update_exec_args_list(gchar **argvList);

/* client_handler.c */
void initialize_client_handler (FramptonAudioPlayer *pAudioPlayer,
                                GApplication *app);
//void set_status_bar_clb( GObject *source_object,GAsyncResult *res,gpointer user_data);

/* audio_agent_handler.c */
void audio_agent_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data);
void audio_agent_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data);
void audio_agent_service_proxy_cb(GObject *source_object, GAsyncResult *res, gpointer user_data);
void audio_agent_registration_info_message_cb(GObject *source_object, GAsyncResult *res, gpointer user_data);
void audio_agent_get_url_message_info_cb(GObject *source_object, GAsyncResult *res, gpointer user_data);
void frampton_audio_player_initiate_play(FramptonAudioPlayer *pAudioPlayer, gchar *pUrl);
void update_seek_to_audio_agent(gfloat position, FramptonAudioPlayer *pAudioPlayer);
void update_pause_to_audio_agent(FramptonAudioPlayer *pAudioPlayer);
void update_resume_to_audio_agent(FramptonAudioPlayer *pAudioPlayer);
void repeat_message_info_cb(GObject *source_object, GAsyncResult *res, gpointer user_data);
void shuffle_message_info_cb(GObject *source_object, GAsyncResult *res, gpointer user_data);
void connect_audio_agent_signals(FramptonAgent *audio_proxy, FramptonAudioPlayer *audioPlayer);
void disconnect_audio_agent_signals(FramptonAgent *audio_proxy, FramptonAudioPlayer *audioPlayer);
void audio_agent_get_state_cb ( GObject *object, GAsyncResult *res, gpointer user_data);

/* ui.c */
void frampton_audio_player_init_ui(FramptonAudioPlayer *pAudioPlayer);
void set_initial_configuration(FramptonAudioPlayer *pAudioPlayer);
void launch_ui_for_open_with(FramptonAudioPlayer *pAudioPlayer);

gboolean build_ui(FramptonAudioPlayer *pAudioPlayer);
void frampton_audio_player_update_progress_bar(gdouble trackPosition);
void initialize_views(FramptonAudioPlayer *pAudioPlayer);
void update_progress_bar_play_state(gboolean bPlayState);
void populate_meta_roller(FramptonAudioPlayer *pAudioPlayer, GrassmoorMediaInfo *newPlayingInfo, gchar *playingTrack);
void populate_info_roller(FramptonAudioPlayer *pAudioPlayer, GrassmoorMediaInfo *newPlayingInfo);
void update_current_track_details (FramptonAudioPlayer *pAudioPlayer, gchar *playingtrack);
void reposition_list_rollers (FramptonAudioPlayer *pAudioPlayer);
gboolean set_list_to_focus_on_view_change(FramptonAudioPlayer *pAudioPlayer, gboolean bHighlight);
gboolean set_thumb_to_focus_on_view_change(gchar *pUri);
void init_new_hash_tables(FramptonAudioPlayer *pAudioPlayer);
void update_media_overlay(FramptonAudioPlayer *pAudioPlayer, gint row, gchar *pUri);
void update_media_overlay_play_state(gboolean bState);
void update_highlight_arrow(FramptonAudioPlayer *pAudioPlayer, gint row, gchar *pUri);
void update_highlight_arrow_state(gboolean bState);
void update_meta_highlight_arrow_state(gboolean bState);
void update_bottom_bar(FramptonAudioPlayer *pAudioPlayer, gboolean bShuffle, gboolean bRepeat);
void add_remove_thumbs_divisible_by_4(FramptonAudioPlayer *pAudioPlayer, gboolean bAddThumbs);

/* functions.c */
ClutterActor* create_and_position_line(ClutterColor lineColor, gfloat x, gfloat y, gfloat width, gfloat height, ClutterActor *parent);
ClutterActor* create_and_position_text(const gchar* fontName, ClutterColor fontColor, gfloat x, gfloat y, ClutterActor *parent);
FramptonAudioPlayerMode get_requested_mode(GList *execArgs);
void albumartist_roller_locking_finished_cb (MildenhallRollerContainer *pRoller, gpointer pUserData);
void song_roller_locking_finished_cb (MildenhallRollerContainer *pRoller, gpointer pUserData);
void sort_roller_locking_finished_cb (MildenhallRollerContainer *pRoller, gpointer pUserData);
void thumb_roller_locking_finished_cb (MildenhallRollerContainer *pRoller, gpointer pUserData);
void albumartist_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData);
void song_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData);
void sort_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData);
void thumb_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData);
void views_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData);
void list_context_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData);
void view_switched(ThornburyViewManager *pThornburyViewManager, gpointer pName, gpointer *pUserData);
gboolean progress_bar_seek_updated_cb(MildenhallProgressBar *progressBar, gfloat position, gpointer userData);
gboolean progress_bar_seek_started_cb(MildenhallProgressBar *progressBar, gfloat position, gpointer userData);
gboolean progress_bar_pause_requested_cb(MildenhallProgressBar *progressBar, gpointer userData);
gboolean progress_bar_play_requested_cb(MildenhallProgressBar *progressBar, gpointer userData);
void info_roller_item_selected_cb(MildenhallInfoRoller *roller, guint row, gpointer data);
void meta_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData);
gboolean bottom_bar_pressed (MildenhallBottomBar *bottomBar, gchar *pButtonName, gpointer pUserData);
gboolean bottom_bar_released (MildenhallBottomBar *bottomBar, gchar *pButtonName, gpointer pUserData);
void view_switch_begin_cb(ThornburyViewManager *pThornburyViewManager, gchar *pName, gpointer *pUserData);
gboolean reset_ui(GVariant *arguments);
void free_current_view(gchar *view);
void free_all_memory(FramptonAudioPlayer *pAudioPlayer);
void frampton_audio_player_free_str(gchar *pStr);
void update_sort_list(gchar *pText, gint inRow, FramptonAudioPlayer *pAudioPlayer);
gint get_current_playing_item_number(ThornburyModel *pModel, FramptonAudioPlayer *pAudioPlayer, gint column);
gboolean frampton_audio_player_roller_scroll_started(ClutterActor *actor, gpointer userData);
gboolean frampton_audio_player_roller_scroll_completed(ClutterActor *actor, gpointer userData);

void frampton_switch_ui_mode (const gchar * const *execArgs,
                              gboolean force);

G_END_DECLS

#endif /* __FRAMPTON_H__ */
