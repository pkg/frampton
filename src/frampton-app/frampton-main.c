/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "frampton.h"

#include "canterbury_app_handler.h"

#include "frampton-app.h"

#define DEFAULT_FRAMPTON_APP_NAME "org.apertis.Frampton"

G_DEFINE_TYPE (FramptonAudioPlayer, frampton_audio_player, CLUTTER_TYPE_ACTOR)

FramptonAudioPlayer *defaultAudioPlayer = NULL;
static GList *startUpExecList = NULL;
static GHashTable *openWithHash = NULL;
ClutterActor *pAudioPlayerStage = NULL;

gchar *FRAMPTON_AUDIO_PLAYER_APP_NAME = NULL;

/********************************************************
 * Function : frampton_audio_player_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void frampton_audio_player_get_property (GObject *object,guint property_id,GValue *value,GParamSpec *pspec)
{
	switch (property_id)
	{
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/********************************************************
 * Function : frampton_audio_player_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void frampton_audio_player_set_property (GObject *object,guint property_id,const GValue *value,GParamSpec *pspec)
{
	switch (property_id)
	{
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/********************************************************
 * Function : frampton_audio_player_dispose
 * Description: Dispose the audio player object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void frampton_audio_player_dispose (GObject *object)
{
	FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER(object);

	g_clear_object (&pAudioPlayer->priv->tracker);

	G_OBJECT_CLASS (frampton_audio_player_parent_class)->dispose (object);

	//FramptonAudioPlayerPrivate *priv = pAudioPlayer->priv;

	free_all_memory(pAudioPlayer);
}

/********************************************************
 * Function : frampton_audio_player_finalize
 * Description: Finalize the audio player object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void frampton_audio_player_finalize (GObject *object)
{
	FramptonAudioPlayer *pAudioPlayer = FRAMPTON_AUDIO_PLAYER (object);
	g_free (pAudioPlayer->priv->pDataDir);

	G_OBJECT_CLASS (frampton_audio_player_parent_class)->finalize (object);
}

/********************************************************
 * Function : frampton_audio_player_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void frampton_audio_player_class_init (FramptonAudioPlayerClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	g_type_class_add_private (klass, sizeof (FramptonAudioPlayerPrivate));
	object_class->get_property = frampton_audio_player_get_property;
	object_class->set_property = frampton_audio_player_set_property;
	object_class->dispose = frampton_audio_player_dispose;
	object_class->finalize = frampton_audio_player_finalize;
}

/********************************************************
 * Function : frampton_audio_player_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void frampton_audio_player_init (FramptonAudioPlayer *self)
{
	self->priv = FRAMPTON_AUDIO_PLAYER_PRIVATE (self);

	self->priv->pDataDir = g_strdup (EXPANDED_DATADIR);

	self->priv->tracker = grassmoor_tracker_new ();
}

/****************************************************
 * Function : frampton_audio_player_get_open_with_url
 * Description: Get the requested url to play
 * Parameters: The arguments with which the process
 *                         was launched
 * Return value: url
 ********************************************************/
gchar *frampton_audio_player_get_open_with_url(GHashTable *pHash)
{
	gchar *pUrl = NULL;

        if(NULL != pHash)
        {
                pUrl = g_hash_table_lookup(pHash, "url");
		DEBUG ("%s", pUrl);
	}
	return pUrl;
}

/*********************************************************************************************
 * Function:    update_exec_args_list
 * Description: update the exec argument list
 * Parameters:  The argv list
 * Return:      None
 ********************************************************************************************/
void update_exec_args_list(gchar **argvList)
{
        gchar **execList = NULL;
        gboolean bFlag = TRUE;

	if(NULL == argvList)
		return;

	if(NULL != openWithHash)
        {
                //g_hash_table_foreach_remove(openWithHash, (GHRFunc)clear_hash_entries, audioPlayer);
                g_hash_table_destroy(openWithHash);
                openWithHash = NULL;
        }

	if(startUpExecList)
        {
                GList *tempList = g_list_first(startUpExecList);
                while(tempList)
                {
                        if(NULL != tempList->data)
                        {
                                g_free(tempList->data);
                                tempList->data = NULL;
                        }
                        startUpExecList = g_list_delete_link(startUpExecList, tempList);
                        tempList = g_list_first(startUpExecList);
                }
		if(NULL != tempList)
		{
			g_free(tempList);
			tempList = NULL;
		}
        }

	/* update argument list */
        execList = argvList;

	while(*execList)
	{
		/*if(g_strcmp0(*execList, "mount-path") == 0)
		{
			bFlag = TRUE;
			break;
		}*/
		if(g_strcmp0(*execList, "url") == 0)
		{
			execList++;
			if(*execList && (g_strrstr(*execList, "file:") != NULL))
			{
				DEBUG ("List = %s", *execList);
				bFlag = FALSE;
				break;
			}
		}
		execList++;
	}

	execList = argvList;

	if(bFlag)
	{
		while(*execList)
		{
			if(g_strcmp0(*execList, "app-name") == 0)
			{
				execList++;
				startUpExecList = g_list_insert(startUpExecList, g_strdup(*execList), 0);
				if(NULL != FRAMPTON_AUDIO_PLAYER_APP_NAME)
				{
					g_free(FRAMPTON_AUDIO_PLAYER_APP_NAME);
					FRAMPTON_AUDIO_PLAYER_APP_NAME = NULL;
				}
				FRAMPTON_AUDIO_PLAYER_APP_NAME = g_strdup(*execList);
			}
			else if(g_strcmp0(*execList, "menu-entry") == 0)
			{
				execList++;
				startUpExecList = g_list_insert(startUpExecList, g_strdup(*execList), 1);
			}
			else if(g_strcmp0(*execList, "url") == 0)
			{
				execList++;
				startUpExecList = g_list_insert(startUpExecList, g_strdup(*execList), 2);
			}
			else if(g_strcmp0(*execList, "mount-path") == 0)
			{
				execList++;
                                startUpExecList = g_list_insert(startUpExecList, g_strdup(*execList), 3);
			}
			execList++;
		}
	}

	else
	{
		gchar **tempStr = argvList;

		openWithHash = g_hash_table_new(g_str_hash, g_str_equal);
		tempStr++;
		while(*tempStr)
		{
			gchar *key = g_strdup((gchar*)*tempStr);
			gchar *value = NULL;
			tempStr++;
			if(*tempStr)
				value = g_strdup((gchar*)*tempStr);
			else
				value = NULL;
	
			if(g_strcmp0(key,  "app-name") == 0)
			{
				if(NULL != FRAMPTON_AUDIO_PLAYER_APP_NAME)
                                {
                                        g_free(FRAMPTON_AUDIO_PLAYER_APP_NAME);
                                        FRAMPTON_AUDIO_PLAYER_APP_NAME = NULL;
                                }
                                FRAMPTON_AUDIO_PLAYER_APP_NAME = g_strdup(value);
			}
		
			g_hash_table_insert(openWithHash, key, value);
			tempStr++;
		}

	}
}

/*********************************************************************************************
 * Function:    frampton_audio_player_get_startup_exec_list
 * Description: Get the Startup exec list
 * Parameters:  None
 * Return:      Get the startup exec list
 ********************************************************************************************/
GList *
frampton_audio_player_get_startup_exec_list (void)
{
        return startUpExecList;
}

/*********************************************************************************************
 * Function:    frampton_audio_player_get_open_with_hash
 * Description: Get the open with hash
 * Parameters:  None
 * Return:      Get the open with hash
 ********************************************************************************************/
GHashTable *
frampton_audio_player_get_open_with_hash (void)
{
        return openWithHash;
}

/********************************************************
 * Function : frampton_audio_player_get_default
 * Description: get the audio player object instance.
 * Parameters:  void
 * Return value: void
 ********************************************************/
FramptonAudioPlayer *
frampton_audio_player_get_default (void)
{
    return defaultAudioPlayer;
}

/********************************************************
 * Function : frampton_audio_player_create
 * Description: audio player object creation with ui.
 * Parameters:  void
 * Return value: audioplayer*
 ********************************************************/
ClutterActor *
frampton_audio_player_create (void)
{
	FramptonAudioPlayer *pAudioplayer = g_object_new(FRAMPTON_TYPE_AUDIO_PLAYER, NULL);
	defaultAudioPlayer = pAudioplayer;

	/* initialize the audio player ui */
	frampton_audio_player_init_ui(pAudioplayer);

	return CLUTTER_ACTOR(pAudioplayer);
}

/*
 * Activate the application as though an older version of Frampton had been
 * invoked with the given command-line. If @force is true, force the new
 * UI to be shown even if it is returning to the same mode.
 *
 * FIXME: Frampton should have a structured representation of the desired
 * state, replacing all uses of argv other than where we actually receive
 * command-line arguments at an API boundary.
 */
static void
activate (GApplication *app,
          int argc,
          gchar **argv,
          gboolean force)
{
  ClutterActor *pAudioPlayer = NULL;

  if (defaultAudioPlayer == NULL)
    {
      /* Initial activation: we need to create the window, etc. */

      update_exec_args_list (argv);

      clutter_set_windowing_backend ("gdk");

      if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
        exit (1);

      if (FRAMPTON_AUDIO_PLAYER_APP_NAME == NULL)
        FRAMPTON_AUDIO_PLAYER_APP_NAME = g_strdup (DEFAULT_FRAMPTON_APP_NAME);

      /* Our GUI window is never closed once it's opened. */
      g_application_hold (app);

      pAudioPlayerStage = mildenhall_stage_new (FRAMPTON_AUDIO_PLAYER_APP_NAME);
      pAudioPlayer = frampton_audio_player_create ();
      g_assert (pAudioPlayer == (ClutterActor *) defaultAudioPlayer);
      clutter_actor_add_child (CLUTTER_ACTOR (pAudioPlayerStage), pAudioPlayer);
      initialize_client_handler (FRAMPTON_AUDIO_PLAYER (pAudioPlayer), app);
    }
  else
    {
      /* A subsequent activation: we already have the window and most of the
       * state, we just need to swap between modes. */

      /* Omit argv[0] here */
      frampton_switch_ui_mode ((const gchar * const *) (argv + 1), force);
    }
}

static void
activate_cb (GApplication *app,
             gpointer user_data)
{
  const gchar *fake_argv[] =
  {
    "frampton",
    "app-name",
    FRAMPTON_APP_ID,
    "menu-entry",
    FRAMPTON_AUDIO_PLAYER_ARTISTS,
    "url",
    " ",
    NULL
  };

  DEBUG ("Activated");
  activate (app, G_N_ELEMENTS (fake_argv) - 1, (gchar **) fake_argv, FALSE);
}

static void
mode_activated_cb (GApplication *app,
                   const gchar *entry_point_id,
                   gint mode,
                   gpointer user_data)
{
  const gchar *fake_argv[] =
  {
    /* 0 */ "frampton",
    /* 1 */ "app-name",
    /* 2 */ entry_point_id,
    /* 3 */ "menu-entry",
    /* 4 */ "<placeholder>",
    /* 5 */ "url",
    /* 6 */ " ",
    NULL
  };

  switch (mode)
    {
    case FRAMPTON_AUDIO_PLAYER_MODE_ARTIST:
      fake_argv[4] = FRAMPTON_AUDIO_PLAYER_ARTISTS;
      break;

    case FRAMPTON_AUDIO_PLAYER_MODE_ALBUM:
      fake_argv[4] = FRAMPTON_AUDIO_PLAYER_ALBUM;
      break;

    case FRAMPTON_AUDIO_PLAYER_MODE_SONGS:
      fake_argv[4] = FRAMPTON_AUDIO_PLAYER_SONGS;
      break;

    default:
      g_return_if_reached ();
    }

  DEBUG ("Switching to %s mode for %s", fake_argv[4], entry_point_id);
  activate (app, G_N_ELEMENTS (fake_argv) - 1, (gchar **) fake_argv, FALSE);
}

static void
command_line_cb (GApplication *app,
                 GApplicationCommandLine *command_line,
                 gpointer user_data)
{
  g_auto (GStrv) argv = NULL;
  gint argc;
  gint i;

  argv = g_application_command_line_get_arguments (command_line, &argc);

  DEBUG ("Activated with command-line arguments:");

  for (i = 0; i < argc; i++)
    DEBUG ("- \"%s\"", argv[i]);

  activate (app, argc, argv, FALSE);
}

static void
open_cb (GApplication *app,
         GFile **files,
         gint n_files,
         const gchar *hint,
         gpointer user_data)
{
  /* Because Frampton is designed around a command-line, we turn open and
   * activate requests from D-Bus into fake command-line arguments.
   * TODO: Reverse this, maintaining a more abstract state. */
  const gchar *fake_argv[] =
  {
    /* 0 */ "frampton",
    /* 1 */ "app-name",
    /* 2 */ FRAMPTON_APP_ID,
    /* 3 */ "menu-entry",
    /* 4 */ FRAMPTON_AUDIO_PLAYER_ARTISTS,
    /* 5 */ "url",
    /* 6 */ "<placeholder>", /* replaced by the real URL below */
    NULL
  };
  gint i;
  g_autofree gchar *first_uri = NULL;

  DEBUG ("Activated to open %d files", n_files);

  for (i = 0; i < n_files; i++)
    {
      g_autofree gchar *uri = g_file_get_uri (files[i]);

      DEBUG ("- %s", uri);
    }

  if (n_files == 0)
    {
      WARNING ("No files specified, treating as an activate request instead");
      activate_cb (app, NULL);
      return;
    }

  /* We can only open one file at the moment. */
  first_uri = g_file_get_uri (files[0]);
  fake_argv[6] = first_uri;

  activate (app, G_N_ELEMENTS (fake_argv) - 1, (gchar **) fake_argv, TRUE);
}

/********************************************************
 * Function : main
 * Description: creates audio player object and init the services
 * Parameters:  gint, **argv
 * Return value: int
 ********************************************************/
int main( gint argc, gchar **argv )
{
  g_autoptr (FramptonApp) app = NULL;

  app = frampton_app_new ();

  /* libthornbury assumes that if we have a default GApplication,
   * we want to use GResource for its JSON views. We aren't ready
   * for that yet. */
  g_application_set_default (NULL);

  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
  g_signal_connect (app, "command-line", G_CALLBACK (command_line_cb), NULL);
  g_signal_connect (app, "mode-activated", G_CALLBACK (mode_activated_cb),
                    NULL);
  g_signal_connect (app, "open", G_CALLBACK (open_cb), NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}
